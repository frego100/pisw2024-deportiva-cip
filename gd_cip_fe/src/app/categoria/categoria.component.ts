import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';

import { CrearCampeonatoComponent } from '../crear-campeonato/crear-campeonato.component';
import { DeporteService } from '../services/SCrearCamp/deporte.service';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent {
  deportes: any[] = [];

  constructor(
    private deporteService: DeporteService, 
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<CategoriaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}
  
  //Datos de deporte
  ngOnInit() {
    this.deporteService.getDeportes().subscribe(data => {
      this.deportes = data;
    });
  }
  
  abrirModalCrearCampeonato(deporte: any): void {
    this.dialogRef.close({ deporte, tipoCampeonato: this.data.tipoCampeonato });
  }

  /*abrirModalCrearCampeonato(deporte: any): void {
    const dialogRef = this.dialog.open(CrearCampeonatoComponent, {
      data: { 
        tipoCampeonato: this.data.tipoCampeonato,
        deporte: deporte,
        campeonatoId: this.data.campeonatoId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dialogRef.close(result);
      }
    });
  }*/
}
