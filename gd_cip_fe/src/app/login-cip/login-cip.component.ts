import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UsuarioService } from '../services/usuario.service';

@Component({
  selector: 'app-login-cip',
  templateUrl: './login-cip.component.html',
  styleUrls: ['./login-cip.component.css']
})
export class LoginCipComponent {
  loginForm = new FormGroup({
    cip: new FormControl('', [Validators.required]),
  });

  constructor(
    private service: UsuarioService
  ){}

  onSubmit() {
    const v = this.loginForm.value
    if (this.loginForm.valid) {
      console.log(v);

      let request;
      request = this.service.findByCip(v.cip);
      request.subscribe((data) => {
        console.log(data);
      })
    }
  }
}
