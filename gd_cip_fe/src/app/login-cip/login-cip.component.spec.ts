import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginCipComponent } from './login-cip.component';

describe('LoginCipComponent', () => {
  let component: LoginCipComponent;
  let fixture: ComponentFixture<LoginCipComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginCipComponent]
    });
    fixture = TestBed.createComponent(LoginCipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
