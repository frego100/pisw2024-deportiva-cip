import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-callback',
  template: `
    <div style="display: flex; justify-content: center; align-items: center; height: 100vh;">
      <p>Autenticando... Por favor espere.</p>
    </div>
  `
})
export class CallbackComponent implements OnInit {
  constructor(
    private auth: AuthService, 
    private router: Router
  ) {}

  ngOnInit() {
    this.auth.handleRedirectCallback().subscribe({
      next: (result) => {
        // Log para depuración
        console.log('Autenticación exitosa', result);
        
        // Redirigir al menú principal después de autenticarse
        this.router.navigate(['/menu1']);
      },
      error: (error) => {
        // Log de error detallado
        console.error('Error de autenticación completo:', error);
        
        // Mostrar mensaje de error más específico si es posible
        if (error.error) {
          console.error('Detalles del error:', error.error);
        }
        
        // Redirigir a página de login
        this.router.navigate(['/user-login']);
      }
    });
  }
}