import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableBoxVoleyComponent } from './editable-box-voley.component';

describe('EditableBoxVoleyComponent', () => {
  let component: EditableBoxVoleyComponent;
  let fixture: ComponentFixture<EditableBoxVoleyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditableBoxVoleyComponent]
    });
    fixture = TestBed.createComponent(EditableBoxVoleyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
