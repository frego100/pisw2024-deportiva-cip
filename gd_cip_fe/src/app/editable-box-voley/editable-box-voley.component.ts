import { Component , Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges, OnDestroy} from '@angular/core';
import { Player } from '../services/player.service';
import { GlobalService } from '../services/global.service';
import { Observable, pipe, Subject, take, takeUntil } from 'rxjs';

@Component({
  selector: 'app-editable-box-voley',
  templateUrl: './editable-box-voley.component.html',
  styleUrls: ['./editable-box-voley.component.css']
})
export class EditableBoxVoleyComponent implements OnInit, OnChanges, OnDestroy {
  TAG = "editable-box-voley"

  @Input() title: string = '';
  @Input() items: any[] = [];
  @Input() itemsEliminar: any[] = []
  @Input() isDisabled: boolean = false;
  @Input() golContra = 0;
  @Input() numeroEquipo = 1
  @Input() tipoEditable = 0
  @Input() isHidden = false

  @Output() itemsChange = new EventEmitter<any[]>();
  @Output() onCambiarPuntaje = new EventEmitter<number>();
  @Output() onDisminuirPuntaje = new EventEmitter<number>();

  isOpen: boolean = true;
  players: Player[] = [];
  jugadoresEnEquipo: Player[] = []
  jugadorEntraId: number | null = null; // Selección del jugador que entra
  jugadorSaleId: number | null = null;
  equipoId = -1 
  tipoEditableBox = this.gs.tipoEditableBox
  partidoId!: number
  setSeleccionado!: number
  destroy$ = new Subject<void>()
  
  constructor(
    private gs: GlobalService,
  ) {}

  ngOnInit(): void {
    //console.log("EditableBoxVoleyComponent.ngOnInit()", this.items)

    //Cargar ID partido
    this.gs.partidoVoleyId$
    .pipe(takeUntil(this.destroy$))
    .subscribe(data => {
      this.partidoId = data
    })

    //Cargar ID equipos y jugadores en alineacion
    if (this.numeroEquipo == 1){
      this.subscribeEquipoId(this.gs.equipo1Id$)
      this.subscribeJugadoresEnAlineacion(this.gs.jugadoresEnAlineacion1$)
    }
    else{
      this.subscribeEquipoId(this.gs.equipo2Id$)
      this.subscribeJugadoresEnAlineacion(this.gs.jugadoresEnAlineacion2$)
    }

    //Cargar set seleccionado
    this.gs.setSeleccionado$
    .pipe(takeUntil(this.destroy$))
    .subscribe(data => {
      this.setSeleccionado = data
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes['items']){
      this.items = changes['items'].currentValue || []
      //console.log("ngOnChanges", this.items)
    }
  }

  ngOnDestroy(): void {
    //console.log("EditableBoxComponent.onDestroy()")
    this.destroy$.next()
    this.destroy$.complete()
  }

  subscribeEquipoId(equipoId$: Observable<number>){
    equipoId$
    .pipe(takeUntil(this.destroy$))
    .subscribe(data => {
      this.equipoId = data
      //console.log("equipoId$", this.equipoId)

      //Cargar jugadores en campeonato
      //console.log("jugadoresEnCampeonato", this.gs.jugadoresEnCampeonato)
      if (this.tipoEditable == this.tipoEditableBox.ALINEACION){
        this.players = this.gs.jugadoresEnCampeonato.get(this.equipoId) || []
      }

      if (this.tipoEditable == this.tipoEditableBox.SUSTITUCION){
        this.jugadoresEnEquipo = this.gs.jugadoresEnCampeonato.get(this.equipoId) || []

        let jugadoresEnAlineacion = this.players
        this.items.forEach(element => {
          let jugador = this.jugadoresEnEquipo.find(jugador => jugador.id == element.jugadorEntraId) 
          if(jugador)
            jugadoresEnAlineacion.push(jugador)
        })
      }
    })
  }

  subscribeJugadoresEnAlineacion(jugadoresEnAlineacion$: Observable<Player[]>){
    if (this.tipoEditable != this.tipoEditableBox.ALINEACION)
      jugadoresEnAlineacion$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.players = data
      })
  }

  loadTestPlayers() {
    const TestPlayers : Player [] = [{
      id: 1,
      email: "juan.perez@example.com",
      name: "Juan Pérez",
      shortName: "J. Pérez",
      codCIP: 12345,
      position: "Defensa",
      number: '1',
      codFam: "F001",
      document: "DNI12345678",
      birthDate: new Date("1990-05-15"),
      phone: "987654321",
      equipoID: 1
    },
    {
      id: 2,
      email: "luis.garcia@example.com",
      name: "Luis García",
      shortName: "L. García",
      codCIP: 67890,
      position: "Delantero",
      number:'2',
      codFam: "F002",
      document: "DNI87654321",
      birthDate: new Date("1992-08-20"),
      phone: "987654322",
      equipoID: 2
    },
    {
      id: 3,
      email: "ana.rodriguez@example.com",
      name: "Ana Rodríguez",
      shortName: "A. Rodríguez",
      codCIP: 54321,
      position: "Mediocampista",
      number:'3',
      codFam: "F003",
      document: "DNI11223344",
      birthDate: new Date("1988-11-25"),
      phone: "987654323",
      equipoID: 3
    }]

    this.players = TestPlayers
  }
  
  addItem() {
    if (this.isOpen) this.toggle()

    //Informacion basica necesaria para cualquier detalle (alineacion, puntos, goles, sustitucion,etc)
    this.items.push({
      equipoId: this.equipoId,
      partidoId: this.partidoId,
      set: this.setSeleccionado
    })

    //console.log("addItem()", this.items)
  }

  removeItem(index: number) {
    if (this.tipoEditable == this.tipoEditableBox.SUSTITUCION){
      let jugadoresEnAlineacion = this.players
      for (let i=0; i<jugadoresEnAlineacion.length; i++){
        if (this.items[index].jugadorEntraId == jugadoresEnAlineacion[i].id){
          jugadoresEnAlineacion.splice(i, 1)
          //console.log("removeItem.SUSTITUCION" ,jugadoresEnAlineacion)
          break
        }
      }
    }

    this.itemsEliminar.push(this.items[index])
    this.items.splice(index, 1)
    this.onPuntosChange()
  }

  onCipChange(idx: number){
    let jugador: any
    if(this.players)
      jugador = this.players.find(jugador => jugador.codCIP === this.items[idx].codCIP)
    if(jugador){
      this.items[idx].nombre = jugador.name
      this.items[idx].jugadorId = jugador.id
    }
    else this.items[idx].nombre = ''
  }

  onPuntosChange(){
    //console.log(this.TAG, "onPuntosChange()")
    this.onCambiarPuntaje.emit()
  }

  onSustitucionChange(item: any){
    if (item.jugadorSaleId && item.jugadorEntraId && item.numeroCamiseta){
      let jugador = this.jugadoresEnEquipo.find(jugador => jugador.id === item.jugadorEntraId)
      if (jugador){
        let jugadoresEnAlineacion = this.players
        jugadoresEnAlineacion.push(jugador)
        
        item.isDisabled = true
      }
    }
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }
}
