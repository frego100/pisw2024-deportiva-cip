
import { PartidoDetalleRequestDTO, PartidoDTO, PartidoResponseDTO, PartidosService } from '../services/partidos.service';
import { Player, PlayerService } from '../services/player.service';
import { GlobalService } from '../services/global.service';
import { RegisterUserComponent } from '../register-user/register-user.component';
import { Tarjeta } from '../services/equipo-vista.service';
import { TablaPartidosComponent } from '../tabla-partidos/tabla-partidos.component';
import { Equipo, EquipoService } from '../services/equipo.service';

import { Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, QueryList, Renderer2, SimpleChanges, ViewChildren } from '@angular/core';
import { PartidosVolleyService, VoleyMatchDetail, VoleyMatchDTO } from '../services/partidos-volley.service';
import { combineLatest, Observable, pipe, Subject, take, takeUntil } from 'rxjs';
import { RoleService } from '../services/role.service';
import { EditableBoxVoleyComponent } from '../editable-box-voley/editable-box-voley.component';

interface DetallesPorSet {
  puntosPorSet: any[][]
}

@Component({
  selector: 'app-editar-resultado-voley',
  templateUrl: './editar-resultado-voley.component.html',
  styleUrls: ['./editar-resultado-voley.component.css']
})
export class EditarResultadoVoleyComponent implements OnInit, OnChanges, OnDestroy {
  @ViewChildren(EditableBoxVoleyComponent) editableBoxComponents!: QueryList<EditableBoxVoleyComponent>;
  TAG = "EditarResultadoVoleyComponent"

  @Input() partido!: VoleyMatchDTO
  @Input() campeonatoId!: number
  @Input() EquiposEliminatorias: any[] = []
  @Input() faseInicial: string = ""
  @Input() isHidden: boolean = false;

  @Output() onEliminar = new Subject<void>()
  @Output() onGuardar = new Subject<void>()

  // Definir las opciones de sets
  setsOptions = [1, 3, 5];

  numeroDeSets: number = 1; // Valor inicial predeterminado

  equipo1!: VoleyMatchDetail;

  equipo2!: VoleyMatchDetail

  partidoEquipo1DetDel: any
  partidoEquipo2DetDel: any
  //mejorJugador: string | null = null;
  mejorJugadorId: number | null = null;
  mejorPorteroId: number | null = null
  jugadoresEnTodaAlineacion: Player[] = []

  selectedStatus = ""
  marcador = {
    equipo1: 0,
    equipo2: 0
  };

  //Equipos
  allTeams: Equipo[] = []

  selectedTeam1!: number | undefined
  selectedTeam2!: number | undefined

  estado = this.gs.partidoFutbolEstado
  equipo1Id = -1
  equipo2Id = -1

  destroy$ = new Subject<void>()

  tipoEditableBox = this.gs.tipoEditableBox

  detallesPorSet1: DetallesPorSet = {
    puntosPorSet: []
  }

  detallesPorSet2: DetallesPorSet = {
    puntosPorSet: []
  }

  //Usado para definir el set seleccionado (el único con valor true)
  setsSeleccionados!: boolean[]

  puntosSet1 = [0, 0, 0, 0, 0]
  puntosSet2 = [0, 0, 0, 0, 0]

  //Agregar para almacenar la fase de cada partido y eliminar el warning
  fase: number = 0

  isAdministrador = false

  constructor(
    private partidoService: PartidosService,
    private playerService: PlayerService,
    private partidoVoleyService: PartidosVolleyService,
    private gs: GlobalService,
    private rs: RoleService,
    private el: ElementRef,
    private renderer: Renderer2,
  ) { }

  ngOnInit() {
    console.log(this.TAG, "ngOnInit()");
    this.isAdministrador = this.rs.hasRole('Administrador')
    this.inicializarEliminarDetalles()
    this.loadEquiposEnCampeonato()
    this.seleccionarSet(0)
  }

  async ngOnChanges(changes: SimpleChanges): Promise<void> {
    //ACTUALIZA LA INFORMACION DEL PARTIDO
    if (changes['partido']) {
      this.partido = changes['partido'].currentValue
      if (this.partido) {
        console.log(this.TAG, "partidoId", this.partido.id)
        this.gs.updatePartidoVoleyId(this.partido.id)
        this.reloadPartidoData()
        //console.log(this.allTeams)
        //console.log(this.EquiposEliminatorias)
      }
      if (this.partido.fase !== 1) {
        const eliminatoriasIds = new Set(this.EquiposEliminatorias.map(equipo => equipo.id));
        //console.log("Partidos seleccionados", this.selectedTeam1, this.selectedTeam2)
        //console.log("IDS DE EQUIPOS", eliminatoriasIds)
        this.gs.equiposEnCampeonato$
          .pipe(take(1))
          .subscribe((data: Equipo[]) => {
            this.allTeams = data.filter(equipo => eliminatoriasIds.has(equipo.id));
            //console.log("equipos-en-campeonato", this.allTeams)
          })
        //console.log(this.allTeams)
        await this.filtrarEquiposPorFase();
      }
    }
  }

  get setsArray(): number[] {
    return Array(this.numeroDeSets).fill(0).map((_, i) => i + 1); // Genera el número de sets
  }

  onSetsChange() {
    //console.log(`Número de sets actualizado a: ${this.numeroDeSets}`);
  }

  loadEquiposEnCampeonato() {
    this.gs.equiposEnCampeonato$
      .pipe(take(1))
      .subscribe((data: Equipo[]) => {
        this.allTeams = data
        //console.log("equipos-en-campeonato", this.allTeams)
      })
  }
  private filtrarEquiposPorFase(): Promise<void> {
    return new Promise((resolve) => {
      this.partidoVoleyService.findByCampeonatoId(this.campeonatoId).subscribe((partidos) => {
        const partidosMismaFase = partidos.filter((partido) => partido.fase === this.partido.fase);
        const equiposEnUso = new Set<number>();
        partidosMismaFase.forEach((partido) => {
          if (partido.id === this.partido.id) return; // Ignorar el partido actual
          if (partido.equipo1Id) equiposEnUso.add(partido.equipo1Id);
          if (partido.equipo2Id) equiposEnUso.add(partido.equipo2Id);
        });

        //console.log("Equipos en uso:", equiposEnUso);
        this.allTeams = this.allTeams.filter((equipo) => !equiposEnUso.has(equipo.id));
        //console.log(this.allTeams);
        resolve(); // Notificar que la operación ha terminado
      });
    });
  }

  //CARGA LA INFORMACION ASOCIADA AL PARTIDO
  reloadPartidoData() {
    console.log(this.TAG, "reloadPartidoData", this.partido)

    if (!this.partido)
      return
    this.numeroDeSets = this.partido.numeroSets

    this.selectedTeam1 = this.partido.equipo1Id
    this.selectedTeam2 = this.partido.equipo2Id
    this.gs.updateEquipo1Id(this.partido.equipo1Id)
    this.gs.updateEquipo2Id(this.partido.equipo2Id)

    this.selectedStatus = this.partido.estado

    this.fase = this.partido.fase

    //console.log("equipo1Id", this.partido.equipo1Id)
    //console.log("equipo2Id", this.partido.equipo2Id)
    this.loadPartidoDetalle()
  }

  getJugadoresEnAlineacion(alineacion: any[], equipoId: number) {
    const jugadorEnAlineacion: Player[] = []
    const jugadoresEnEquipo = new Map<number, Player>()
    this.gs.jugadoresEnCampeonato.get(equipoId)?.forEach(element => {
      if (element.id)
        jugadoresEnEquipo.set(element.id, element)
    })
    alineacion.forEach(element => {
      const player = jugadoresEnEquipo.get(element.jugadorId)
      if (player)
        jugadorEnAlineacion.push(player)
    })

    //console.log("jugadoresEnAlineacion", jugadorEnAlineacion)

    return jugadorEnAlineacion
  }

  loadPartidoDetalle() {
    combineLatest([
      this.partidoVoleyService.findPartidoDetalle(this.partido.id, this.partido.equipo1Id),
      this.partidoVoleyService.findPartidoDetalle(this.partido.id, this.partido.equipo2Id),
    ])
      .pipe(take(1))
      .subscribe(([partidoEquipo1Data, partidoEquipo2Data]) => {
        this.equipo1 = this.mapResponseToTeam(partidoEquipo1Data, this.detallesPorSet1)
        this.equipo2 = this.mapResponseToTeam(partidoEquipo2Data, this.detallesPorSet2)

        this.setPuntajes()
        this.onSetSeleccionado(0, 1)

        //console.log("equipo1Detalle", this.equipo1)
        //console.log("equipo2Detalle", this.equipo2)

        //Si el partido no esta preparacion, es necesario filtrar por alineacion
        this.updateAlineaciones()
      })
  }

  updateAlineaciones() {
    if (this.selectedTeam1 && this.selectedTeam2) {
      const jugadoresEnAlineacion1 = this.getJugadoresEnAlineacion(this.equipo1.alineacion, this.selectedTeam1)
      const jugadoresEnAlineacion2 = this.getJugadoresEnAlineacion(this.equipo2.alineacion, this.selectedTeam2)

      this.gs.updateJugadoresEnAlineacion1(jugadoresEnAlineacion1)
      this.gs.updateJugadoresEnAlineacion2(jugadoresEnAlineacion2)

      this.jugadoresEnTodaAlineacion = [...jugadoresEnAlineacion1, ...jugadoresEnAlineacion2]
    }
  }

  inicializarPartidoVacio() {
    this.equipo1 = {
      puntos: [],
      alineacion: [],
      sustituciones: [],
    };

    this.equipo2 = {
      puntos: [],
      alineacion: [],
      sustituciones: [],
    };

    this.marcador = {
      equipo1: 0,
      equipo2: 0
    };

    this.selectedTeam1 = undefined;
    this.selectedTeam2 = undefined;

    //console.log("Partido inicializado sin equipos.");
  }

  mapResponseToTeam(response: any, detallesPorSet: DetallesPorSet) {
    detallesPorSet.puntosPorSet = this.getDetallePorSet(response.puntos)

    const res: VoleyMatchDetail = {
      alineacion: response.alineacion,
      puntos: detallesPorSet.puntosPorSet[0],
      sustituciones: response.sustituciones,
    }

    //Completar informacion de alineacion
    const players = this.gs.jugadoresEnCampeonato
    const equipoId = res.alineacion[0]?.equipoId
    res.alineacion.forEach(element => {
      const jugador = players.get(equipoId)?.find((jugador) => jugador.id == element.jugadorId)
      if (jugador) {
        element.codCIP = jugador.codCIP
        element.nombre = jugador.name
      }
    })

    //Completar informacion de sustitucion
    res.sustituciones.forEach(element => {
      element.isDisabled = true
    })

    return res
  }

  setPuntajes() {
    this.puntosSet1 = this.getPuntosTotalesPorSet(this.detallesPorSet1)
    this.puntosSet2 = this.getPuntosTotalesPorSet(this.detallesPorSet2)

    let setsVersus = this.getSetsGanados(this.puntosSet1, this.puntosSet2)

    this.marcador.equipo1 = setsVersus[0]
    this.marcador.equipo2 = setsVersus[1]
  }

  getPuntosTotalesPorSet(detallesPorSet: DetallesPorSet) {
    const puntosTotalesPorSet: number[] = []

    detallesPorSet.puntosPorSet.forEach((e_) => {
      let total = 0
      e_.forEach(ee_ => {
        total += ee_.puntos || 0
      })
      puntosTotalesPorSet.push(total)
    })

    return puntosTotalesPorSet
  }

  getSetsGanados(puntosPorSet1: number[], puntosPorSet2: number[]) {
    let setsVersus = [0, 0]

    for (let i = 0; i < this.numeroDeSets; i++) {
      if (puntosPorSet1[i] > puntosPorSet2[i]) setsVersus[0]++
      else if (puntosPorSet1[i] < puntosPorSet2[i]) setsVersus[1]++
    }

    return setsVersus
  }

  getDetallePorSet(detalle: any[]) {
    const detalleFiltrado: any[][] = [[], [], [], [], []]
    detalle.forEach(e => {
      detalleFiltrado[e.set - 1].push(e)
    })

    return detalleFiltrado
  }

  fusionarDetallesPorSet(detalle1: any[][], detalle2: any[][]) {
    const detalleFusionado: any[] = []
    detalle1.forEach(e => {
      detalleFusionado.push(...e)
    })
    detalle2.forEach(e => {
      detalleFusionado.push(...e)
    })

    return detalleFusionado
  }

  incrementScore(equipo: 'equipo1' | 'equipo2') {
    this.marcador[equipo]++;
    //this.updateGoles(equipo);
  }

  decrementScore(equipo: 'equipo1' | 'equipo2') {
    if (this.marcador[equipo] > 0) {
      this.marcador[equipo]--;
      //this.updateGoles(equipo);
    }
  }

  onStatusChange(newStatus: string) {
    this.selectedStatus = newStatus;

    //Actualizar lista de jugadores en alineacion al cambiar estado
    //PROBABLEMENTE SE ELIMINE: es mejor cargar la lista de alineacion en cualquiera estado
    this.updateAlineaciones()
  }

  onEquipoSeleccionado(numeroEquipo: number) {
    switch (numeroEquipo) {
      case 1:
        if (this.selectedTeam1)
          this.gs.updateEquipo1Id(this.selectedTeam1)
        this.equipo1.alineacion = []
        break
      case 2:
        if (this.selectedTeam2)
          this.gs.updateEquipo2Id(this.selectedTeam2)
        this.equipo2.alineacion = []
        break
    }
  }

  onSetSeleccionado(idx: number, numeroEquipo: number) {
    this.gs.updateSetSeleccionado(idx + 1)
    this.seleccionarSet(idx)
    //console.log("set seleccionado", idx, numeroEquipo)

    this.equipo1.puntos = this.detallesPorSet1.puntosPorSet[idx]
    this.equipo2.puntos = this.detallesPorSet2.puntosPorSet[idx]
  }

  seleccionarSet(idx: number) {
    const seleccionarSets_: boolean[] = []
    for (let i = 0; i < 5; i++)
      seleccionarSets_.push(false)
    seleccionarSets_[idx] = true
    this.setsSeleccionados = seleccionarSets_
  }

  guardar() {
    //Por mientras un condicional para evitar equipo repetidos
    if (this.selectedTeam1 === undefined || this.selectedTeam2 === undefined) {
      console.error('Ambos equipos deben ser seleccionados.');
      return;
    }

    if (this.selectedTeam1 === this.selectedTeam2) {
      console.error('No puedes seleccionar el mismo equipo para ambos.');
      return;
    }

    const partido: VoleyMatchDTO = {
      id: this.partido.id,
      fecha: this.partido.fecha,
      puntaje11: this.puntosSet1[0],
      puntaje12: this.puntosSet2[0],
      puntaje21: this.puntosSet1[1],
      puntaje22: this.puntosSet2[1],
      puntaje31: this.puntosSet1[2],
      puntaje32: this.puntosSet2[2],
      puntaje41: this.puntosSet1[3],
      puntaje42: this.puntosSet2[3],
      puntaje51: this.puntosSet1[4],
      puntaje52: this.puntosSet2[4],
      campeonatoId: this.partido.campeonatoId,
      estado: this.selectedStatus,
      equipo1Id: this.selectedTeam1,
      equipo2Id: this.selectedTeam2,
      numeroSets: this.numeroDeSets,
      fase: this.partido.fase, //revisar asignacion
      grupo: "", //revisar asignacion
    }

    this.partidoVoleyService.save(partido)
      .pipe(take(1))
      .subscribe(data => {
        //console.log("partido guardado", data)

        this.onGuardar.next()
        this.inicializarPartidoVacio()
        this.inicializarEliminarDetalles()

        this.updatePartidosGlobal()
      })

    const detalle: VoleyMatchDetail = {
      alineacion: [...this.equipo1.alineacion, ...this.equipo2.alineacion],
      puntos: this.fusionarDetallesPorSet(this.detallesPorSet1.puntosPorSet, this.detallesPorSet2.puntosPorSet),
      sustituciones: [...this.equipo1.sustituciones, ...this.equipo2.sustituciones],
    }
    this.partidoVoleyService.savePartidoDetalle(detalle)
      .pipe(take(1))
      .subscribe(data => {
        //console.log("detalle guardado", data)
      })

    const detalleEliminar: VoleyMatchDetail = {
      alineacion: [...this.partidoEquipo1DetDel.alineacion, ...this.partidoEquipo2DetDel.alineacion],
      puntos: [...this.partidoEquipo1DetDel.puntos, ...this.partidoEquipo2DetDel.puntos],
      sustituciones: [...this.partidoEquipo1DetDel.sustituciones, ...this.partidoEquipo2DetDel.sustituciones],
    }
    this.partidoVoleyService.deletePartidoDetalle(detalleEliminar)
      .pipe(take(1))
      .subscribe()
  }

  handleCambiarPuntaje() {
    this.setPuntajes()
  }

  inicializarEliminarDetalles() {
    this.partidoEquipo1DetDel = {
      alineacion: [],
      puntos: [],
      sustituciones: [],
    }

    this.partidoEquipo2DetDel = {
      alineacion: [],
      puntos: [],
      sustituciones: [],
    }
  }

  eliminarPartido() {
    if (this.partido.fase != 1) {
      this.limpiarPartido()
      return
    }
    console.log(this.TAG, "eliminarPartido")
    this.partidoVoleyService.deleteById(this.partido.id)
      .pipe(take(1))
      .subscribe(data => {
        this.onEliminar.next()
        this.inicializarPartidoVacio()
        this.inicializarEliminarDetalles()

        this.updatePartidosGlobal()
      })
  }

  limpiarPartido() {
    console.log(this.TAG, "limpiarPartido()")
    const partido: VoleyMatchDTO = {
      id: this.partido.id,
      fecha: this.partido.fecha,
      puntaje11: 0,
      puntaje12: 0,
      puntaje21: 0,
      puntaje22: 0,
      puntaje31: 0,
      puntaje32: 0,
      puntaje41: 0,
      puntaje42: 0,
      puntaje51: 0,
      puntaje52: 0,
      campeonatoId: this.partido.campeonatoId,
      estado: this.gs.partidoFutbolEstado.NO_REALIZADO,
      equipo1Id: this.selectedTeam1 || 0,
      equipo2Id: this.selectedTeam2 || 0,
      numeroSets: 3, //valor por defecto
      fase: this.partido.fase, //revisar asignacion
      grupo: "", //revisar asignacion
    }

    this.partidoVoleyService.save(partido)
      .pipe(take(1))
      .subscribe(data => {
        //console.log("partido guardado", data)

        this.onGuardar.next()
        this.inicializarPartidoVacio()
        this.inicializarEliminarDetalles()
      })

    //En este caso incluye todos los detalles
    const detalle: VoleyMatchDetail = {
      alineacion: [...this.equipo1.alineacion, ...this.equipo2.alineacion],
      puntos: this.fusionarDetallesPorSet(this.detallesPorSet1.puntosPorSet, this.detallesPorSet2.puntosPorSet),
      sustituciones: [...this.equipo1.sustituciones, ...this.equipo2.sustituciones],
    }
    this.partidoVoleyService.deletePartidoDetalle(detalle)
      .pipe(take(1))
      .subscribe()

    const detalleEliminar: VoleyMatchDetail = {
      alineacion: [...this.partidoEquipo1DetDel.alineacion, ...this.partidoEquipo2DetDel.alineacion],
      puntos: [...this.partidoEquipo1DetDel.puntos, ...this.partidoEquipo2DetDel.puntos],
      sustituciones: [...this.partidoEquipo1DetDel.sustituciones, ...this.partidoEquipo2DetDel.sustituciones],
    }
    this.partidoVoleyService.deletePartidoDetalle(detalleEliminar)
      .pipe(take(1))
      .subscribe()
  }

  ngOnDestroy(): void {
    this.destroy$.next()
    this.destroy$.complete()
  }
  getFaseValue(phase: string): number {
    switch (phase) {
      case 'Octavos': return this.gs.Fase.OCTAVOS;
      case 'Cuartos': return this.gs.Fase.CUARTOS;
      case 'Semifinal': return this.gs.Fase.SEMIFINAL;
      case 'Final': return this.gs.Fase.FINAL;
      default: return -1; // O algún valor por defecto
    }
  }

  updatePartidosGlobal() {
    this.partidoVoleyService.findByCampeonatoId(this.campeonatoId)
      .pipe(take(1))
      .subscribe(data => {
        this.gs.updatePartidos(data)
      })
  }

  soloVista(on: boolean) {
    if (!on) return
    if (this.editableBoxComponents) {
      console.log('editable', this.editableBoxComponents)
      this.editableBoxComponents.toArray().forEach((component) => {
        this.renderer.setProperty(component, 'isDisabled', true)
      })
    }

    const elementos = this.el.nativeElement.querySelectorAll('#btnEditar')
    elementos.array.forEach((element: any) => {
      this.renderer.setProperty(element, 'hidden', true)
    });
  }
}
