import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarResultadoVoleyComponent } from './editar-resultado-voley.component';

describe('EditarResultadoVoleyComponent', () => {
  let component: EditarResultadoVoleyComponent;
  let fixture: ComponentFixture<EditarResultadoVoleyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditarResultadoVoleyComponent]
    });
    fixture = TestBed.createComponent(EditarResultadoVoleyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
