import { Input } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { combineLatest, Observable, Subject, Subscription, take, takeUntil } from 'rxjs'; // Importa Subscription de rxjs
import { RolUsuarioService } from '../rol-usuario.service';
import { Campeonato, CampeonatoService } from '../services/campeonato.service';
import { EquipoCampeonato, EquipoCampeonatoService } from '../services/equipo-campeonato.service';
import { Equipo, EquipoService } from '../services/equipo.service';
import { PartidosVolleyService } from '../services/partidos-volley.service';
import { PartidosService } from '../services/partidos.service';
import { PlayerService } from '../services/player.service';
import { AuthService } from '@auth0/auth0-angular';
@Component({
  selector: 'app-menu2',
  templateUrl: './menu2.component.html',
  styleUrls: ['./menu2.component.css']
})
export class Menu2Component implements OnInit {

  idCampeonato: number = -1;

  campeonato$!: Observable<Campeonato>;
  private destroy$ = new Subject<void>()

  nuevoCampeonatoOpen= true;
  isLoggedIn= true;
  esAdministrador = false;

  deporte = this.gs.Deporte;

  equiposEnCampeonato: Equipo[] = []

  private subscription!: Subscription;

  vistapartidoisopen = false;
  menuItems = {
    inicioMenu: true,
    clasificacionMenu: false,

  };

  constructor(
    private route: ActivatedRoute,
    private rolUsuarioService: RolUsuarioService,
    private campeonatoService: CampeonatoService,
    private equipoCampeonatoService: EquipoCampeonatoService,
    private equipoService: EquipoService,
    private partidoVoleyService: PartidosVolleyService,
    private partidoFutbolService: PartidosService,
    private jugadorService: PlayerService,
    private router: Router,
    private gs: GlobalService,
    public auth: AuthService,
  ) {}
  menuItemsCenter = {
    vistaPartido: false,
    vistaClasificacion: true,
  };

  ngOnInit(): void {

    this.subscription = this.rolUsuarioService.esAdministrador$.subscribe(esAdministrador => {
      this.esAdministrador = true;
    });

    this.route.queryParamMap.subscribe(paramMap => {
      const id = paramMap.get('id');
      console.log('ID recibido:', id);
      this.idCampeonato = Number(id);
      this.gs.setCampeonatoSelected(this.idCampeonato);//obetner id
      //Get observador Campeonato de forma asíncrona
      //Este observador es usado en el primer bloque del HTML
      this.campeonato$ = this.campeonatoService.getCampeonato(this.idCampeonato)

      //Precargar partidos
      this.campeonato$
        .pipe(take(1))
        .subscribe(data => {
          let tipoCampeonato = data.tipoCampeonato

          if (tipoCampeonato == this.gs.Deporte.FUTBOL)
            this.precargarPartidosFutbol()
          
          if (tipoCampeonato == this.gs.Deporte.VOLEY)
            this.precargarPartidosVoley()
        })

      //Cargar equipos en campeonato
      combineLatest([
        this.equipoCampeonatoService.findByCampeonatoId(this.idCampeonato),
        this.equipoService.getEquipos()
      ])
      .pipe(take(1))
      .subscribe(([equipoCampeonatoData, equiposData]) => {
        console.log("equipos de campeonato observer")
        let equipos = this.campeonatoService.getEquiposEnCampeonato(equiposData, equipoCampeonatoData)
        this.equiposEnCampeonato = equipos
        this.gs.updateEquipoCampeonato(equipos)
        this.precargarJugadoresEnCampeonato()
        
      });
    });
  }

  precargarPartidosFutbol(){
    this.partidoFutbolService.getPartidosByCampeonato(this.idCampeonato)
      .pipe(take(1))
      .subscribe(data => {
        
        this.gs.updatePartidos(data)
        console.log('partidos de futbol', data)
      })
  }

  precargarPartidosVoley(){
    this.partidoVoleyService.findByCampeonatoId(this.idCampeonato)
      .pipe(take(1))
      .subscribe(data => {
        this.gs.updatePartidos(data)
      })
  }

  precargarJugadoresEnCampeonato(){
    //Precargar jugadores en campeonato
    this.jugadorService.getPlayers()
    .pipe(take(1))
    .subscribe(data => {
      this.gs.jugadoresEnCampeonato = this.campeonatoService.getJugadoresEnCampeonato(this.equiposEnCampeonato, data)
      console.log('precargaJugadores', data)
    })
  }

  resetMenuItems() {
    Object.keys(this.menuItems).forEach((key: string) => {
      // Afirmación de tipo como índice del objeto menuItems
      (this.menuItems as {[key: string]: boolean})[key] = false;
    });
  }

  resetMenuItemCenter() {
    Object.keys(this.menuItemsCenter).forEach((key: string) => {
      // Afirmación de tipo como índice del objeto menuItems
      (this.menuItemsCenter as {[key: string]: boolean})[key] = false;
    });
  }

  onVistaPartidoOpen() {
    this.resetMenuItemCenter();
    this.menuItemsCenter.vistaPartido = true;

  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
    this.destroy$.next()
    this.destroy$.complete()
  }

  toMenu1(){
    this.router.navigate(["/menu1"])
  }
  LogoutwithAuth0(){
    const returnToUrl = window.location.origin; // Detecta el origen dinámicamente
    this.auth.logout({ logoutParams: { returnTo: returnToUrl } });
  }
}
