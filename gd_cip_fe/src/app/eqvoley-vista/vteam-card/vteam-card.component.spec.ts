import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VteamCardComponent } from './vteam-card.component';

describe('VteamCardComponent', () => {
  let component: VteamCardComponent;
  let fixture: ComponentFixture<VteamCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VteamCardComponent]
    });
    fixture = TestBed.createComponent(VteamCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
