import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-vteam-card',
  templateUrl: './vteam-card.component.html',
  styleUrls: ['./vteam-card.component.css']
})
export class VteamCardComponent {
  @Input() team: any;
}
