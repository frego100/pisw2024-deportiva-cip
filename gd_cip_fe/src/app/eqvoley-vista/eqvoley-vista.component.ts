import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { forkJoin, Subject, takeUntil, switchMap, of, map, EMPTY, Observable, tap, from, mergeMap, toArray } from 'rxjs';
import { ClasificacionService } from '../services/clasificacion.service';
import { Alineacion, EquipoVistaService, Gol } from '../services/equipo-vista.service';
import { PartidosService } from 'src/app/services/partidos.service';
import { TablaPartidosVoleyComponent } from '../tabla-partidos-voley/tabla-partidos-voley.component';


import { GlobalService } from '../services/global.service';
import { group } from '@angular/animations';
import { Campeonato } from '../services/lista-campeonatos.service';
import { PartidosVolleyService, VoleyMatchDTO } from '../services/partidos-volley.service';

@Component({
  selector: 'app-eqvoley-vista',
  templateUrl: './eqvoley-vista.component.html',
  styleUrls: ['./eqvoley-vista.component.css']
})
export class EqvoleyVistaComponent implements OnInit, OnDestroy {

  @Input() campeonato!: Campeonato;
  @Input() setsGanadosEquipo1!: number;
  @Input() setsGanadosEquipo2!: number;
  
  private campeonatoId!: number;
  private destroy$ = new Subject<void>();

  selectedView: string = 'admin';
  /* Estrcutura de los datos del equipo*/
  team = {
    name: '',
    wins: 0,
    draws: 0,
    losses: 0,
    image: '/assets/team1-icon-voley.png'
  };

  players: any[] = [];
  matches: any[] = [];
  private filteredMatches: any[] = []; 


  constructor(
    private clasificacionService: ClasificacionService,
    private gs: GlobalService,
    private location: Location,
    private equipoVistaService: EquipoVistaService,
    private partidosService: PartidosService,
    private pvs: PartidosVolleyService,
  ) {}

  ngOnInit(): void {
    this.gs.campeonatoSelected$
      .pipe(
        takeUntil(this.destroy$),
        switchMap(id => {
          if (id === -1) {
            console.warn('ID de campeonato no disponible');
            return of([]); 
          }
          this.campeonatoId = id;
          console.log('ID recibido en EquipoVistaComponent:', id);

          const selectedTeam = this.clasificacionService.getSelectedTeam();
          if (!selectedTeam) return EMPTY;


          return forkJoin({
            equipoData: this.loadEquipoData(selectedTeam),
            partidos: this.loadPartidos(selectedTeam),
          });
        })
      )
      .subscribe({
        next: data => console.log('Datos cargados correctamente', data),
        error: (err) => console.error('Error al cargar los datos:', err)
      });
  }

  loadEquipoData(selectedTeam: any): Observable<any>{
    return of(selectedTeam).pipe(
          tap(team => {
            this.team = {
              name: team.name,
              wins: team.wins,
              draws: team.draws,
              losses: team.loses,
              image: '/assets/team1-icon-voley.png'
            };
          })
        );
  }

  loadPartidos(selectedTeam: any): Observable<any> {
    return this.pvs.findByCampeonatoId(this.campeonatoId).pipe(
      tap(partidos => {
        // Filtrar partidos por equipo
        this.filteredMatches = partidos.filter(match =>
          match.equipo1Nombre && match.equipo2Nombre &&
          (match.equipo1Nombre === selectedTeam.name || match.equipo2Nombre === selectedTeam.name)
        );
        // Mapear para incluir iconos
        this.matches = this.filteredMatches.map(match => ({
          ...match,
          equipo1: { nombre: match.equipo1Nombre, icon1: '/assets/team1-icon-voley.png' },
          equipo2: { nombre: match.equipo2Nombre, icon2: '/assets/team2-icon-voley.png' }
        }));
      }),
      // Cargar jugadores y actualizar estadísticas
      switchMap(() => this.loadJugadorYActualizarStats(selectedTeam))
    );
  }

  loadJugadorYActualizarStats(selectedTeam: any): Observable<any> {
    const equipoId = selectedTeam.id;
  
    // Verificar que el Map contiene datos para el campeonato actual
    const jugadoresCampeonato = this.gs.jugadoresEnCampeonato.get(equipoId);
  
    if (!jugadoresCampeonato) {
      console.error('No se encontraron jugadores para el campeonato:', this.campeonatoId);
      return EMPTY;
    }
  
    // Continuar con la actualización de estadísticas
    const partidoIds = this.filteredMatches.map(match => match.id);
  
    return this.updatePlayerStats(partidoIds, selectedTeam).pipe(
      tap(datosCombinados => {
        const jugadoresEnAlineacion = datosCombinados.alineacion.map((alineado: { jugadorId: any }) => alineado.jugadorId);
  
        // Filtrar jugadores por alineación
        const jugadoresFiltrados = jugadoresCampeonato.filter(jugador => 
          jugadoresEnAlineacion.includes(jugador.id)
        );
  
        // Mapear los jugadores filtrados al formato esperado
        this.players = jugadoresFiltrados.map(jugador => ({
          id: jugador.id,
          icon: '/assets/perfil.jpg',
          name: jugador.name || 'Desconocido',
          nrocamiseta: jugador.number || '',
          posicion: jugador.position || 'Sin posicion',
          goals: datosCombinados.puntos.filter((gol: { jugadorId: any; }) => gol.jugadorId === jugador.id).reduce((total: any, gol: { puntos: any; }) => total + gol.puntos,0),
          matches: datosCombinados.alineacion.filter((alineado: { jugadorId: any }) => alineado.jugadorId === jugador.id).length
        }));
      })
    );
  }


  updatePlayerStats(partidoIds: number[], selectedTeam: any): Observable<any> {
      return from(partidoIds).pipe(
        mergeMap(id => this.pvs.findPartidoDetalle(id, selectedTeam.id), 5),
        toArray(),
        map(partidoDataArray => this.combinarPartidoData(partidoDataArray)),
        tap(datosCombinados => {
          this.players.forEach(player => {
            player.goals = datosCombinados.puntos.filter((gol: { jugadorId: any; }) => gol.jugadorId === player.id).reduce((total: any, gol: { puntos: any; }) => total + gol.puntos,0);
            player.matches = datosCombinados.alineacion.filter((jugada: { jugadorId: any; }) => jugada.jugadorId === player.id).length;
          });
        })
      );
    }

  combinarPartidoData(partidoDataArray: any[]) {
    return partidoDataArray.reduce((acc, data) => {
      acc.puntos.push(...data.puntos);
      acc.alineacion.push(...data.alineacion);
      return acc;
    }, {
      puntos: [], alineacion: []
    });
  }
  

  goBack(): void{
    this.location.back();
  }
  
  /* Cambio de vista entre admin y administracion*/
  onRadioChange(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.selectedView = inputElement.value;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

}


