import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EqvoleyVistaComponent } from './eqvoley-vista.component';

describe('EqvoleyVistaComponent', () => {
  let component: EqvoleyVistaComponent;
  let fixture: ComponentFixture<EqvoleyVistaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EqvoleyVistaComponent]
    });
    fixture = TestBed.createComponent(EqvoleyVistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
