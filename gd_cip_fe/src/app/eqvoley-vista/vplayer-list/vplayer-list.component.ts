import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-vplayer-list',
  templateUrl: './vplayer-list.component.html',
  styleUrls: ['./vplayer-list.component.css']
})
export class VplayerListComponent {
  @Input()
  players: any[] = [];

  @Input()
  viewMode: string = 'user'; //solo maneja las vistas

  constructor(public dialog: MatDialog) {} /*----------Mat dialog para abrir el modal de editar--------*/

  openEditModal() {
   // Clonar el objeto para evitar cambios directos
    // Lógica para abrir el modal (puedes usar Angular Material Dialog)
  }

}
