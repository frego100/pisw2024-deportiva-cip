import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VplayerListComponent } from './vplayer-list.component';

describe('VplayerListComponent', () => {
  let component: VplayerListComponent;
  let fixture: ComponentFixture<VplayerListComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VplayerListComponent]
    });
    fixture = TestBed.createComponent(VplayerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
