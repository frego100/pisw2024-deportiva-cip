import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GestInfoCampComponent } from 'src/app/gest-info-camp/gest-info-camp.component';

@Component({
  selector: 'app-vmatch-summary',
  templateUrl: './vmatch-summary.component.html',
  styleUrls: ['./vmatch-summary.component.css']
})
export class VmatchSummaryComponent {

  @Input() match: any;
  @Input()
  viewMode: string = 'user'; //Variable para cambiar de entre user y admin
 

  //numColumnas: number = 5;
  //columnas: number[] = [];

  constructor(
    public dialog: MatDialog)
     {
      //this.ajustarColumnas();
     }


  /*ajustarColumnas(): void {
      // Generar columnas dinámicas
      this.columnas = Array(this.numColumnas).fill(0).map((x, i) => i + 1);
  }*/

  getColumnClass(index: number): string {
    // Alterna entre las clases 'columna-blanca' y 'columna-gris' dependiendo del índice
    return index % 2 === 0 ? 'columna-blanca' : 'columna-gris';
  }

//Tambien pertenece al cambio de estado, que pertenece  a gest-info-camp
  getStateClass(state: string) {
    return {
      'no-realizado': state === 'NO REALIZADO',
      'preparacion':state === 'PREPARACION',
      'en-vivo': state === 'EN VIVO',
      'finalizado': state === 'REALIZADO'
    };
  }

   // Método para obtener el puntaje de cada set dinámicamente
   getSetScore(setNumber: number, team: 'equipo1' | 'equipo2'): number | null {
    return this.match ? this.match[`puntaje${setNumber}${team === 'equipo1' ? '1' : '2'}`] : null;
  }

   // Por ahora replique el metodo de calculo, debo hallar una forma para returilzar el otro valor que esta en tabla-partidos
   calculateSetsWon(match: any, equipo: 'equipo1' | 'equipo2'): number {
    let setsWon = 0;

    if (equipo === 'equipo1') {
      if (match.puntaje11 > match.puntaje12) setsWon++;
      if (match.puntaje21 > match.puntaje22) setsWon++;
      if (match.puntaje31 > match.puntaje32) setsWon++;
      if (match.puntaje41 > match.puntaje42) setsWon++;
      if (match.puntaje51 > match.puntaje52) setsWon++;
    } else {
      if (match.puntaje12 > match.puntaje11) setsWon++;
      if (match.puntaje22 > match.puntaje21) setsWon++;
      if (match.puntaje32 > match.puntaje31) setsWon++;
      if (match.puntaje42 > match.puntaje41) setsWon++;
      if (match.puntaje52 > match.puntaje51) setsWon++;
    }

    return setsWon;
  }

}
