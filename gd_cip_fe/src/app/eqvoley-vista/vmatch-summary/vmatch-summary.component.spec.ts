import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VmatchSummaryComponent } from './vmatch-summary.component';

describe('VmatchSummaryComponent', () => {
  let component: VmatchSummaryComponent;
  let fixture: ComponentFixture<VmatchSummaryComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VmatchSummaryComponent]
    });
    fixture = TestBed.createComponent(VmatchSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
