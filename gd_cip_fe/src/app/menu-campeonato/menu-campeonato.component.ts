import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu-campeonato',
  templateUrl: './menu-campeonato.component.html',
  styleUrls: ['./menu-campeonato.component.css']
})
export class MenuCampeonatoComponent {
  nombreOlimpiada = "XII OLIMPIADA INTERNA 2024";
  nombreTorneo = "FUTBOL 11"
  organizador = "ING. HUGO MURILLO ZEGARRA";
  equipos = [
    { nombre: 'Equipo 1' },
    { nombre: 'Equipo 2' },
    { nombre: 'Equipo 3' },
    { nombre: 'Equipo 4' },
    { nombre: 'Equipo 5' },
    { nombre: 'Equipo 6' }, 
  ];
}
