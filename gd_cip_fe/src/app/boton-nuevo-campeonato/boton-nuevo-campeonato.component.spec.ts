import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BotonNuevoCampeonatoComponent } from './boton-nuevo-campeonato.component';

describe('BotonNuevoCampeonatoComponent', () => {
  let component: BotonNuevoCampeonatoComponent;
  let fixture: ComponentFixture<BotonNuevoCampeonatoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BotonNuevoCampeonatoComponent]
    });
    fixture = TestBed.createComponent(BotonNuevoCampeonatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
