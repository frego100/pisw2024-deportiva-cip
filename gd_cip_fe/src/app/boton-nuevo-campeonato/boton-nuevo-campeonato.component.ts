import { Component, OnInit, TemplateRef, ViewChild, Input} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CampeonatoService, Campeonato } from '../services/campeonato.service';

@Component({
  selector: 'app-boton-nuevo-campeonato',
  templateUrl: './boton-nuevo-campeonato.component.html',
  styleUrls: ['./boton-nuevo-campeonato.component.css']
})
export class BotonNuevoCampeonatoComponent implements OnInit{
  @ViewChild('campeonatoDialog') campeonatoDialog!: TemplateRef<any>;


  campeonatos: Campeonato[] = [];
  filteredCampeonatos: Campeonato[] = [];
  searchTerm: string = '';
  campeonatoForm: FormGroup;
  isEditing: boolean = false;
  currentCampeonatoId?: number;

  desplegablesCampeonatos: boolean[] = [];

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private CampeonatoService: CampeonatoService,
    private route: ActivatedRoute
  ) {
    this.campeonatoForm = this.fb.group({
      tituloCampeonato: [''],
      tipoCampeonato: [''],
      fechaInicio: [''],
      fechaFin: [''],
      organizadorNombre: [''],
      organizadorEmail: [''],
      organizadorNumero: [''],
    });
  }

  ngOnInit(): void {
    this.loadCampeonato();

  }

  navigateToMenu2(id: number) {
    console.log(id);
    this.router.navigate(['/menu2'], { queryParams: { id: id.toString() }});
  }
  
  loadCampeonato(): void {
    this.CampeonatoService.getCampeonatos().subscribe(campeonatos => {
      console.log(campeonatos);
      this.campeonatos = campeonatos;
      this.filteredCampeonatos = campeonatos
      this.desplegablesCampeonatos = new Array(this.campeonatos.length).fill(false);
      console.log(this.desplegablesCampeonatos);
    });
  }
  //Metodos de Dialogo
  openAddCampeonatoDialog(): void {
    this.isEditing = false;
    this.campeonatoForm.reset();
    this.dialog.open(this.campeonatoDialog);
  }
  
  openEditCampeonatoDialog(campeonato: Campeonato): void {
    this.isEditing = true;
    this.currentCampeonatoId = campeonato.id;
    this.campeonatoForm.patchValue(campeonato);
    this.dialog.open(this.campeonatoDialog);
  }
  
  closeDialog(): void {
    this.dialog.closeAll();
  }

  //Manipulacion de Campeonatos
  saveCampeonato(): void {
    if (this.isEditing && this.currentCampeonatoId !== undefined) {
      const updateCampeonato: Campeonato = { id: this.currentCampeonatoId, ...this.campeonatoForm.value };
      this.CampeonatoService.updateCampeonato(updateCampeonato).subscribe(() => {
        this.loadCampeonato();
        this.dialog.closeAll();
      });
    } else {
      const newCampeonato: Campeonato = { ...this.campeonatoForm.value };
      this.CampeonatoService.addCampeonato(newCampeonato).subscribe(() => {
        this.loadCampeonato();
        this.dialog.closeAll();
      });
    }
  }
  

  filterCampeonatos(): void {
    this.filteredCampeonatos = this.campeonatos.filter(campeonato =>
      campeonato.tituloCampeonato.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
  }


}
