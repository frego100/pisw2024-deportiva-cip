import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '@auth0/auth0-angular';
import { Observable, of } from 'rxjs';
import { tap, switchMap } from 'rxjs/operators';
import { UsuarioService } from './services/usuario.service';
import { GlobalService } from './services/global.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private auth: AuthService,
    private router: Router,
    private usuarioService: UsuarioService,
    private gs: GlobalService
  ) {}

  canActivate(): Observable<boolean> {
    return this.usuarioService.isAuthenticated().pipe(
      switchMap(isAuth0Authenticated => {
        if (isAuth0Authenticated) {
          return this.auth.isAuthenticated$;
        } else {
          return of(this.gs.isCIPAuthenticated);
        }
      }),
      tap(isAuthenticated => {
        if (!isAuthenticated) {
          // Redirigir al usuario a la página de login personalizada
          this.router.navigate(['/user-login']);
        }
      })
    );
  }
}
