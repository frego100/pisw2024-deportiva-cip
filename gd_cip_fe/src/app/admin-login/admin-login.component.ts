import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UsuarioAdminService } from '../services/usuario-admin.service';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { RolUsuarioService } from '../rol-usuario.service';

declare var FB: any;

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  loginForm!: FormGroup;
  hidePassword: boolean = true; // Controlar la visibilidad de la contraseña

  constructor(
    private fb: FormBuilder,
    private service: UsuarioAdminService,
    private router: Router,
    private http: HttpClient,
    private gs: GlobalService,
    private rs: RolUsuarioService,
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      cip: [''],
      email: ['', Validators.required],
      contraseña: ['', Validators.required]
    });
    // Inicializar el SDK de Facebook
    (window as any).fbAsyncInit = function() {
      FB.init({
        appId      : '1621008412083335', // Reemplaza con tu App ID
        cookie     : true,
        xfbml      : true,
        version    : 'v11.0'
      });
    };

    (function(d, s, id) {
      let js: HTMLScriptElement;
      const fjs = d.getElementsByTagName(s)[0] as HTMLScriptElement;
      if (d.getElementById(id)) { return; }
      js = d.createElement(s) as HTMLScriptElement; js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      if (fjs && fjs.parentNode) {
        fjs.parentNode.insertBefore(js, fjs);
      }
    }(document, 'script', 'facebook-jssdk'));
  }

  togglePasswordVisibility(): void {
    this.hidePassword = !this.hidePassword;
  }
  
  onLogin(): void {
    const v = this.loginForm.value;

    if (this.loginForm.valid) {
      // Lógica de inicio de sesión aquí
      console.log('Formulario de administrador válido', this.loginForm.value);
      
      let request;
      request = this.service.loginAdmin(v);
      request.subscribe((data) => {
        console.log(data);

        if (data != null){
          this.router.navigate(['/menu1'])
          this.gs.adminView = true
          this.gs.nombreUsuario = data.nombre
          this.rs.cambiarEsAdministrador(true)
        }
      });
    } else {
      console.log('Formulario de administrador inválido');
    }
  }
  
  sendToBackend(accessToken: string) {
    // Aquí puedes usar HttpClient para enviar el token al backend
    this.http.post('http://localhost:8080/api/auth/facebook', { accessToken })
      .subscribe(response => {
        console.log('Datos enviados al backend: ', { accessToken });
        console.log('Respuesta del servidor:', response);
      }, error => {
        console.error('Error al enviar el token al backend:', error);
      });
  }
}
