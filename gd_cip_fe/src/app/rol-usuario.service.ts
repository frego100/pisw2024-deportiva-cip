import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RolUsuarioService {
  private esAdministradorSubject = new BehaviorSubject<boolean>(false); // Inicializado con false, indicando que el usuario no es administrador por defecto
  esAdministrador$ = this.esAdministradorSubject.asObservable(); // Observable público para suscripciones

  constructor() {}

  cambiarEsAdministrador(esAdministrador: boolean): void {
    this.esAdministradorSubject.next(esAdministrador);
  }
}