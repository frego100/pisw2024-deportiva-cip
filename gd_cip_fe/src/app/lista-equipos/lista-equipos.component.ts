import { Component, OnInit, TemplateRef, ViewChild, Input} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PlayerService } from '../services/player.service';
import { EquipoService, Equipo } from '../services/equipo.service';
import { RoleService } from '../services/role.service';


interface Player {
  id?: number;
  name: string;
  shortName: string;
  position: string;
  number: string;
  document: string;
  birthDate: Date;
  phone: string;
  email: string;
  equipoID: number;
}

@Component({
  selector: 'app-lista-equipos',
  templateUrl: './lista-equipos.component.html',
  styleUrls: ['./lista-equipos.component.css']
})
export class ListaEquiposComponent implements OnInit {

  @ViewChild('equipoDialog') equipoDialog!: TemplateRef<any>;
  @Input() esAdministrador: boolean = true;
  

  players: Player[] = [];
  equipos: Equipo[] = [];
  filteredEquipos: Equipo[] = [];
  searchTerm: string = '';
  equipoForm: FormGroup;
  isEditing: boolean = false;
  currentEquipoId?: number;


  desplegablesEquipos: boolean[] = [];

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private playerService: PlayerService,
    private equipoService: EquipoService,
    private roleService: RoleService,

  ) { 
    this.equipoForm = this.fb.group({
      email: [''],
      nombre: [''],
      entrenador: [''],
      delegadoNombre1: [''],
      delegadoTelefono1: [''],
      delegadoNombre2: [''],
      delegadoTelefono2: [''],
      delegadoNombre3: [''],
      delegadoTelefono3: [''],
      delegadoNombre4: [''],
      delegadoTelefono4: [''],
      delegadoNombre5: [''],
      delegadoTelefono5: [''],
    });
   }
  
  ngOnInit(): void {
    this.loadPlayers();
    this.loadEquipos();
    this.esAdministrador = this.roleService.hasRole('Administrador');
    if(!this.esAdministrador){
      this.equipoForm.disable();
    }

  
  }


  loadPlayers(): void {
    this.playerService.getPlayers().subscribe(players => {
      console.log(players);
      this.players = players;
    });
  }
  loadEquipos(): void {
    this.equipoService.getEquipos().subscribe(equipos => {
      console.log(equipos);
      this.equipos = equipos;
      this.filteredEquipos = equipos
      this.desplegablesEquipos = new Array(this.equipos.length).fill(false);
      console.log(this.desplegablesEquipos);
    });
  }
  //Metodos de Dialogo
  openAddEquipoDialog(): void {
    this.isEditing = false;
    this.equipoForm.reset();
    this.dialog.open(this.equipoDialog);
  }
  
  openEditEquipoDialog(equipo: Equipo): void {
    this.isEditing = true;
    this.currentEquipoId = equipo.id;
    this.equipoForm.patchValue(equipo);
    this.dialog.open(this.equipoDialog);
  }
  
  closeDialog(): void {
    this.dialog.closeAll();
  }

  //Manipulacion de Equipos
  saveEquipo(): void {
    const nombreEquipo = this.equipoForm.get('nombre')?.value;

    // Validación de duplicados
    const nombreDuplicado = this.equipos.some(
        equipo => equipo.nombre === nombreEquipo && equipo.id !== this.currentEquipoId
    );

    if (nombreDuplicado) {
        alert('El nombre del equipo ya está en uso. Por favor, elige otro nombre.');
        return;
    }

    if (this.isEditing && this.currentEquipoId !== undefined) {
        const updateEquipo: Equipo = { id: this.currentEquipoId, ...this.equipoForm.value };
        this.equipoService.updateEquipo(updateEquipo).subscribe(() => {
            this.loadEquipos();
            this.dialog.closeAll();
        });
    } else {
        const newEquipo: Equipo = { ...this.equipoForm.value };
        this.equipoService.addEquipo(newEquipo).subscribe(() => {
            this.loadEquipos();
            this.dialog.closeAll();
        });
    }
}

  
  deleteEquipo(): void {
    if (this.isEditing && this.currentEquipoId !== undefined) {
      this.equipoService.deleteEquipo(this.currentEquipoId).subscribe(() => {
        this.loadEquipos();
        this.dialog.closeAll();
      });
    }
  }

  filterEquipos(): void {
    this.filteredEquipos = this.equipos.filter(equipo =>
      equipo.nombre.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
  }
  filterPlayersByEquipo(equipoID: number): Player[] {
    return this.players.filter(player => player.equipoID === equipoID);
  }
  toggleDesplegable(index: number): void {
    this.desplegablesEquipos[index] = !this.desplegablesEquipos[index];
  }
  //Barra que se despliega al hacer click a un equipo ya sea para ver sus jugadores o editar caracteristicas de equipo
  desplegableEquipos = {
    edicion: false,
    listadoJugadores: false,
  };
  resetDesplegableEquipos() {
    Object.keys(this.desplegableEquipos).forEach((key: string) => {
      // Afirmación de tipo como índice del objeto menuItems
      (this.desplegableEquipos as {[key: string]: boolean})[key] = false;
    });
  }
  clickNombreEquipo(){
    if(this.desplegableEquipos.edicion == true){
      this.resetDesplegableEquipos();
      this.desplegableEquipos.listadoJugadores = true;
    }
    else if(this.desplegableEquipos.listadoJugadores == true){
      this.resetDesplegableEquipos();
    }
    else{
      this.desplegableEquipos.listadoJugadores = true;
    }
  }
  clickBotonEdicion(){
    if(this.desplegableEquipos.edicion == true){
      this.resetDesplegableEquipos();
    }
    else if(this.desplegableEquipos.listadoJugadores == true){
      this.resetDesplegableEquipos();
      this.desplegableEquipos.edicion = true;
    }
    else{
      this.desplegableEquipos.edicion = true;
    }
  }

  //Final del desplegable por equipos

  
  
}
