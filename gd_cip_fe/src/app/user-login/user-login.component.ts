import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from '../services/usuario.service';
import { GlobalService } from '../services/global.service';
import { AuthService } from '@auth0/auth0-angular';
import { RoleService } from '../services/role.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {
  loginForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private service: UsuarioService,
    private gs: GlobalService,
    public auth: AuthService,
    private roleService: RoleService
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      cip: ['', Validators.required]
    });
    this.auth.isAuthenticated$.subscribe(isAuthenticated => {
      if (isAuthenticated) {
        this.router.navigate(['/menu1']);
      }
    });
  }

  login_auth0() {
    // Redirige a la autenticación de Auth0
    this.auth.loginWithRedirect({
      authorizationParams: {
        redirect_uri: window.location.origin+'/callback'
      }
    });
  }

  onLogin(): void {
    const v = this.loginForm.value;

    if (this.loginForm.valid) {
      // Lógica de inicio de sesión por CIP
      this.service.findByCip(v.cip).subscribe((data) => {
        if (data != null) {
          sessionStorage.setItem("usuarioCipId", `${data.id}`)
          this.gs.adminView = false;
          this.gs.nombreUsuario = data.nombre;
          this.gs.isCIPAuthenticated = true; // Variable para indicar que el usuario se autenticó por CIP
          this.roleService.setCIPRole(); // Establecer el rol de CIP
          this.router.navigate(['/menu1']);
        }
      });
    } else {
      console.log('Formulario de usuario inválido');
    }
  }
  logout(): void {
    this.gs.logout();
    this.auth.logout();
    this.router.navigate(['/user-login']);
  }
}
