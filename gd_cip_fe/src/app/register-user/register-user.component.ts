import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterUserService } from '../services/register-user.service';



@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  registerForm!: FormGroup;

  constructor(private fb: FormBuilder, private router: Router, private userService: RegisterUserService) {}

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      cellphone: [''],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required]
    }, { validators: this.matchPasswords });
  }

  get matchPasswords() {
    return (group: FormGroup) => {
      const passwordField = group.get('password');
      const confirmPasswordField = group.get('confirmPassword');
  
      // Verifica que ambos campos existan antes de compararlos
      if (passwordField && confirmPasswordField) {
        const password = passwordField.value;
        const confirmPassword = confirmPasswordField.value;
        return password === confirmPassword ? null : { notSame: true };
      } else {
        // Retorna un error si alguno de los campos no existe
        return { invalidFields: true };
      }
    };
  }

  onRegister(): void {
    if (this.registerForm.valid) {
      const userData = this.registerForm.value;
      console.log(userData)

      this.userService.register(userData).subscribe(
        response => {
          console.log('Registro exitoso:', response);
          // Aquí puedes redirigir al usuario a otra página después de un éxito
          this.router.navigate(['/success-page']); // Ajusta la ruta según sea necesario
        },
        error => {
          console.error('Error durante el registro:', error);
          // Manejo de errores, por ejemplo, mostrar un mensaje al usuario
        }
      );
    } else {
      console.log('Formulario de registro inválido');
    }
  }
}