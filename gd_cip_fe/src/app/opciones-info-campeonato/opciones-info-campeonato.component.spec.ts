import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpcionesInfoCampeonatoComponent } from './opciones-info-campeonato.component';

describe('OpcionesInfoCampeonatoComponent', () => {
  let component: OpcionesInfoCampeonatoComponent;
  let fixture: ComponentFixture<OpcionesInfoCampeonatoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OpcionesInfoCampeonatoComponent]
    });
    fixture = TestBed.createComponent(OpcionesInfoCampeonatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
