import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-opciones-info-campeonato',
  templateUrl: './opciones-info-campeonato.component.html',
  styleUrls: ['./opciones-info-campeonato.component.css']
})
export class OpcionesInfoCampeonatoComponent {
  @Output() onVistaPartido = new EventEmitter <void> ();

  vistaPartido(){
    this.onVistaPartido.emit();
  }
}
