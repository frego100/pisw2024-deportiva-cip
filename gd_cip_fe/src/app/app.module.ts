import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginCipComponent } from './login-cip/login-cip.component';
import { HttpClientModule } from '@angular/common/http';

import { MatDialogModule } from '@angular/material/dialog'//ng add @angular/material
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatOptionModule } from '@angular/material/core'; // Importa el módulo de MatOption
import { MatInputModule } from '@angular/material/input'; // Importa el módulo de MatInput si usas inputs

import { CommonModule } from '@angular/common';

import { CategoriaComponent } from './categoria/categoria.component';
import { CrearCampeonatoComponent } from './crear-campeonato/crear-campeonato.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { MisCampeonatosComponent } from './mis-campeonatos/mis-campeonatos.component';
import { OAuthModule } from 'angular-oauth2-oidc';
import { Menu1Component } from './menu1/menu1.component';
import { Menu2Component } from './menu2/menu2.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { VistaPartidoComponent } from './vista-partido/vista-partido.component';  /*Jess*/
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CrCampCatComponent } from './cr-camp-cat/cr-camp-cat.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { InicioMenu2Component } from './inicio-menu2/inicio-menu2.component';
import { MenuCampeonatoComponent } from './menu-campeonato/menu-campeonato.component';
import { ListaEquiposComponent } from './lista-equipos/lista-equipos.component';
import { AgregarEquipoComponent } from './agregar-equipo/agregar-equipo.component';
import { TablaPartidosComponent } from './tabla-partidos/tabla-partidos.component';
import { EditarResultadoComponent } from './editar-resultado/editar-resultado.component';
import { EditableBoxComponent } from './editable-box/editable-box.component';
import { ClasificacionComponent } from './clasificacion/clasificacion.component';
import { EquipoVistaComponent } from './equipo-vista/equipo-vista.component';
import { TeamCardComponent } from './equipo-vista/team-card/team-card.component';
import { PlayerListComponent } from './equipo-vista/player-list/player-list.component';
import { MatchSummaryComponent } from './equipo-vista/match-summary/match-summary.component';
import { GestInfoCampComponent } from './gest-info-camp/gest-info-camp.component';
import { PlayerEditModalComponent } from './equipo-vista/player-edit-modal/player-edit-modal.component';

import { OpcionesInfoCampeonatoComponent } from './opciones-info-campeonato/opciones-info-campeonato.component';
import { GestionJugadoresComponent } from './gestion-jugadores/gestion-jugadores.component';
import { MatProgressBarModule } from '@angular/material/progress-bar'; // Importar el módulo
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

// Importaciones de Angular Material
import { MatListModule } from '@angular/material/list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ListaCampeonatosComponent } from './lista-campeonatos/lista-campeonatos.component';
import { ListaCampeonatosQueSigoComponent } from './lista-campeonatos-que-sigo/lista-campeonatos-sigo.component';
import { ListaDeportesComponent } from './lista-deportes/lista-deportes.component';
import { ClasificacionUsuarioComponent } from './clasificacion-usuario/clasificacion-usuario.component';
import { EquipoVistaUsuarioComponent } from './equipo-vista-usuario/equipo-vista-usuario.component';
import { BotonNuevoCampeonatoComponent } from './boton-nuevo-campeonato/boton-nuevo-campeonato.component';
import { GestionUsuariosComponent } from './gestion-usuarios/gestion-usuarios.component';
import { MatTooltipModule } from '@angular/material/tooltip';

// angular select
import { NgSelectModule } from '@ng-select/ng-select';
import { TablaPartidosVoleyComponent } from './tabla-partidos-voley/tabla-partidos-voley.component';
import { EditarResultadoVoleyComponent } from './editar-resultado-voley/editar-resultado-voley.component';
import { ClasificacionVoleyComponent } from './clasificacion-voley/clasificacion-voley.component';
import { EqvoleyVistaComponent } from './eqvoley-vista/eqvoley-vista.component';
import { VteamCardComponent } from './eqvoley-vista/vteam-card/vteam-card.component';
import { VplayerListComponent } from './eqvoley-vista/vplayer-list/vplayer-list.component';
import { VmatchSummaryComponent } from './eqvoley-vista/vmatch-summary/vmatch-summary.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { EditableBoxVoleyComponent } from './editable-box-voley/editable-box-voley.component';
import { ConfirmationPopupComponent } from './confirmation-popup/confirmation-popup.component';
// Importa el módulo de paginación
import { MatPaginatorModule } from '@angular/material/paginator';
import { OctavosComponent } from './clasificacion/octavos/octavos.component';
import { CuartosComponent } from './clasificacion/cuartos/cuartos.component';
import { SemifinalesComponent } from './clasificacion/semifinales/semifinales.component';
import { FinalesComponent } from './clasificacion/finales/finales.component';
import { AuthModule } from '@auth0/auth0-angular';
import { MenuCampeonatoCategoriasComponent } from './menu-campeonato-categorias/menu-campeonato-categorias.component';
import { VfinalesComponent } from './clasificacion-voley/vfinales/vfinales.component';
import { VOctavosComponent } from './clasificacion-voley/voctavos/voctavos.component';
import { VCuartosComponent } from './clasificacion-voley/vcuartos/vcuartos.component';
import { VSemifinalesComponent } from './clasificacion-voley/vsemifinales/vsemifinales.component';
import { HasRoleDirective } from './directives/has-role.directive';
import { RoleService } from './services/role.service';
import { MatSnackBarModule } from '@angular/material/snack-bar';

@NgModule({
  declarations: [
    AppComponent,
    LoginCipComponent,
    CategoriaComponent,
    CrearCampeonatoComponent,
    UserLoginComponent,
    AdminLoginComponent,
    MisCampeonatosComponent,
    Menu1Component,
    CrCampCatComponent,
    RegisterUserComponent,
    InicioMenu2Component,
    MenuCampeonatoComponent,
    ListaEquiposComponent,
    AgregarEquipoComponent,
    TablaPartidosComponent,
    EditarResultadoComponent,
    EditableBoxComponent,
    ClasificacionComponent,
    EquipoVistaComponent,
    TeamCardComponent,
    PlayerListComponent,
    MatchSummaryComponent,
    GestInfoCampComponent,
    PlayerEditModalComponent,
    Menu2Component,
    VistaPartidoComponent,
    OpcionesInfoCampeonatoComponent,
    MenuCampeonatoComponent,
    GestionJugadoresComponent,
    ListaCampeonatosComponent,
    ListaCampeonatosQueSigoComponent,
    ListaDeportesComponent,
    ClasificacionUsuarioComponent,
    EquipoVistaUsuarioComponent,
    BotonNuevoCampeonatoComponent,
    GestionUsuariosComponent,
    TablaPartidosVoleyComponent,
    EditarResultadoVoleyComponent,
    ClasificacionVoleyComponent,
    EqvoleyVistaComponent,
    VteamCardComponent,
    VplayerListComponent,
    VmatchSummaryComponent,
    ConfirmationComponent,
    EditableBoxVoleyComponent,
    ConfirmationPopupComponent,
    OctavosComponent,
    CuartosComponent,
    SemifinalesComponent,
    FinalesComponent,
    MenuCampeonatoCategoriasComponent,
    VfinalesComponent,
    VOctavosComponent,
    VCuartosComponent,
    VSemifinalesComponent,
    HasRoleDirective,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    OAuthModule.forRoot(),
    NoopAnimationsModule,
    FormsModule,
    BrowserAnimationsModule,
    CommonModule,
    MatDialogModule,
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatSlideToggleModule,
    MatOptionModule,
    MatInputModule,
    // Importaciones de Angular Material
    MatInputModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgSelectModule,
    MatTooltipModule,
    MatPaginatorModule, // Agregado el módulo de paginación
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatProgressBarModule,
    AuthModule.forRoot({
      domain: 'dev-nhti1ktswj6imfpk.us.auth0.com',
      clientId: 'HCdQS5yFOSg8mB3wMyjafGfQFNC98NpG',
      cacheLocation: 'localstorage', // O 'sessionstorage'
      useRefreshTokens: true,
    }),
  ],
  providers: [ RoleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
