import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, ValidationErrors } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Auth0Service } from '../auth0.service';
import { UsuarioAdminService } from '../services/usuario-admin.service';
import { Usuario } from '../services/usuario-admin.service';
import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
@Component({
  selector: 'app-gestion-usuarios',
  templateUrl: './gestion-usuarios.component.html',
  styleUrls: ['./gestion-usuarios.component.css']
})
export class GestionUsuariosComponent implements OnInit {
  @ViewChild('userDialog') userDialog!: TemplateRef<any>;
  @Input() esAdministrador: boolean = true;
  @ViewChild('confirmationPopup') confirmationPopup!: ConfirmationPopupComponent;
  @ViewChild('confirmationSuccess') confirmationSuccess!: ConfirmationComponent;

  //Confirmación de eliminación
  @ViewChild('confirmationDelete') confirmationDelete!: ConfirmationPopupComponent;
  @ViewChild('confirmationDeleteSuccess') confirmationDeleteSuccess!: ConfirmationComponent;

  usuarios: Usuario[] = [];
  filteredUsuarios: Usuario[] = [];
  searchTerm: string = '';
  userForm: FormGroup;
  isEditing: boolean = false;
  currentUserId?: number;
  showPassword: boolean = false;
  showRepeatPassword: boolean = false;
  weakPassword: boolean = true;
  userRole: string | null = null; // Rol del usuario logueado


  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private s: UsuarioAdminService,
    private auth0Service: Auth0Service
  ) {
    this.userForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      contraseña: ['', [Validators.required, this.validatePasswordStrength]],
      repetirContraseña: ['', Validators.required],
      nombre: ['', Validators.required],
      apellidos: ['', Validators.required],
      fechaNacimiento: ['', Validators.required],
      dni: ['', [Validators.required, this.validateDNI]],
      rol: ['', Validators.required]
    }, { validators: this.passwordsMatchValidator });
  }


  ngOnInit(): void {
    this.loadUsuarios();
    this.loadUserRole();
  }

  loadUsuarios(): void {
    this.auth0Service.getAll().subscribe({
      next: (data) => {
        this.usuarios = data;
        this.filteredUsuarios = this.usuarios;
      },
      error: (error) => {
        console.error('Error al cargar usuarios:', error);
        // Manejar el error, posiblemente mostrando un mensaje al usuario
      }
    });
  }

  openAddUserDialog(): void {
    this.isEditing = false;
    this.userForm.reset();
    this.dialog.open(this.userDialog);
  }

  openEditUserDialog(usuario: Usuario): void {
    this.isEditing = true;
    this.currentUserId = usuario.id;
    this.userForm.patchValue({
      ...usuario,
      repetirContraseña: usuario.contraseña
    });
    this.dialog.open(this.userDialog);
  }

  saveUser(): void {
    if (this.userForm.valid) {
      const { repetirContraseña, ...userData } = this.userForm.value;

      if (this.isEditing && this.currentUserId !== undefined) {
        // Actualización de usuario solo en Auth0
        this.auth0Service.updateUser(this.currentUserId.toString(), {
          email: userData.email,
          name: userData.nombre,
          given_name: userData.nombre,
          family_name: userData.apellidos,
          user_metadata: {
            dni: userData.dni,
            rol: userData.rol,
            fechaNacimiento: userData.fechaNacimiento
          }
        }).subscribe(
          (auth0Response: any) => {
            console.log('✅ Usuario actualizado exitosamente en Auth0');
            const index = this.usuarios.findIndex(u => u.id === this.currentUserId);
            if (index !== -1) {
              this.usuarios[index] = { ...this.usuarios[index], ...userData };
              this.filteredUsuarios = this.usuarios;
            }
            this.dialog.closeAll();
          },
          (error) => console.error('❌ Error al actualizar usuario en Auth0:', error)
        );
      } else {
        // Creación de nuevo usuario
        this.auth0Service.createUser({
          email: userData.email,
          password: userData.contraseña,
          name: userData.nombre,
          given_name: userData.nombre,
          family_name: userData.apellidos,
          user_metadata: {
            dni: userData.dni,
            rol: userData.rol,
            fechaNacimiento: userData.fechaNacimiento
          }
        }).subscribe(
          (auth0Response: any) => {
            console.log('✅ Usuario creado exitosamente en Auth0');

            // Usar insertAdmin sin ID para creación
            this.s.insertAdmin(userData).subscribe(
              (response: any) => {
                console.log('✅ Usuario creado exitosamente en BD');
                const newUser: Usuario = {
                  ...userData,
                  id: response?.id || (this.usuarios.length + 1)
                };
                this.usuarios.push(newUser);
                this.filteredUsuarios = this.usuarios;
                this.dialog.closeAll();
              },
              (error) => console.error('❌ Error al crear usuario en BD:', error)
            );
          },
          (error) => console.error('❌ Error al crear usuario en Auth0:', error)
        );
      }
    } else {
      console.warn('⚠️ Formulario inválido. Verifica los campos.');
    }
  }

  deleteUser(): void {
    if (this.isEditing && this.currentUserId !== undefined) {
      // Eliminar de Auth0
      this.auth0Service.deleteUser(this.currentUserId.toString()).subscribe(
        () => {
          console.log('✅ Usuario eliminado exitosamente de Auth0');
          this.usuarios = this.usuarios.filter(u => u.id !== this.currentUserId);
          this.filteredUsuarios = this.usuarios;
          this.dialog.closeAll();
        },
        (error) => console.error('❌ Error al eliminar usuario de Auth0:', error)
      );
    }
  }

  filterUsuarios(): void {
    this.filteredUsuarios = this.usuarios.filter(usuario =>
      usuario.nombre.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      usuario.apellidos.toLowerCase().includes(this.searchTerm.toLowerCase()) ||
      usuario.email.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
  }

  closeDialog(): void {
    this.dialog.closeAll();
  }

  private passwordsMatchValidator(control: AbstractControl): ValidationErrors | null {
    const password = control.get('contraseña')?.value;
    const confirmPassword = control.get('repetirContraseña')?.value;
    return password === confirmPassword ? null : { passwordsMismatch: true };
  }

  private validatePasswordStrength(control: AbstractControl): ValidationErrors | null {
    const password = control.value;
    const hasUpperCase = /[A-Z]/.test(password);
    const hasNumber = /\d/.test(password);
    const hasSpecialChar = /[!@#$%^&*(),.?":{}|<>]/.test(password);
    const minLength = password?.length >= 8;

    if (!minLength) {
      return { shortPassword: true };
    }

    if (!hasUpperCase || !hasNumber || !hasSpecialChar) {
      return { weakPassword: true };
    }

    return null;
  }

  loadUserRole(): void {
    this.auth0Service.getCurrentUserRole().subscribe(
      role => {
        this.userRole = role;
        console.log('User Role:', this.userRole);
      },
      error => {
        console.error('Error fetching user role:', error);
      }
    );
  }

  private validateDNI(control: AbstractControl): ValidationErrors | null {
    const dni = control.value;

    // Check if DNI contains only numbers and is exactly 8 characters long
    if (!/^\d+$/.test(dni)) {
      return { invalidDNI: 'El DNI debe contener solo números' };
    }

    if (dni.length !== 8) {
      return { invalidDNI: ', el DNI debe tener exactamente 8 dígitos' };
    }

    return null;
  }

}
