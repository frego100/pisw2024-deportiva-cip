import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipoVistaUsuarioComponent } from './equipo-vista-usuario.component';

describe('EquipoVistaUsuarioComponent', () => {
  let component: EquipoVistaUsuarioComponent;
  let fixture: ComponentFixture<EquipoVistaUsuarioComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EquipoVistaUsuarioComponent]
    });
    fixture = TestBed.createComponent(EquipoVistaUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
