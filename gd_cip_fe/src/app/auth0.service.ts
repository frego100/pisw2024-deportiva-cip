// src/app/services/auth0.service.ts
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { AuthService } from '@auth0/auth0-angular';
import { Usuario } from './services/usuario-admin.service';
export interface Auth0User {
  email: string;
  password: string;
  name?: string;
  given_name?: string;
  family_name?: string;
  user_metadata?: {
    dni?: string;
    rol?: string;
    fechaNacimiento?: string;
  };
}

@Injectable({
  providedIn: 'root'
})
export class Auth0Service {
  private readonly config = {
    domain: 'dev-nhti1ktswj6imfpk.us.auth0.com',
    clientId: 'pQ6jHtzXWNq79oidGsQRuS40LkluPxt1',
    clientSecret: 'O7dcGw5UxqU8m2fOqbDluXi5osUiJkDohYsDo_EVvZFFYqBBSBWhRpABYH0WRHFy',
  };

  private readonly managementApiUrl = `https://${this.config.domain}/api/v2`;
  private readonly tokenUrl = `https://${this.config.domain}/oauth/token`;

  constructor(private http: HttpClient, private auth0Angular: AuthService) {}

  private getAccessToken(): Observable<string> {
    const body = {
      client_id: this.config.clientId,
      client_secret: this.config.clientSecret,
      audience: `${this.managementApiUrl}/`,
      grant_type: 'client_credentials'
    };

    return this.http.post<any>(this.tokenUrl, body).pipe(
      map(response => response.access_token),
      catchError(error => {
        console.error('Error obteniendo el token:', error);
        return throwError(() => new Error('Error al obtener el token de acceso'));
      })
    );
  }

  private getHeaders(token: string): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
  }

  // Crear usuario
  createUser(userData: Auth0User): Observable<any> {
    return this.getAccessToken().pipe(
      switchMap(token => {
        const headers = this.getHeaders(token);
        const body = {
          ...userData,
          connection: 'Username-Password-Authentication',
          verify_email: false // Cambia a true si quieres verificación por email
        };

        return this.http.post(
          `${this.managementApiUrl}/users`,
          body,
          { headers }
        );
      }),
      catchError(error => {
        console.error('Error creando usuario:', error);
        return throwError(() => new Error('Error al crear el usuario'));
      })
    );
  }

  // Método para obtener usuarios desde Auth0
  getUsers(): Observable<any[]> {
    return this.getAccessToken().pipe(
      switchMap(token => {
        const headers = this.getHeaders(token);
        return this.http.get<any[]>(
          `${this.managementApiUrl}/users`,
          { headers }
        );
      }),
      catchError(error => {
        console.error('Error obteniendo usuarios:', error);
        return throwError(() => new Error('Error al obtener usuarios'));
      })
    );
  }

  // Método para mapear usuarios de Auth0 al formato de tu aplicación
  getAll(): Observable<Usuario[]> {
    return this.getUsers().pipe(
      map(auth0Users => {
        return auth0Users.map(user => ({
          id: user.user_id,
          email: user.email,
          contraseña: '', // No se puede obtener la contraseña por seguridad
          nombre: user.given_name || '',
          apellidos: user.family_name || '',
          fechaNacimiento: user.user_metadata?.fechaNacimiento || '',
          dni: user.user_metadata?.dni || '',
          rol: user.user_metadata?.rol || 'Sin Rol'
        }));
      }),
      catchError(error => {
        console.error('Error mapeando usuarios:', error);
        return of([]); // Devolver un array vacío en caso de error
      })
    );
  }

  // Actualizar usuario
   // Update user
   updateUser(userId: string, userData: Partial<Auth0User>): Observable<any> {
    return this.getAccessToken().pipe(
      switchMap(token => {
        const headers = this.getHeaders(token);
        // Exclude password from update data
        const { password, ...updateData } = userData;

        return this.http.patch(
          `${this.managementApiUrl}/users/${userId}`,
          updateData,
          { headers }
        );
      }),
      catchError(error => {
        console.error('Error updating user:', error);
        return throwError(() => new Error('Error updating user'));
      })
    );
  }

  // Delete user
  deleteUser(userId: string): Observable<void> {
    return this.getAccessToken().pipe(
      switchMap(token => {
        const headers = this.getHeaders(token);

        return this.http.delete<void>(
          `${this.managementApiUrl}/users/${userId}`,
          { headers }
        );
      }),
      catchError(error => {
        console.error('Error deleting user:', error);
        return throwError(() => new Error('Error deleting user'));
      })
    );
  }
  // Obtener un usuario por ID
getUserById(userId: string): Observable<Auth0User> {
  return this.getAccessToken().pipe(
    switchMap(token => {
      const headers = this.getHeaders(token);
      return this.http.get<Auth0User>(
        `${this.managementApiUrl}/users/${userId}`,
        { headers }
      );
    }),
    catchError(error => {
      console.error('Error obteniendo usuario:', error);
      return throwError(() => new Error('Error al obtener el usuario'));
    })
  );
}

getUserByEmail(email: string): Observable<Auth0User[]> {
  return this.getAccessToken().pipe(
    switchMap(token => {
      const headers = this.getHeaders(token);
      return this.http.get<Auth0User[]>(
        `${this.managementApiUrl}/users?q=email:${email}&search_engine=v3`,
        { headers }
      );
    }),
    catchError(error => {
      console.error('Error obteniendo usuario por email:', error);
      return throwError(() => new Error('Error al obtener el usuario por email'));
    })
  );
}
getCurrentUserRole(): Observable<string | null> {
  return this.auth0Angular.user$.pipe(
    switchMap(user => {
      if (!user || !user.email) {
        return of(null);
      }

      // Use the email from the authenticated user
      return this.getUserByEmail(user.email).pipe(
        map(users => {
          if (users.length > 0) {
            return users[0].user_metadata?.rol || null;
          }
          return null;
        }),
        catchError(() => of(null))
      );
    })
  );
}

}
