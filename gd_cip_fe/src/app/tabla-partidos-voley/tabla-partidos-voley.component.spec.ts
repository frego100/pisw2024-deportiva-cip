import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TablaPartidosVoleyComponent } from './tabla-partidos-voley.component';

describe('TablaPartidosVoleyComponent', () => {
  let component: TablaPartidosVoleyComponent;
  let fixture: ComponentFixture<TablaPartidosVoleyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TablaPartidosVoleyComponent]
    });
    fixture = TestBed.createComponent(TablaPartidosVoleyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
