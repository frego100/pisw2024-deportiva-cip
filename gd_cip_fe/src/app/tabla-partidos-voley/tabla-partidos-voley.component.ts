import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { lastValueFrom, Subject, take, takeUntil } from 'rxjs';
import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { EditarResultadoVoleyComponent } from '../editar-resultado-voley/editar-resultado-voley.component';
import { Campeonato } from '../services/campeonato.service';
import { Equipo } from '../services/equipo.service';
import { GlobalService } from '../services/global.service';
import { PartidosVolleyService, VoleyMatchDTO } from '../services/partidos-volley.service';
import { Auth0Service } from '../auth0.service';

declare var bootstrap: any

interface VoleyMatchDate {
  fecha: string | null;
  partidos: VoleyMatchDTO[];
}

@Component({
  selector: 'app-tabla-partidos-voley',
  templateUrl: './tabla-partidos-voley.component.html',
  styleUrls: ['./tabla-partidos-voley.component.css']
})
export class TablaPartidosVoleyComponent implements OnInit, OnDestroy {
  @Input() campeonato!: Campeonato

  esAdministrador: boolean = false;

  matchDates: VoleyMatchDate[] = [];
  eliminatoriaDates: VoleyMatchDate[] = [];
  eliminatoriaPhaseSelectedDate: VoleyMatchDate[] = [];
  matches: VoleyMatchDTO[] = [];

  selectedMatch!: VoleyMatchDTO
  isEditarPartidoModalOpen = false

  equiposMap: Map<number, Equipo> = new Map();
  selectedDate: string | null = null;
  phase: string = 'grupos';
  eliminatoriaPhases: string[] = [];
  isEliminatorias: boolean = false;
  selectedEliminatoriaPhase: string | null = null;
  eliminatoriasCreated: boolean = false;

  //Array vacio para pasarte los equipos clasificados, estoy usando el global service
  equiposClasificados: any[] = [];

  @ViewChild('confirmation') confirmation!: ConfirmationComponent;
  @ViewChild('confirmationPopup') confirmationPopup!: ConfirmationPopupComponent;
  @ViewChild(EditarResultadoVoleyComponent) editarResultadoVoley!: EditarResultadoVoleyComponent
  @ViewChild('phaseChange') phaseChange!: ConfirmationComponent;
  @ViewChild('phaseChanged') phaseChanged!: ConfirmationComponent;

  partidoVoleyEstado = this.gs.partidoFutbolEstado;
  fechaSeleccionadaParaEliminar: string | null = null;

  destroy$ = new Subject<void>()

  constructor(
    private gs: GlobalService,
    private authService: Auth0Service,
    private pvs: PartidosVolleyService,
  ) { }

  ngOnInit(): void {
    this.authService.getCurrentUserRole().subscribe((rol) => {
      this.esAdministrador = rol === 'Administrador';
    });
    console.log("Campeonato", this.campeonato)
    this.loadMatches();

    /* this.gs.updatePartido$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        console.log('Se recibió una actualización de partidos');
        this.loadMatches();
      }); */
  }

  ngOnDestroy(): void {
    this.destroy$.next()
    this.destroy$.complete()
  }

  loadMatches() {
    const campeonatoId = this.campeonato.id
    this.pvs.findByCampeonatoId(campeonatoId).subscribe(
      data => {
        const processedData = data.map((match: VoleyMatchDTO) => {
          return {
            ...match,
            puntaje11: match.puntaje11 ?? 0,
            puntaje12: match.puntaje12 ?? 0,
            puntaje21: match.puntaje21 ?? 0,
            puntaje22: match.puntaje22 ?? 0,
            puntaje31: match.puntaje31 ?? 0,
            puntaje32: match.puntaje32 ?? 0,
            puntaje41: match.puntaje41 ?? 0,
            puntaje42: match.puntaje42 ?? 0,
            puntaje51: match.puntaje51 ?? 0,
            puntaje52: match.puntaje52 ?? 0,
          };
        });
        this.gs.partidosSubject$
          .pipe(takeUntil(this.destroy$))
          .subscribe(data => {
            this.matches = data;
          })
        console.log("Partidos", this.matches)

        this.matchDates = this.groupMatchesByDate(
          processedData.filter((match) => match.fase === this.gs.Fase.GRUPOS)
        );
        this.eliminatoriaDates = this.groupMatchesByDate(
          processedData.filter((match) => match.fase > this.gs.Fase.GRUPOS)
        );

        this.gs.updateEliminatoriaDates(this.eliminatoriaDates);

        if (this.eliminatoriaDates.length > 0) {
          this.eliminatoriasCreated = true;
        }

        if (!this.selectedDate) {
          this.selectedDate = this.matchDates.length > 0
            ? this.matchDates[0].fecha
            : moment().format('YYYY-MM-DD');
        }

        if (!this.selectedEliminatoriaPhase && this.eliminatoriaPhases.length > 0) {
          this.selectedEliminatoriaPhase = this.eliminatoriaPhases[0];
        }

        this.eliminatoriaPhases = Array.from(
          new Set(this.eliminatoriaDates.map((date) => this.getFaseName(date.partidos[0].fase)))
        );

        if (this.eliminatoriaPhases.length > 0) {
          this.eliminatoriaPhaseSelectedDate = this.eliminatoriaDates.filter((date) =>
            date.partidos.some((p) => p.fase === this.getFaseValue(this.selectedEliminatoriaPhase || 'Octavos'))
          );
        }

        this.gs.equiposEnCampeonato$.pipe(take(1)).subscribe((equipos) => {
          this.equiposMap = new Map(equipos.map((equipo) => [equipo.id, equipo]));
        });
      }
    )
  }

  getDateLabel(item: VoleyMatchDate): string {
    const index = this.matchDates.findIndex(date => date.fecha === item.fecha);
    return index >= 0 ? `${index + 1}° fecha` : 'Sin fecha';
  }
  getDateLabelByIndex(index: number): string {
    return `${index + 1}° fecha`;
  }

  getFaseName(fase: number): string {
    switch (fase) {
      case this.gs.Fase.OCTAVOS:
        return 'Octavos';
      case this.gs.Fase.CUARTOS:
        return 'Cuartos';
      case this.gs.Fase.SEMIFINAL:
        return 'Semifinal';
      case this.gs.Fase.FINAL:
        return 'Final';
      default:
        return 'Octavos';
    }
  }
  getFaseValue(fase: string): number {
    switch (fase) {
      case 'Octavos':
        return this.gs.Fase.OCTAVOS;
      case 'Cuartos':
        return this.gs.Fase.CUARTOS;
      case 'Semifinal':
        return this.gs.Fase.SEMIFINAL;
      case 'Final':
        return this.gs.Fase.FINAL;
      default:
        return this.gs.Fase.OCTAVOS;
    }
  }

  // Agrupa los partidos por fecha
  groupMatchesByDate(data: VoleyMatchDTO[]): VoleyMatchDate[] {
    const grouped = data.reduce((acc: any, match: any) => {
      const key = `${match.fase}-${match.fecha}`;
      if (!acc[key]) {
        acc[key] = { fecha: match.fecha, fase: match.fase, partidos: [] };
      }
      acc[key].partidos.push(match);
      return acc;
    }, {});
    return Object.values(grouped);
  }

  async initializeEliminatorias(): Promise<void> {
    const totalEquipos = this.equiposClasificados.length;

    let faseInicial = this.gs.Fase.OCTAVOS;
    if (totalEquipos < 3) {
      this.eliminatoriaPhases = [];
      this.removeMatchesByPhase(this.gs.Fase.FINAL);
      this.removeMatchesByPhase(this.gs.Fase.SEMIFINAL);
      return;
    } else if (totalEquipos == 4) {
      faseInicial = this.gs.Fase.SEMIFINAL;
    } else if (totalEquipos == 8) {
      faseInicial = this.gs.Fase.CUARTOS;
    }

    this.eliminateExtraPhases(faseInicial);

    let existingMatchesBackend: any[] | undefined;
    try {
      if (!this.campeonato || !this.campeonato.id) {
        console.error('Error: campeonato o campeonato.id no está definido');
        return;
      }
      existingMatchesBackend = await lastValueFrom(this.pvs.findByCampeonatoId(this.campeonato.id));
    } catch (error) {
      console.error('Error al obtener los partidos existentes:', error);
      return;
    }

    // Crear partidos para cada fase desde la inicial hasta la final
    while (faseInicial >= this.gs.Fase.FINAL) {
      if (this.eliminatoriaPhases.includes(this.getFaseName(faseInicial))) {
        faseInicial--;
        continue;
      }
      if (!this.eliminatoriaPhases.includes(this.getFaseName(faseInicial))) {
        this.eliminatoriaPhases.push(this.getFaseName(faseInicial));
      }
      const existingMatches = existingMatchesBackend!.filter((p) => p.fase === faseInicial).length;
      const expectedMatches = this.getExpectedMatchesCount(this.getFaseName(faseInicial));
      if (existingMatches < expectedMatches) {
        const partidosPorCrear = expectedMatches - existingMatches;
        for (let i = 0; i < partidosPorCrear; i++) {
          await this.addMatch(faseInicial);
        }
      }
      faseInicial--;
    }
    //Service para enviar los partidos de eliminatorias al componente 8vos/4tos/semi
    this.gs.updateEliminatoriaDates(this.eliminatoriaDates);

    this.eliminatoriaPhases.sort((a, b) => {
      if (a === 'Octavos') return -1;
      if (a === 'Cuartos' && b === 'Semifinal') return -1;
      if (a === 'Cuartos' && b === 'Final') return -1;
      if (a === 'Semifinal' && b === 'Final') return -1;
      return 1;
    });

    //Asegura el envio de la lista de partidos
    this.gs.updateEliminatoriaDates(this.eliminatoriaDates);

    this.selectedEliminatoriaPhase = this.eliminatoriaPhases[0]
    this.onDateSelectChange({ target: { value: this.selectedEliminatoriaPhase } } as any);
    this.eliminatoriaPhaseSelectedDate = this.eliminatoriaDates.filter((date) =>
      date.partidos.some((p) => p.fase === this.getFaseValue(this.selectedEliminatoriaPhase || 'Octavos'))
    );
  }

  private eliminateExtraPhases(faseInicial: number): void {
    const fasesActuales = this.eliminatoriaPhases.slice();
    for (const fase of fasesActuales) {
      const faseNum = this.getFaseValue(fase);
      if (faseNum > faseInicial) {
        this.removeMatchesByPhase(faseNum);
        this.eliminatoriaPhases = this.eliminatoriaPhases.filter(
          (f) => f !== fase
        );
      }
    }
  }

  removeMatchesByPhase(phase: number): void {
    const partidosIds = this.eliminatoriaDates
      .flatMap((date) => date.partidos)
      .filter((partido) => partido.fase === phase)
      .map((partido) => partido.id)
      .filter((id) => id !== undefined);

    partidosIds.forEach((partidoId) => {
      this.pvs.deleteById(partidoId).subscribe(
        () => {
          this.loadMatches();
        },
        (error) => {
          console.error(`Error eliminando partido con ID ${partidoId}:`, error);
        }
      );
    });
  }

  // Maneja el cambio en el input de tipo date para modificar la fecha de los partidos
  onDateChange(event: any): void {
    const input = event.target as HTMLInputElement;
    this.selectedDate = input.value;
    this.filterMatchesByDate();
  }

  // Maneja el cambio en el select y actualiza la lista de partidos
  onDateSelectChange(event: any): void {
    const selectedPhase = (event.target as HTMLSelectElement).value;
    if (this.phase === 'eliminatorias') {
      this.selectedEliminatoriaPhase = selectedPhase;
      const phaseValue = this.getFaseValue(this.selectedEliminatoriaPhase || 'Octavos');
      this.eliminatoriaPhaseSelectedDate = this.eliminatoriaDates.filter((date) =>
        date.partidos.some((partido) => partido.fase === phaseValue)
      );
    }
  }

  // Filtra los partidos mostrados según la fecha seleccionada
  private filterMatchesByDate(): void {
    if (this.phase === 'grupos') {
      this.matchDates.filter((date) => date.fecha === this.selectedDate);
    } else if (this.phase === 'eliminatorias' && this.selectedEliminatoriaPhase) {
      this.eliminatoriaDates.filter(
        (date) => date.partidos[0].fase === this.getFaseValue(this.selectedEliminatoriaPhase || 'Octavos')
      );
    }
  }

  // Método para remover la fecha seleccionada y mostrar el popup de confirmación
  removeFecha(fechaSeleccionada: string): void {
    this.fechaSeleccionadaParaEliminar = fechaSeleccionada;
    this.confirmationPopup.show();
  }

  // Se ejecuta cuando se confirma la eliminación de una fecha
  async handleConfirmDelete(): Promise<void> {
    if (!this.fechaSeleccionadaParaEliminar) return;

    const index = this.matchDates.findIndex(date => date.fecha === this.fechaSeleccionadaParaEliminar);

    if (index < 0) {
      console.error('Fecha no encontrada o no hay fechas para eliminar.');
      return;
    }

    const partidosIds = this.matchDates[index].partidos
      .map(partido => partido.id)
      .filter(id => id !== undefined);

    try {
      // Elimina los partidos asociados a la fecha
      for (const partidoId of partidosIds) {
        await lastValueFrom(this.pvs.deleteById(partidoId as number));
        console.log(`Partido con ID ${partidoId} eliminado.`);
      }

      // Elimina la fecha seleccionada
      this.matchDates.splice(index, 1);

      // Actualiza `selectedDate` con la primera fecha disponible
      if (this.matchDates.length > 0) {
        this.selectedDate = this.matchDates[0].fecha;
      } else {
        this.selectedDate = moment().format('YYYY-MM-DD');
      }

      console.log('Fechas actualizadas:', this.matchDates);
      console.log('selectedDate actualizado:', this.selectedDate);

      this.loadMatches(); // Recargar los partidos después de actualizar las fechas

      this.confirmation.message = 'Fecha eliminada correctamente.';
      this.confirmation.type = 'success';
      this.confirmation.show();
    } catch (error) {
      console.error('Error al eliminar partidos:', error);
      this.confirmation.message = 'Error al eliminar partido o fecha.';
      this.confirmation.type = 'danger';
      this.confirmation.show();
    } finally {
      this.fechaSeleccionadaParaEliminar = null;
    }
  }

  private getExpectedMatchesCount(phase: string): number {
    switch (phase) {
      case 'Octavos': return 8;
      case 'Cuartos': return 4;
      case 'Semifinal': return 2;
      case 'Final': return 2;
      default: return 0;
    }
  }

  // Se ejecuta si se cancela la eliminación
  handleCancelDelete(): void {
    this.fechaSeleccionadaParaEliminar = null;
    console.log('Eliminación cancelada.');
  }

  // Agregar un nuevo partido
  async addMatch(phase: number): Promise<void> {
    const newMatch: VoleyMatchDTO = {
      id: 0,
      fecha: this.selectedDate,
      equipo1Nombre: 'Equipo 1',
      equipo2Nombre: 'Equipo 2',
      puntaje11: 0,
      puntaje12: 0,
      puntaje21: 0,
      puntaje22: 0,
      puntaje31: 0,
      puntaje32: 0,
      puntaje41: 0,
      puntaje42: 0,
      puntaje51: 0,
      puntaje52: 0,
      campeonatoId: this.campeonato.id,
      estado: 'PREPARACION',
      equipo1Id: 0,
      equipo2Id: 0,
      numeroSets: 3,
      fase: phase || 1,
      grupo: "",
    };

    return new Promise((resolve, reject) => {
      this.pvs.save(newMatch).subscribe(
        (response) => {
          const match: VoleyMatchDTO = { ...newMatch, id: response.id };

          if (this.phase === 'eliminatorias') {
            const phaseDate = this.eliminatoriaDates.find((d) => d.fecha === this.selectedDate);
            if (phaseDate) {
              phaseDate.partidos.push(match);
            } else {
              this.eliminatoriaDates.push({ fecha: this.selectedDate, partidos: [match] });
            }
          } else {
            const groupDate = this.matchDates.find((d) => d.fecha === this.selectedDate);
            if (groupDate) {
              groupDate.partidos.push(match);
            } else {
              this.matchDates.push({ fecha: this.selectedDate!, partidos: [match] });
            }
          }
          this.matchDates = this.groupMatchesByDate(
            this.matchDates.flatMap((date) => date.partidos).filter((match) => match.fase === this.gs.Fase.GRUPOS)
          );
          this.eliminatoriaDates = this.groupMatchesByDate(
            this.eliminatoriaDates.flatMap((date) => date.partidos).filter((match) => match.fase > this.gs.Fase.GRUPOS)
          );
          resolve();
        },
        (error) => {
          console.error('Error al agregar partido:', error);
          reject(error);
        }
      );

    });
  }

  shouldDisplaySet(setNumber: number, match: VoleyMatchDTO): boolean {
    return setNumber <= match.numeroSets;
  }

  // Calcula la cantidad de sets ganados por un equipo en el partido
  calculateSetsWon(match: VoleyMatchDTO, equipo: 'equipo1' | 'equipo2'): number {
    let setsWon = 0;

    if (equipo === 'equipo1') {
      if (match.puntaje11 > match.puntaje12) setsWon++;
      if (match.puntaje21 > match.puntaje22) setsWon++;
      if (match.puntaje31 > match.puntaje32) setsWon++;
      if (match.puntaje41 > match.puntaje42) setsWon++;
      if (match.puntaje51 > match.puntaje52) setsWon++;
    } else {
      if (match.puntaje12 > match.puntaje11) setsWon++;
      if (match.puntaje22 > match.puntaje21) setsWon++;
      if (match.puntaje32 > match.puntaje31) setsWon++;
      if (match.puntaje42 > match.puntaje41) setsWon++;
      if (match.puntaje52 > match.puntaje51) setsWon++;
    }

    return setsWon;
  }

  // Método para finalizar la fase
  finalizarFase(): void {
    const partidosSinFinalizar = this.matchDates
      .flatMap(date => date.partidos)
      .filter(partido => partido.estado !== 'REALIZADO');

    if (partidosSinFinalizar.length > 0) {
      this.phaseChanged.message = 'Partidos Sin Finalizar';
      this.phaseChanged.type = 'warning';
      this.phaseChanged.show();
      return;
    }
    if (this.eliminatoriasCreated === false && this.esAdministrador === true && this.eliminatoriaDates.length === 0) {
      this.phaseChange.show();
    } else {
      this.handleConfirmarFinalizacion();
    }
  }

  // Método de confirmación del popup para finalizar fase
  handleConfirmarFinalizacion(): void {
    this.phase = 'eliminatorias';

    if (this.eliminatoriasCreated === false && this.eliminatoriaDates.length === 0) {
      this.phaseChanged.message = 'Fase Eliminatorias Creada';
      this.phaseChanged.type = 'success';
      this.phaseChanged.show();
    }
    this.eliminatoriasCreated = true;

    console.log('Fase finalizada');
    this.gs.finalizarFase(this.campeonato.id);

    this.gs.getEquiposClasificados$(this.campeonato.id).pipe(take(1)).subscribe(equipos => {
      this.equiposClasificados = equipos;
      console.log('Array de equipos clasificados recibido:', this.equiposClasificados);

      if (this.equiposClasificados.length === 0) {
        console.error('No se recibieron equipos clasificados.');
      } else {
        this.initializeEliminatorias();
        console.log('eliminatorias iniciadas')
      }
    });
  }

  // Método para regresar a la fase de grupos
  regresarAFaseGrupos(): void {
    this.phase = 'grupos';
    this.isEliminatorias = false;
    this.loadMatches();

    /* this.gs.partidosSubject$
      .pipe(takeUntil(this.destroy$))
      .subscribe((data: VoleyMatchDTO[]) => {
        this.matches = data.map((match) => {
          return {
            ...match,
            puntaje11: match.puntaje11 ?? 0,
            puntaje12: match.puntaje12 ?? 0,
            puntaje21: match.puntaje21 ?? 0,
            puntaje22: match.puntaje22 ?? 0,
            puntaje31: match.puntaje31 ?? 0,
            puntaje32: match.puntaje32 ?? 0,
            puntaje41: match.puntaje41 ?? 0,
            puntaje42: match.puntaje42 ?? 0,
            puntaje51: match.puntaje51 ?? 0,
            puntaje52: match.puntaje52 ?? 0,
          };
        });
        this.matchDates = this.groupMatchesByDate(this.matches);
        this.selectedDate = this.matchDates.length > 0 ? this.matchDates[0].fecha : moment().format('YYYY-MM-DD');
        this.filterMatchesByDate();
      }); */
  }

  openTeamModal(): void {
    const modal = new bootstrap.Modal(document.getElementById('teamModal'), {
      keyboard: false,
    });
    modal.show();
  }

  showEditarPartidoModal(match: VoleyMatchDTO) {
    const element = document.getElementById("matchModal")
    const modal = new bootstrap.Modal(element)
    modal.show()
    if (this.selectedMatch != match) this.selectedMatch = match
    else {
      this.editarResultadoVoley.reloadPartidoData()
    }

  }

  private async updateNextPhase(match: VoleyMatchDTO): Promise<void> {
    // Verificar que el partido esté marcado como "REALIZADO"
    console.log('Resultado obtenido:', match.estado);
    const partido = await lastValueFrom(this.pvs.findById(match.id));
    match = partido;
    if (match.estado !== 'REALIZADO') {
      console.warn('El partido no está marcado como "REALIZADO".');
      return;
    }

    // Determinar el equipo ganador
    const equipoGanadorId = this.calculateSetsWon(match, 'equipo1') > this.calculateSetsWon(match, 'equipo2') ? match.equipo1Id : match.equipo2Id;
    const equipoPerdedorId = this.calculateSetsWon(match, 'equipo1') < this.calculateSetsWon(match, 'equipo2') ? match.equipo1Id : match.equipo2Id;
    console.log('Equipo ganador:', equipoGanadorId, 'Equipo perdedor:', equipoPerdedorId);

    if (equipoGanadorId === equipoPerdedorId) {
      console.warn('No se pudo determinar el ganador del partido:', match);
      return;
    }
    const partidosFase = this.eliminatoriaDates
      .flatMap(date => date.partidos)
      .filter(p => p.fase === match.fase);
    // Obtener la fase siguiente
    const ordenActual = partidosFase.findIndex(p => p.id === match.id);
    console.log(ordenActual)
    const nextPhase = match.fase - 1; // Ejemplo: de cuartos (3) a semifinal (2)

    if (nextPhase > 1) {
      // Buscar el partido correspondiente en la fase siguiente
      console.log('Fase actual:', match.fase, 'Fase siguiente:', nextPhase);


      const partidosFaseSiguiente = this.eliminatoriaDates
        .flatMap(date => date.partidos)
        .filter(p => p.fase === nextPhase);
      console.log('Partidos en la fase actual:', partidosFase);
      console.log('Partidos en la fase siguiente:', partidosFaseSiguiente);

      // Seleccionar el partido correcto según el orden
      let nextMatchIndex = Math.floor(ordenActual / 2);
      const nextMatch = partidosFaseSiguiente[nextMatchIndex];

      if (nextMatch) {
        if (
          nextMatch.equipo1Id === equipoGanadorId ||
          nextMatch.equipo2Id === equipoGanadorId
        ) {
          console.warn('El equipo ya está asignado en el partido siguiente:', nextMatch.id);
          return;
        }

        if (ordenActual % 2 === 0) {
          nextMatch.equipo1Id = equipoGanadorId;
        } else if (ordenActual % 2 !== 0) {
          nextMatch.equipo2Id = equipoGanadorId;
        }

        try {
          await this.pvs.save(nextMatch).toPromise();
          console.log(`Equipo ganador asignado al partido siguiente (ID: ${nextMatch.id})`);
        } catch (error) {
          console.error('Error al actualizar el partido siguiente:', error);
        }
      } else {
        console.warn(`No se encontró un partido en la fase ${this.getFaseName(nextPhase)} con el índice adecuado.`);
      }
    }

    // Manejar el partido por el tercer puesto si es una semifinal
    if (match.fase === 3) {
      const partidosTercerPuesto = this.eliminatoriaDates
        .flatMap(date => date.partidos)
        .filter(p => p.fase === 2);

      const partidoTercerPuesto = partidosTercerPuesto[1];

      if (partidoTercerPuesto) {
        // Asignar el equipo perdedor al tercer puesto
        if (
          partidoTercerPuesto.equipo1Id === equipoPerdedorId ||
          partidoTercerPuesto.equipo2Id === equipoPerdedorId
        ) {
          console.warn('El equipo ya está asignado en el partido por el tercer puesto:', partidoTercerPuesto.id);
          return;
        }
        if (ordenActual % 2 === 0) {
          partidoTercerPuesto.equipo1Id = equipoPerdedorId; // Lado izquierdo
        } else if (ordenActual % 2 !== 0) {
          partidoTercerPuesto.equipo2Id = equipoPerdedorId; // Lado derecho
        }

        try {
          await lastValueFrom(this.pvs.save(partidoTercerPuesto));
          console.log(`Equipo perdedor asignado al partido por el tercer puesto (ID: ${partidoTercerPuesto.id})`);
        } catch (error) {
          console.error('Error al actualizar el partido de tercer puesto:', error);
        }
      } else {
        console.warn('No se encontró un partido para el tercer puesto.');
      }
    }
    this.loadMatches()
  }

  handleEliminarPartido() {
    console.log("handleEliminarPartido")
    const element = document.getElementById("matchModal")
    const modal = bootstrap.Modal.getInstance(element)
    modal.hide()
    this.loadMatches()
  }

  handleGuardarPartido() {
    console.log("handleOnGuardarPartido")
    const element = document.getElementById("matchModal")
    const modal = bootstrap.Modal.getInstance(element)
    modal.hide()
    this.updateNextPhase(this.selectedMatch)
      .then(() => console.log("Partido actualizado"))
      .catch((err) => console.error("Error al actualizar:", err));
    this.loadMatches()
  }

  getMatchBackgroundColor(match: any, index: number): any {
    const baseColor = 'white'; // You can set a default background color here
    const partidosFinal = this.eliminatoriaDates.filter((date) => date.partidos.some((p) => p.fase === 2))[0];
    if (match.fase === 2 && match === partidosFinal.partidos[0]) {
      const style = {
        'background-color': '#ddffdb',
        'color': 'black',
        'border-radius': '10px',
      }
      return style; // Replace with your desired highlight color
    }
    return { 'background-color': baseColor };
  }
}
