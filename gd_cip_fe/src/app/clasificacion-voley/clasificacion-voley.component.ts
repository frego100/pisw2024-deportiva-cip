import { Component, Input, OnInit, OnDestroy, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MenuCampeonatoComponent } from '../menu-campeonato/menu-campeonato.component';//
import { ClasificacionService, ClasificacionData } from '../services/clasificacion.service';//Por ahora usa el servicio de futbol
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { Campeonato } from '../services/campeonato.service';
import { Equipo } from '../services/equipo.service';
import { combineLatest, of, Subject, Subscription, take, takeUntil } from 'rxjs';
import { VoleyMatchDTO } from '../services/partidos-volley.service';
import { EquipoCampeonatoService } from '../services/equipo-campeonato.service';


@Component({
  selector: 'app-clasificacion-voley',
  templateUrl: './clasificacion-voley.component.html',
  styleUrls: ['./clasificacion-voley.component.css']
})
export class ClasificacionVoleyComponent implements OnInit, OnDestroy {

  @Input() campeonato!: Campeonato
  @Input() partido!: VoleyMatchDTO;

  selectedView: string = 'admin';
  displayedColumns: string[] = ['position', 'team', 'pts', 'j', 'g', 'p', 'f', 'c', 'ds', 'dp'];
  isGrouped: boolean = false;
  selectedPhase: string = '1';
  selectedCampeonatoId: number = -1;//para el filtrado por campeonato

  groups: { name: string, teams: any[] }[] = [];//por grupos
  allTeamsDataSource: any[] = [];//array de todos los equipos
  semiFinalMatches: { team1: string; team2: string; }[] = [];
  finalMatch: { team1: string; team2: string; } = { team1: '', team2: '' };

  //Extraer los equipos que pasaron 
  topTeamsPerGroup: { groupName: string; teams: any[] }[] = [];
  equiposClasificados: any[] = []; // Array para almacenar los equipos clasificados
  estructura2: any [] = []; //Array del service EquipoCampeonato

  //Bandera para activar y desactivar el calculo de clasificacion
  isClasificacionActiva: boolean = true;
  faseFinalizada: boolean = false;

  equiposEnCampeonato: Equipo[] = [];

  equipos!: Map<number, string>

  destroy$ = new Subject<void>()

  constructor(
    public dialog: MatDialog, 
    private clasificacionService: ClasificacionService,
    private equipoCampeonatoService: EquipoCampeonatoService, 
    private router: Router,
    private gs: GlobalService) 
  {}

  ngOnInit(): void {
    console.log("clasificacionComponent.ngOnInit()")
    this.loadClasificacionDataNuevo();
  }

  ngOnDestroy(): void {
    console.log("ClasificacionComponent.onDetroy()")
    this.destroy$.next()
    this.destroy$.complete()
  }

  // Método para detener el cálculo de la tabla
  detenerCalculoClasificacion(): void {
    this.isClasificacionActiva = false; // Desactiva el cálculo
    console.log('Cálculo de clasificación detenido');
  }

   //Método para reactivar el cálculo si es necesario
   reactivarCalculoClasificacion(): void {
    this.isClasificacionActiva = true;
    console.log('Cálculo de clasificación activado');
    this.loadClasificacionDataNuevo(); // Recarga los datos al activar
  }


  //Nueva implementacion de loadClasificacionData
  loadClasificacionDataNuevo(){

    if (!this.isClasificacionActiva) {
      console.log("Cálculo de clasificación está desactivado.");
      return; // No hace nada si la bandera está desactivada
    }

    this.selectedCampeonatoId = this.campeonato.id;

    this.equipoCampeonatoService.findByCampeonatoId(this.campeonato.id).subscribe(datos => {
      this.estructura2 = datos;
      console.log('estructura 2:', this.estructura2);

      combineLatest([
        this.gs.equiposEnCampeonato$,
        this.gs.partidosSubject$,
      ])
      .pipe(take(1))
      .subscribe(([equiposEnCampeonatoData, partidos]) => {
        this.equiposEnCampeonato = equiposEnCampeonatoData;
        this.calculateClasificacion(equiposEnCampeonatoData, partidos);
      });
      
      this.gs.partidosSubject$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        combineLatest([
          this.gs.equiposEnCampeonato$,
          this.gs.partidosSubject$,
        ])
        .pipe(take(1))
        .subscribe(([equiposEnCampeonatoData, partidos]) => {
          this.equiposEnCampeonato = equiposEnCampeonatoData;
          this.calculateClasificacion(equiposEnCampeonatoData, partidos);
        });
      });
    });
  }

  calculateClasificacion(equiposEnCampeonato: Equipo[], partidos: VoleyMatchDTO[]): void {
    
    if (!this.isClasificacionActiva && this.faseFinalizada) {
      console.log('Cálculo de clasificación desactivado y fase finalizada.');
      return;
    }
    
    // Crear un mapa para obtener el grupoId basado en equipoId desde estructura2
    const equipoGrupoMap = new Map<number, number>();
    this.estructura2.forEach(item => {
      equipoGrupoMap.set(item.equipoId, item.grupoId);
    });

    const equipoStats = new Map<number, any>();

    // Inicializa estadísticas para cada equipo
    equiposEnCampeonato.forEach(equipo => {
        equipoStats.set(equipo.id, {
            id: equipo.id,
            team: equipo.nombre,
            grupoId:  equipoGrupoMap.get(equipo.id) || null,
            pts: 0,
            j: 0,
            g: 0,
            p: 0,
            f: 0,
            c: 0,
            ds: 0,
            dp: 0,
        });
    });

    // Procesa cada partido para actualizar estadísticas
    partidos.filter(match => match.estado === 'REALIZADO'  && (match.fase === 1 || match.fase === null)).forEach(match => {
        const equipo1 = equipoStats.get(match.equipo1Id);
        const equipo2 = equipoStats.get(match.equipo2Id);

        if (!equipo1 || !equipo2) return;

        let setsGanadosEquipo1 = 0;
        let setsGanadosEquipo2 = 0;
        const setsEquipo1 = [match.puntaje11, match.puntaje21, match.puntaje31, match.puntaje41, match.puntaje51];
        const setsEquipo2 = [match.puntaje12, match.puntaje22, match.puntaje32, match.puntaje42, match.puntaje52];

        // Cuenta los sets ganados por cada equipo
        for (let i = 0; i < setsEquipo1.length; i++) {
            if (setsEquipo1[i] > setsEquipo2[i]) setsGanadosEquipo1++;
            else if (setsEquipo2[i] > setsEquipo1[i]) setsGanadosEquipo2++;
        }

        // Actualiza juegos jugados
        equipo1.j++;
        equipo2.j++;

        // Actualiza ganados y perdidos
        if (setsGanadosEquipo1 > setsGanadosEquipo2) {
            equipo1.g++;
            equipo2.p++;
            equipo1.pts += 3; // 3 puntos por victoria
        } else if (setsGanadosEquipo2 > setsGanadosEquipo1) {
            equipo2.g++;
            equipo1.p++;
            equipo2.pts += 3;
        } else {
            equipo1.pts += 1; // 1 punto por empate
            equipo2.pts += 1;
        }

        // Actualiza sets a favor y en contra
        equipo1.f += setsGanadosEquipo1;
        equipo1.c += setsGanadosEquipo2;
        equipo2.f += setsGanadosEquipo2;
        equipo2.c += setsGanadosEquipo1;

        // Calcula diferencias
        equipo1.ds = equipo1.f - equipo1.c;
        equipo2.ds = equipo2.f - equipo2.c;
        equipo1.dp += setsEquipo1.reduce((a, b) => a + b, 0) - setsEquipo2.reduce((a, b) => a + b, 0);
        equipo2.dp += setsEquipo2.reduce((a, b) => a + b, 0) - setsEquipo1.reduce((a, b) => a + b, 0);
    });

    const teamData = Array.from(equipoStats.values());
    
    if (equiposEnCampeonato.length > 5) { 
        // Clasificación por grupos
        const groupA = teamData.filter(team => team.grupoId === 1);
        const groupB = teamData.filter(team => team.grupoId === 2);
        
        this.groups = [
            {
                name: 'Grupo A',
                teams: groupA.sort((a, b) => b.pts - a.pts || b.ds - a.ds || b.dp - a.dp)
            },
            {
                name: 'Grupo B',
                teams: groupB.sort((a, b) => b.pts - a.pts || b.ds - a.ds || b.dp - a.dp)
            }
        ];

        this.groups.forEach(group => {
            group.teams.forEach((team, index) => {
                team.position = index + 1;
            });
        });

        
        //Extrae el top de equipos segun la cantidad requerida
        combineLatest([
          this.gs.faseFinalizada$,
          of(this.groups)
        ])
        .pipe(
          takeUntil(this.destroy$)
        )
        .subscribe(([faseFinalizadaMap, gruposProcesados]) => {

          this.faseFinalizada = faseFinalizadaMap.get(this.selectedCampeonatoId) ?? false;
          //this.equiposClasificados = equiposClasificados;
          if (this.faseFinalizada) {
            console.log('Fase finalizada para campeonato:', this.selectedCampeonatoId);
            this.detenerCalculoClasificacion();
            
            // Procesar clasificación según los equipos clasificados
            if (gruposProcesados.length > 0) {
              if (equiposEnCampeonato.length > 5 && equiposEnCampeonato.length <= 9) {
                this.extraerEquiposSemifinales();
              } else if (equiposEnCampeonato.length > 9 && equiposEnCampeonato.length <= 20) {
                this.extraerEquiposCuartos();
              } else if (equiposEnCampeonato.length > 20) {
                this.extraerEquiposOctavos();
              }
            }else {
              console.warn('No se encontraron grupos procesados.');
            }
          }
        });

    } else {
        // Clasificación general (todos contra todos)
        this.allTeamsDataSource = teamData.sort((a, b) => b.pts - a.pts || b.ds - a.ds || b.dp - a.dp);
        this.allTeamsDataSource.forEach((team, index) => {
            team.position = index + 1;
        });
    }
  }


  extraerEquiposSemifinales(): void {
    this.topTeamsPerGroup = this.groups.map(group => ({
      groupName: group.name,
      teams: group.teams.slice(0, 2), // Extrae los 2 primeros equipos de cada grupo
    }));

    this.equiposClasificados = this.topTeamsPerGroup.flatMap(group => group.teams);
    console.log('Equipos clasificados semifinales', this.equiposClasificados);
    //Actualiza el listado de equipos clasificados
    this.gs.updateEquiposClasificados(this.selectedCampeonatoId, this.equiposClasificados);
    console.log('id del campeonato', this.selectedCampeonatoId);
  }
  
  extraerEquiposCuartos(): void {
    this.topTeamsPerGroup = this.groups.map(group => ({
      groupName: group.name,
      teams: group.teams.slice(0, 4), // Extrae los 4 primeros equipos de cada grupo
    }));
    this.equiposClasificados = this.topTeamsPerGroup.flatMap(group => group.teams);
    console.log('Equipos clasificados cuartos', this.equiposClasificados);
    //Actualiza el listado de equipos clasificados
    this.gs.updateEquiposClasificados(this.selectedCampeonatoId, this.equiposClasificados);
  }

  extraerEquiposOctavos(): void {
    this.topTeamsPerGroup = this.groups.map(group => ({
      groupName: group.name,
      teams: group.teams.slice(0, 8), // Extrae los 8 primeros equipos de cada grupo
    }));
    this.equiposClasificados = this.topTeamsPerGroup.flatMap(group => group.teams);
    console.log('Equipos clasificados octavos', this.equiposClasificados);
    //Actualiza el listado de equipos clasificados
    this.gs.updateEquiposClasificados(this.selectedCampeonatoId, this.equiposClasificados);
  }

  


  //Evento que cambia de fase
  onPhaseChange(event: any): void {
    this.selectedPhase = event.target.value;
    
  }

  toggleGroup(event: any): void {
    this.isGrouped = event.checked;
  }

  openMenuCampeonato(): void {
    this.dialog.open(MenuCampeonatoComponent);
  }



  //Esto todavia no es funcional, es de fase 2
  updatePhaseData(): void {
    if (this.selectedPhase === '2') {
      const topTeams = this.allTeamsDataSource.slice(0, 4);
      this.semiFinalMatches = [
        { team1: topTeams[0].team, team2: topTeams[1].team },
        { team1: topTeams[2].team, team2: topTeams[3].team }
      ];
      this.finalMatch = { team1: '', team2: '' };
    }
  }


//Obtiene los datos para mostrar en Equipo-vista
  setTeamData(row: any): void {
    this.clasificacionService.setSelectedTeam({
      name: row.team,
      wins: row.g,
      draws: row.j,
      loses: row.p,
      id: row.id
    });
    this.router.navigate(['/Team-Voley']);
  }
//envia los datos al momento de seleccionar el equipo en la tabla clasificacion
  onRowClick(row: any): void {
    this.setTeamData(row);
  }

  isTeamClasificado(team: any): boolean {
    if (this.equiposEnCampeonato.length > 5 && this.equiposEnCampeonato.length <= 9) {
      const topTeams = this.groups.flatMap(group => group.teams.slice(0, 2)); 
      return topTeams.some(eq => eq.id === team.id);
    }else if(this.equiposEnCampeonato.length > 9 && this.equiposEnCampeonato.length <= 20){
      const topTeams = this.groups.flatMap(group => group.teams.slice(0, 4)); 
      return topTeams.some(eq => eq.id === team.id);
    }else if(this.equiposEnCampeonato.length > 20){
      const topTeams = this.groups.flatMap(group => group.teams.slice(0, 8)); 
      return topTeams.some(eq => eq.id === team.id);
    }
    return false;
  }

}
