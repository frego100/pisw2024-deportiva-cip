import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VfinalesComponent } from './vfinales.component';

describe('VfinalesComponent', () => {
  let component: VfinalesComponent;
  let fixture: ComponentFixture<VfinalesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VfinalesComponent]
    });
    fixture = TestBed.createComponent(VfinalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
