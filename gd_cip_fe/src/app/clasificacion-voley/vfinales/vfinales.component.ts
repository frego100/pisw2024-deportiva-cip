import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-vfinales',
  templateUrl: './vfinales.component.html',
  styleUrls: ['./vfinales.component.css']
})
export class VfinalesComponent {

  @Input() equipos: any[] = [];

  ngOnInit() {
    console.log('Equipos en Finales:', this.equipos);
  }

}
