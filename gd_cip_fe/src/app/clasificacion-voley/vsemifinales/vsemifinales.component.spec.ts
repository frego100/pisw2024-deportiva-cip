import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VSemifinalesComponent } from './vsemifinales.component';

describe('VSemifinalesComponent', () => {
  let component: VSemifinalesComponent;
  let fixture: ComponentFixture<VSemifinalesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VSemifinalesComponent]
    });
    fixture = TestBed.createComponent(VSemifinalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
