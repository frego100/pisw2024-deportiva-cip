import { Component, Input } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-vsemifinales',
  templateUrl: './vsemifinales.component.html',
  styleUrls: ['./vsemifinales.component.css']
})
export class VSemifinalesComponent {
  @Input() equipos: any[] = [];

  eliminatoriaDatesVoley: any[] = [];
  private destroy$ = new Subject<void>();

  constructor(private gs: GlobalService) {}
  
  ngOnInit() {
    this.gs.eliminatoriaDates$
    .pipe(takeUntil(this.destroy$))
    .subscribe((dates) => {
      this.eliminatoriaDatesVoley = dates;
      console.log('Recibido en OtroComponente:', this.eliminatoriaDatesVoley);
    });
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


  // Obtener el nombre del equipo por ID
  getEquipoNombre(id: number): string {
    const equipo = this.equipos.find(e => e.id === id);
    return equipo ? equipo.team : 'Sin Asignar';
  }

  // Obtener partidos por fase (3: Semifinales)
  getPartidos(fase: number): any[] {
    const faseData = this.eliminatoriaDatesVoley.find(f => f.fase === fase);
    return faseData ? faseData.partidos : [];
  }

  //Obtener partidos de la final
  getFinal(): any[] {
    const faseData = this.eliminatoriaDatesVoley.find(f => f.fase === 2);
    return faseData ? [faseData.partidos[0]] : [];
  }

  //Obtener partidos de 3er/4to puesto
  getTercerCuarto(): any[] {
    const faseData = this.eliminatoriaDatesVoley.find(f => f.fase === 2);
    return faseData ? [faseData.partidos[1]] : [];
  }

  //Determinar si un equipo ganó  es mas css
  isWinner(partido: any, equipo: number): boolean {
    if (equipo === 1) {
      return this.calculateSetsWon(partido).equipo1> this.calculateSetsWon(partido).equipo2;
    } else if (equipo === 2) {
      return this.calculateSetsWon(partido).equipo2 > this.calculateSetsWon(partido).equipo1;
    }
    return false;
  }


  calculateSetsWon(partido: any): { equipo1: number; equipo2: number } {
    const resultados = [
      [partido.puntaje11, partido.puntaje12],
      [partido.puntaje21, partido.puntaje22],
      [partido.puntaje31, partido.puntaje32],
      [partido.puntaje41, partido.puntaje42],
      [partido.puntaje51, partido.puntaje52],
    ];

    let setsEquipo1 = 0;
    let setsEquipo2 = 0;

    resultados.forEach(([p1, p2]) => {
      if (p1 !== null && p2 !== null) {
        if (p1 > p2) setsEquipo1++;
        else if (p2 > p1) setsEquipo2++;
      }
    });

    return { equipo1: setsEquipo1, equipo2: setsEquipo2 };
  }
}
