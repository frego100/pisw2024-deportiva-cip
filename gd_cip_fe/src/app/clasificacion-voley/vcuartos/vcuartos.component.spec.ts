import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VCuartosComponent } from './vcuartos.component';

describe('VCuartosComponent', () => {
  let component: VCuartosComponent;
  let fixture: ComponentFixture<VCuartosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VCuartosComponent]
    });
    fixture = TestBed.createComponent(VCuartosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
