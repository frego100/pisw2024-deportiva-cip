import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VOctavosComponent } from './voctavos.component';

describe('VOctavosComponent', () => {
  let component: VOctavosComponent;
  let fixture: ComponentFixture<VOctavosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VOctavosComponent]
    });
    fixture = TestBed.createComponent(VOctavosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
