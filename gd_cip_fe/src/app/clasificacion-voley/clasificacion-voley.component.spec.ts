import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasificacionVoleyComponent } from './clasificacion-voley.component';

describe('ClasificacionVoleyComponent', () => {
  let component: ClasificacionVoleyComponent;
  let fixture: ComponentFixture<ClasificacionVoleyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ClasificacionVoleyComponent]
    });
    fixture = TestBed.createComponent(ClasificacionVoleyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
