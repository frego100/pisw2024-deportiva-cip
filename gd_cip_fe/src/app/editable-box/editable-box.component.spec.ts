import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditableBoxComponent } from './editable-box.component';

describe('EditableBoxComponent', () => {
  let component: EditableBoxComponent;
  let fixture: ComponentFixture<EditableBoxComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditableBoxComponent]
    });
    fixture = TestBed.createComponent(EditableBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
