import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges, OnDestroy } from '@angular/core';
import { PlayerService, Player } from '../services/player.service';
import { GlobalService } from '../services/global.service';
import { Alineacion } from '../services/equipo-vista.service';
import { AlineacionService } from '../services/alineacion.service';
import { reference } from '@popperjs/core';
import { Observable, Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-editable-box',
  templateUrl: './editable-box.component.html',
  styleUrls: ['./editable-box.component.css']
})
export class EditableBoxComponent implements OnInit, OnChanges, OnDestroy {
  tipoEditableBox = this.gs.tipoEditableBox

  @Input() title: string = '';
  @Input() items: any[] = [];
  @Input() itemsEliminar: any[] = []
  @Input() isDisabled: boolean = false;
  @Input() golContra = 0;
  @Input() cambiaPuntaje = false; 
  @Input() numeroEquipo = 1
  @Input() flag = 0
  @Input() isHidden = false;
  @Input() tipoEditable = this.tipoEditableBox.GENERAL
  @Input() equipoId = -1

  @Output() onCambiarPuntaje = new EventEmitter<number>();
  @Output() onDisminuirPuntaje = new EventEmitter<number>();

  isOpen: boolean = false;
  players: Player[] = [];
  jugadorEntraId: number | null = null; // Selección del jugador que entra
  jugadorSaleId: number | null = null;

  destroy$ = new Subject<void>

  constructor(
    private gs: GlobalService,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes["equipoId"]){
      this.equipoId = changes["equipoId"].currentValue
      if (this.tipoEditable == this.tipoEditableBox.ALINEACION)
        this.players = this.gs.jugadoresEnCampeonato.get(this.equipoId)!
      //console.log("ngOnChanges equipoId", this.equipoId)
      //console.log("ngOnChanges players", this.players)
    }

    if (changes["items"]){
      this.items = changes["items"].currentValue
      //console.log("ngOnChanges items", this.items)
    }
  }

  ngOnInit() {
    //console.log(`ngOnInit()-${this.title}`, this.items)

    if (this.tipoEditable != this.tipoEditableBox.ALINEACION)
      this.subscribeJugadoresEnAlineacion()
  }

  addItem() {
    if (this.isOpen == true) {
      this.toggle();
    }

    this.items.push({
      partidoId: this.gs.partidoFutbolDTO.id,
      equipoId: this.equipoId,
      enContra: this.golContra
    })
    
    if (this.cambiaPuntaje)
      this.onCambiarPuntaje.emit(this.equipoId)

    console.log("items", this.numeroEquipo, this.items)

    //this.updateItems();
  }

  removeItem(index: number) {
    this.itemsEliminar.push(this.items[index])
    this.items.splice(index, 1);

    if (this.cambiaPuntaje)
      this.onCambiarPuntaje.emit(this.equipoId)
    //console.log(`items eliminados`, this.itemsEliminar)
  }

  toggle() {
    this.isOpen = !this.isOpen;
  }
  
  // Manejar cambios en las selecciones de jugador
  onPlayerChange(index: number, type: 'entra' | 'sale') {
    const jugador = this.items[index][type === 'entra' ? 'jugadorEntraId' : 'jugadorSaleId'];

    // Validar que no se seleccione el mismo jugador en ambas categorías
    if (this.items[index].jugadorEntraId === this.items[index].jugadorSaleId) {
      alert('No puedes seleccionar el mismo jugador que entra y sale.');
      this.items[index][type === 'entra' ? 'jugadorSaleId' : 'jugadorEntraId'] = null;
    }
    
  }
  
  getCardClass(color: number): string {
    switch (color) {
      case 1: // Amarilla
        return 'card-icon-yellow';
      case 2: // Roja
        return 'card-icon-red';
      case 3: // Azul
        return 'card-icon-blue'; // Si estás usando el color azul
      default:
        return 'card-icon-yellow';
    }
  }

  changeCardType(index: number, increment: number) {
    const cardColors = [1, 2, 3]; // 1: Amarilla, 2: Roja, 3: Azul (si es aplicable)
    const currentColor = this.items[index].color;
    let newIndex = cardColors.indexOf(currentColor) + increment;
    if (newIndex < 0) {
      newIndex = cardColors.length - 1;
    } else if (newIndex >= cardColors.length) {
      newIndex = 0;
    }
    this.items[index].color = cardColors[newIndex];
  }  

  onCipChange(idx: number){
    //console.log(`onCipChange(${idx})\nitems:`, this.items)
    let jugador: any
    if(this.players)
      jugador = this.players.find(jugador => jugador.codCIP === this.items[idx].codCIP)
    if(jugador){
      this.items[idx].nombre = jugador.name
      this.items[idx].jugadorId = jugador.id
    }
    else this.items[idx].nombre = ''
  }

  subscribeJugadoresEnAlineacion(){
    let jugadoresEnAlineacion$: Observable<Player[]>
    if (this.numeroEquipo == 1)
      jugadoresEnAlineacion$ = this.gs.jugadoresEnAlineacion1$
    else
      jugadoresEnAlineacion$ = this.gs.jugadoresEnAlineacion2$

    if (this.tipoEditable != this.tipoEditableBox.ALINEACION)
      jugadoresEnAlineacion$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.players = data
      })
  }

  ngOnDestroy(): void {
    this.destroy$.next()
    this.destroy$.complete()
  }
}
