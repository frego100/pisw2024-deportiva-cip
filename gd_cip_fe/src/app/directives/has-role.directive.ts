import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { RoleService } from '../services/role.service';

@Directive({
  selector: '[appHasRole]'
})
export class HasRoleDirective {
  private currentRole: string | null = null;

  constructor(
    private roleService: RoleService,
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
  ) {
    this.roleService.currentRole$.subscribe(role => {
      this.currentRole = role;
      this.updateView();
    });
  }

  @Input() set appHasRole(allowedRoles: string | string[]) {
    this.roles = Array.isArray(allowedRoles) ? allowedRoles : [allowedRoles];
    this.updateView();
  }

  private roles: string[] = [];

  private updateView(): void {
    this.viewContainer.clear();

    if (this.checkRolePermission()) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    }
  }

  private checkRolePermission(): boolean {
    if (!this.currentRole) return false;
    return this.roles.includes(this.currentRole);
  }
}
