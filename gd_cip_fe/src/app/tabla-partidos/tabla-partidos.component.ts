import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { lastValueFrom, Subject, take } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { EditarResultadoComponent } from '../editar-resultado/editar-resultado.component';
import { Campeonato } from '../services/campeonato.service';
import { Equipo } from '../services/equipo.service';
import { GlobalService } from '../services/global.service';
import { PartidoDTO, PartidosService } from '../services/partidos.service';
import { Auth0Service } from '../auth0.service';

declare var bootstrap: any;

interface MatchDate {
  fecha: string | null;
  partidos: PartidoDTO[];
}

@Component({
  selector: 'app-tabla-partidos',
  templateUrl: './tabla-partidos.component.html',
  styleUrls: ['./tabla-partidos.component.css'],
})

export class TablaPartidosComponent implements OnInit {
  @Input() campeonato!: Campeonato
  @Output() onVistaPartidoOpen = new EventEmitter<void>();

  esAdministrador: boolean = false;

  matchDates: MatchDate[] = [];
  eliminatoriaDates: MatchDate[] = [];
  eliminatoriaPhaseSelectedDate: MatchDate[] = [];
  matches: PartidoDTO[] = [];

  equiposMap: Map<number, Equipo> = new Map();
  selectedDate: string | null = null;
  phase: 'grupos' | 'eliminatorias' = 'grupos';
  eliminatoriaPhases: string[] = [];
  isEliminatorias: boolean = false;
  selectedEliminatoriaPhase: string | null = null;
  eliminatoriasCreated: boolean = false;

  //Array vacio para pasarte los equipos clasificados, estoy usando el global service
  equiposClasificados: any[] = [];

  @ViewChild(EditarResultadoComponent) editarResultadoComponent!: EditarResultadoComponent;
  @ViewChild('confirmation') confirmation!: ConfirmationComponent;
  @ViewChild('confirmationPopup') confirmationPopup!: ConfirmationPopupComponent;
  @ViewChild('phaseChange') phaseChange!: ConfirmationComponent;
  @ViewChild('phaseChanged') phaseChanged!: ConfirmationComponent;

  //Para pasar el listado de partidos de eliminatorias

  partidoFutbolEstado = this.gs.partidoFutbolEstado;
  fechaSeleccionadaParaEliminar: string | null = null;

  private destroy$ = new Subject<void>();

  constructor(
    private partidosService: PartidosService,
    private authService: Auth0Service,
    private gs: GlobalService,
  ) { }

  ngOnInit(): void {
    this.authService.getCurrentUserRole().subscribe((rol) => {
      this.esAdministrador = rol === 'Administrador';
    });
    this.loadMatches();

    // Escuchar las actualizaciones emitidas desde el GlobalService
    /* this.gs.updatePartido$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        console.log('Se recibió una actualización de partidos');
        this.loadMatches();
      }); */
  }

  loadMatches(): void {
    this.partidosService.getPartidosByCampeonato(this.campeonato.id).subscribe(
      (data) => {
        console.log("loadMAtches()", data)
        const processedData = data.map((match: PartidoDTO) => ({
          id: match.id,
          campeonatoId: match.campeonatoId,
          equipo1Id: match.equipo1Id,
          equipo2Id: match.equipo2Id,
          fecha: match.fecha ? moment(match.fecha).format('YYYY-MM-DD') : null,
          tipoResultado: match.tipoResultado || 'PREPARACION',
          marcadorEquipo1: match.marcadorEquipo1 || 0,
          marcadorEquipo2: match.marcadorEquipo2 || 0,
          mejorJugadorId: match.mejorJugadorId,
          mejorPorteroId: match.mejorPorteroId,
          fase: match.fase || 1,
          grupo: match.grupo || '',
        }));

        this.gs.partidosSubject$
          .pipe(takeUntil(this.destroy$))
          .subscribe(data => {
            this.matches = data;
          });

        console.log("Partidos:", this.gs.partidosSubject$);

        this.matchDates = this.groupMatchesByDate(
          processedData.filter((match) => match.fase === this.gs.Fase.GRUPOS)
        );
        this.eliminatoriaDates = this.groupMatchesByDate(
          processedData.filter((match) => match.fase > this.gs.Fase.GRUPOS)
        );
        //Actualiza la informacion de partidos en el arbol
        this.gs.updateEliminatoriaDates(this.eliminatoriaDates);

        //Descomentar para que una vez se tenga partidos en eliminatorias no se puedan editar la fase de grupos
        if (this.eliminatoriaDates.length > 0) {
          this.eliminatoriasCreated = true;
        }
        console.log("Fecha seleccionada", this.selectedDate)
        if (!this.selectedDate) {
          this.selectedDate = this.matchDates.length > 0
            ? this.matchDates[0].fecha
            : moment().format('YYYY-MM-DD');
        }
        console.log("Fecha cambiada", this.selectedDate)

        if (!this.selectedEliminatoriaPhase && this.eliminatoriaPhases.length > 0) {
          this.selectedEliminatoriaPhase = this.eliminatoriaPhases[0];
        }

        this.eliminatoriaPhases = Array.from(
          new Set(this.eliminatoriaDates.map((date) => this.getFaseName(date.partidos[0].fase)))
        );

        if (this.eliminatoriaPhases.length > 0) {
          this.eliminatoriaPhaseSelectedDate = this.eliminatoriaDates.filter((date) =>
            date.partidos.some((p) => p.fase === this.getFaseValue(this.selectedEliminatoriaPhase || 'Octavos'))
          );
        }

        this.gs.equiposEnCampeonato$.pipe(take(1)).subscribe((equipos) => {
          this.equiposMap = new Map(equipos.map((equipo) => [equipo.id, equipo]));
        });
      },
      (error) => {
        console.error('Error al cargar los partidos', error);
      }
    );
  }
  getDateLabel(item: MatchDate): string {
    const index = this.matchDates.findIndex(date => date.fecha === item.fecha);
    return index >= 0 ? `${index + 1}° fecha` : 'Sin fecha';
  }
  getDateLabelByIndex(index: number): string {
    return `${index + 1}° fecha`;
  }

  groupMatchesByDate(matches: any[]): MatchDate[] {
    const grouped = matches.reduce((acc: any, match: any) => {
      const key = `${match.fase}-${match.fecha}`;
      if (!acc[key]) {
        acc[key] = { fecha: match.fecha, fase: match.fase, partidos: [] };
      }
      acc[key].partidos.push(match);
      return acc;
    }, {});
    return Object.values(grouped);
  }

  // Método para obtener el nombre de un equipo dado su ID
  getEquipoNombre(id: number): string {
    return this.equiposMap.get(id)?.nombre || 'Equipo';
  }
  // Determina y genera partidos de eliminatorias según la cantidad de equipos
  async initializeEliminatorias(): Promise<void> {
    const totalEquipos = this.equiposClasificados.length;

    let faseInicial = this.gs.Fase.OCTAVOS;
    if (totalEquipos < 3) {
      this.eliminatoriaPhases = [];
      this.removeMatchesByPhase(this.gs.Fase.FINAL);
      this.removeMatchesByPhase(this.gs.Fase.SEMIFINAL);
      return;
    } else if (totalEquipos == 4) {
      faseInicial = this.gs.Fase.SEMIFINAL;
    } else if (totalEquipos == 8) {
      faseInicial = this.gs.Fase.CUARTOS;
    }

    this.eliminateExtraPhases(faseInicial);

    let existingMatchesBackend: any[] | undefined;
    try {
      if (!this.campeonato || !this.campeonato.id) {
        console.error('Error: campeonato o campeonato.id no está definido');
        return;
      }
      existingMatchesBackend = await lastValueFrom(this.partidosService.getPartidosByCampeonato(this.campeonato.id));
    } catch (error) {
      console.error('Error al obtener los partidos existentes:', error);
      return;
    }

    while (faseInicial >= this.gs.Fase.FINAL) {
      if (this.eliminatoriaPhases.includes(this.getFaseName(faseInicial))) {
        faseInicial--;
        continue;
      }
      if (!this.eliminatoriaPhases.includes(this.getFaseName(faseInicial))) {
        this.eliminatoriaPhases.push(this.getFaseName(faseInicial));
      }
      const existingMatches = existingMatchesBackend!.filter((p) => p.fase === faseInicial).length;
      const expectedMatches = this.getExpectedMatchesCount(this.getFaseName(faseInicial));
      if (existingMatches < expectedMatches) {
        const partidosPorCrear = expectedMatches - existingMatches;
        for (let i = 0; i < partidosPorCrear; i++) {
          await this.addMatch(faseInicial);
        }
      }
      faseInicial--;
    }

    this.eliminatoriaPhases.sort((a, b) => {
      if (a === 'Octavos') return -1;
      if (a === 'Cuartos' && b === 'Semifinal') return -1;
      if (a === 'Cuartos' && b === 'Final') return -1;
      if (a === 'Semifinal' && b === 'Final') return -1;
      return 1;
    });

    this.selectedEliminatoriaPhase = this.selectedEliminatoriaPhase || this.eliminatoriaPhases[0]
    console.log('FASE SELECCIONADA:', this.selectedEliminatoriaPhase);
    this.onDateSelectChange({ target: { value: this.selectedEliminatoriaPhase } } as any);
    this.eliminatoriaPhaseSelectedDate = this.eliminatoriaDates.filter((date) =>
      date.partidos.some((p) => p.fase === this.getFaseValue(this.selectedEliminatoriaPhase || 'Octavos'))
    );

    //Permite listar los equipos correspondientes dentro del arbol
    this.gs.updateEliminatoriaDates(this.eliminatoriaDates);
  }

  private getFaseName(phase: number): string {
    switch (phase) {
      case this.gs.Fase.OCTAVOS: return 'Octavos';
      case this.gs.Fase.CUARTOS: return 'Cuartos';
      case this.gs.Fase.SEMIFINAL: return 'Semifinal';
      case this.gs.Fase.FINAL: return 'Final';
      default: return '';
    }
  }

  private eliminateExtraPhases(faseInicial: number): void {
    const fasesActuales = this.eliminatoriaPhases.slice();
    for (const fase of fasesActuales) {
      const faseNum = this.getFaseValue(fase);
      if (faseNum > faseInicial) {
        this.removeMatchesByPhase(faseNum);
        this.eliminatoriaPhases = this.eliminatoriaPhases.filter(
          (f) => f !== fase
        );
      }
    }
  }

  removeMatchesByPhase(phase: number): void {
    const partidosIds = this.eliminatoriaDates
      .flatMap((date) => date.partidos)
      .filter((partido) => partido.fase === phase)
      .map((partido) => partido.id)
      .filter((id) => id !== undefined);

    partidosIds.forEach((partidoId) => {
      this.partidosService.deletePartido(partidoId as number).subscribe(
        () => {
          this.loadMatches();
        },
        (error) => {
          console.error(`Error eliminando partido con ID ${partidoId}:`, error);
        }
      );
    });
  }

  // Método para remover la fecha seleccionada y los partidos asociados
  removeFecha(selectedDate: string | null): void {
    this.fechaSeleccionadaParaEliminar = selectedDate;
    this.confirmationPopup.show();
  }

  // Se ejecuta cuando se confirma la eliminación de una fecha
  async handleConfirmDelete(): Promise<void> {
    if (!this.fechaSeleccionadaParaEliminar) return;

    const index = this.matchDates.findIndex(date => date.fecha === this.fechaSeleccionadaParaEliminar);

    if (index < 0) {
      console.error('Fecha no encontrada o no hay fechas para eliminar.');
      return;
    }

    const partidosIds = this.matchDates[index].partidos
      .map(partido => partido.id)
      .filter(id => id !== undefined);

    try {
      // Elimina los partidos asociados a la fecha
      for (const partidoId of partidosIds) {
        await lastValueFrom(this.partidosService.deletePartido(partidoId as number));
        console.log(`Partido con ID ${partidoId} eliminado.`);
      }

      // Elimina la fecha seleccionada
      this.matchDates.splice(index, 1);

      // Actualiza `selectedDate` con la primera fecha disponible
      if (this.matchDates.length > 0) {
        this.selectedDate = this.matchDates[0].fecha;
      } else {
        this.selectedDate = moment().format('YYYY-MM-DD');
      }

      console.log('Fechas actualizadas:', this.matchDates);
      console.log('selectedDate actualizado:', this.selectedDate);

      this.loadMatches(); // Recargar los partidos después de actualizar las fechas

      this.confirmation.message = 'Fecha eliminada correctamente.';
      this.confirmation.type = 'success';
      this.confirmation.show();
    } catch (error) {
      console.error('Error al eliminar partidos:', error);
      this.confirmation.message = 'Error al eliminar partido o fecha.';
      this.confirmation.type = 'danger';
      this.confirmation.show();
    } finally {
      this.fechaSeleccionadaParaEliminar = null;
    }
  }

  // Se ejecuta si se cancela la eliminación
  handleCancelDelete(): void {
    this.fechaSeleccionadaParaEliminar = null;
  }

  // Maneja el cambio en el input de tipo date para modificar la fecha de los partidos
  onDateChange(event: Event): void {
    const input = event.target as HTMLInputElement;
    this.selectedDate = input.value;
    this.filterMatchesByDate();
  }

  // Maneja el cambio en el select y actualiza la lista de partidos
  onDateSelectChange(event: Event): void {
    const selectedPhase = (event.target as HTMLSelectElement).value;
    if (this.phase === 'eliminatorias') {
      this.selectedEliminatoriaPhase = selectedPhase;
      const phaseValue = this.getFaseValue(this.selectedEliminatoriaPhase || 'Octavos');
      this.eliminatoriaPhaseSelectedDate = this.eliminatoriaDates.filter((date) =>
        date.partidos.some((partido) => partido.fase === phaseValue)
      );
    }
  }

  private getExpectedMatchesCount(phase: string): number {
    switch (phase) {
      case 'Octavos': return 8;
      case 'Cuartos': return 4;
      case 'Semifinal': return 2;
      case 'Final': return 2;
      default: return 0;
    }
  }

  private getFaseValue(phase: string): number {
    switch (phase) {
      case 'Octavos': return this.gs.Fase.OCTAVOS;
      case 'Cuartos': return this.gs.Fase.CUARTOS;
      case 'Semifinal': return this.gs.Fase.SEMIFINAL;
      case 'Final': return this.gs.Fase.FINAL;
      default: return -1; // O algún valor por defecto
    }
  }

  // Actualiza la lista de partidos a mostrar basados en la fecha seleccionada
  filterMatchesByDate(): void {
    if (this.phase === 'grupos') {
      this.matchDates.filter((date) => date.fecha === this.selectedDate);
    } else if (this.phase === 'eliminatorias' && this.selectedEliminatoriaPhase) {
      this.eliminatoriaDates.filter(
        (date) => date.partidos[0].fase === this.getFaseValue(this.selectedEliminatoriaPhase || 'Octavos')
      );
    }
  }

  // Agrega un partido a la fecha seleccionada
  async addMatch(phase: number): Promise<void> {
    const newMatch = {
      campeonatoId: this.campeonato.id,
      fecha: this.selectedDate,
      tipoResultado: 'PREPARACION',
      marcadorEquipo1: 0,
      marcadorEquipo2: 0,
      equipo1: null,
      equipo2: null,
      fase: phase || 1,
      grupo: null,
    };
    return new Promise((resolve, reject) => {
      this.partidosService.addPartido(newMatch).subscribe(
        (response) => {
          const match: PartidoDTO = {
            ...newMatch,
            id: response.id,
            equipo1Id: response.equipo1Id?.id || 0,
            equipo2Id: response.equipo2Id?.id || 0,
            fase: response.fase || 1,
            grupo: response.grupo || '',
            mejorJugadorId: response.mejorJugadorId,
            mejorPorteroId: response.mejorPorteroId,
          };
          if (this.phase === 'eliminatorias') {
            const phaseDate = this.eliminatoriaDates.find((d) => d.fecha === this.selectedDate);
            if (phaseDate) {
              phaseDate.partidos.push(match);
            } else {
              this.eliminatoriaDates.push({ fecha: this.selectedDate, partidos: [match] });
            }
          } else {
            const groupDate = this.matchDates.find((d) => d.fecha === this.selectedDate);
            if (groupDate) {
              groupDate.partidos.push(match);
            } else {
              this.matchDates.push({ fecha: this.selectedDate, partidos: [match] });
            }
          }
          this.matchDates = this.groupMatchesByDate(
            this.matchDates.flatMap((date) => date.partidos).filter((match) => match.fase === this.gs.Fase.GRUPOS)
          );
          this.eliminatoriaDates = this.groupMatchesByDate(
            this.eliminatoriaDates.flatMap((date) => date.partidos).filter((match) => match.fase > this.gs.Fase.GRUPOS)
          );
          resolve();
        },
        (error) => {
          console.error('Error al agregar partido:', error);
          reject(error);
        }
      );
    });
  }

  // Método para finalizar la fase
  finalizarFase(): void {
    const partidosSinFinalizar = this.matchDates
      .flatMap(date => date.partidos)
      .filter(partido => partido.tipoResultado !== 'REALIZADO');
    if (partidosSinFinalizar.length > 0) {
      this.phaseChanged.message = 'Partidos Sin Finalizar';
      this.phaseChanged.type = 'warning';
      this.phaseChanged.show();
      return;
    }
    if (this.eliminatoriasCreated === false && this.esAdministrador === true && this.eliminatoriaDates.length === 0) {
      this.phaseChange.show();
    } else {
      this.handleConfirmarFinalizacion();
    }
  }

  // Método de confirmación del popup para finalizar fase
  handleConfirmarFinalizacion(): void {
    this.phase = 'eliminatorias';
    if (this.eliminatoriasCreated === false && this.eliminatoriaDates.length === 0) {
      this.phaseChanged.message = 'Fase Eliminatorias Creada';
      this.phaseChanged.type = 'success';
      this.phaseChanged.show();
    }
    this.eliminatoriasCreated = true;
    this.gs.finalizarFase(this.campeonato.id);

    this.gs.getEquiposClasificados$(this.campeonato.id).pipe(take(1)).subscribe(equipos => {
      this.equiposClasificados = equipos;
      if (this.equiposClasificados.length === 0) {
        console.error('No se recibieron equipos clasificados.');
      } else {
        this.initializeEliminatorias();
      }
    });
  }

  // Método para regresar a la fase de grupos
  regresarAFaseGrupos(): void {
    this.phase = 'grupos';
    this.isEliminatorias = false;
    this.loadMatches();
  }

  async openOptions(match: PartidoDTO): Promise<void> {
    const modalElement = document.getElementById('matchModal');
    const modal = new bootstrap.Modal(modalElement);
    this.gs.partidoId = match.id || 0
    this.gs.partidoFutbolDTO = match
    this.editarResultadoComponent?.ngOnInit()
    this.editarResultadoComponent.onGuardarExitoso.pipe(take(1)).subscribe(() => {
      this.cerrarModal();
      // Actualizar el partido de la siguiente fase
      this.updateNextPhase(this.gs.partidoFutbolDTO)
        .then(() => console.log('Fase siguiente actualizada correctamente'))
        .catch(err => console.error('Error al actualizar fase siguiente:', err));
    });
    modal.show();
  }

  private async updateNextPhase(match: PartidoDTO): Promise<void> {
    console.log('PARTIDO OBTENIDO', match);
    if (match.fase === 1) return;
    const partido = await lastValueFrom(this.partidosService.getPartidoById(match.id));
    match = partido;
    console.log('PARTIDO DE BACK', partido);
    if (partido.tipoResultado !== 'REALIZADO') {
      console.log('El partido no está marcado como REALIZADO.');
      return;
    }
    console.log('PARTIDO ACTUALIZADO:', match);
    const equipoGanadorId =
      match.marcadorEquipo1 > match.marcadorEquipo2
        ? match.equipo1Id
        : match.equipo2Id;
    console.log('Equipo ganador:', equipoGanadorId);
    const equipoPerdedorId =
      match.marcadorEquipo1 < match.marcadorEquipo2
        ? match.equipo1Id
        : match.equipo2Id;
    console.log('Equipo perdedor:', equipoPerdedorId);

    if (equipoGanadorId === equipoPerdedorId || !equipoGanadorId || !equipoPerdedorId) {
      console.warn('No se pudo determinar el ganador del partido:', match);
      return;
    }
    console.log('Equipo ganador:', equipoGanadorId, 'Equipo perdedor:', equipoPerdedorId);
    const partidosFase = this.eliminatoriaDates
      .flatMap(date => date.partidos)
      .filter(p => p.fase === match.fase);
    // Obtener la fase siguiente
    const ordenActual = partidosFase.findIndex(p => p.id === match.id);
    console.log(ordenActual)
    const nextPhase = match.fase - 1; // Ejemplo: de cuartos (3) a semifinal (2)

    if (nextPhase > 1) {
      // Buscar el partido correspondiente en la fase siguiente
      console.log('Fase actual:', match.fase, 'Fase siguiente:', nextPhase);

      const partidosFaseSiguiente = this.eliminatoriaDates
        .flatMap(date => date.partidos)
        .filter(p => p.fase === nextPhase);
      console.log('Partidos en la fase actual:', partidosFase);
      console.log('Partidos en la fase siguiente:', partidosFaseSiguiente);

      // Seleccionar el partido correcto según el orden
      let nextMatchIndex = Math.floor(ordenActual / 2);
      const nextMatch = partidosFaseSiguiente[nextMatchIndex];
      console.log('Partido siguiente:', nextMatch);

      if (nextMatch) {
        if (
          nextMatch.equipo1Id === equipoGanadorId ||
          nextMatch.equipo2Id === equipoGanadorId
        ) {
          console.warn('El equipo ya está asignado en el partido siguiente:', nextMatch.id);
          return;
        }

        if (ordenActual % 2 === 0) {
          nextMatch.equipo1Id = equipoGanadorId;
        } else if (ordenActual % 2 !== 0) {
          nextMatch.equipo2Id = equipoGanadorId;
        }

        try {
          await lastValueFrom(this.partidosService.savePartido(nextMatch.id, nextMatch));
          console.log(`Equipo ganador asignado al partido siguiente (ID: ${nextMatch.id})`);
        } catch (error) {
          console.error('Error al actualizar el partido siguiente:', error);
        }
      } else {
        console.warn(`No se encontró un partido en la fase ${this.getFaseName(nextPhase)} con el índice adecuado.`);
      }
    }

    // Manejar el partido por el tercer puesto si es una semifinal
    if (match.fase === 3) {
      const partidosTercerPuesto = this.eliminatoriaDates
        .flatMap(date => date.partidos)
        .filter(p => p.fase === 2);
      console.log('Partidos por el tercer puesto:', partidosTercerPuesto);

      const partidoTercerPuesto = partidosTercerPuesto[1];
      console.log('Partido por el tercer puesto:', partidoTercerPuesto);

      if (partidoTercerPuesto) {
        // Asignar el equipo perdedor al tercer puesto
        if (
          partidoTercerPuesto.equipo1Id === equipoPerdedorId ||
          partidoTercerPuesto.equipo2Id === equipoPerdedorId
        ) {
          console.warn('El equipo ya está asignado en el partido por el tercer puesto:', partidoTercerPuesto.id);
          return;
        }
        if (ordenActual % 2 === 0) {
          partidoTercerPuesto.equipo1Id = equipoPerdedorId; // Lado izquierdo
        } else if (ordenActual % 2 !== 0) {
          partidoTercerPuesto.equipo2Id = equipoPerdedorId; // Lado derecho
        }

        try {
          await lastValueFrom(this.partidosService.savePartido(partidoTercerPuesto.id, partidoTercerPuesto));
          console.log(`Equipo perdedor asignado al partido por el tercer puesto (ID: ${partidoTercerPuesto.id})`);
        } catch (error) {
          console.error('Error al actualizar el partido de tercer puesto:', error);
        }
      } else {
        console.warn('No se encontró un partido para el tercer puesto.');
      }
    }
    this.loadMatches();
  }

  openTeamModal(): void {
    const modalElement = document.getElementById('teamModal');
    const modal = new bootstrap.Modal(modalElement);
    modal.show();
  }

  onVistaPartido(): void {
    this.onVistaPartidoOpen.emit();
  }

  cerrarModal() {
    const modalElement = document.getElementById('matchModal');
    const modal = bootstrap.Modal.getInstance(modalElement);
    modal.hide();
  }

  handleGuardarExitoso(exito: boolean) {
    this.ngOnInit()
  }
  getMatchBackgroundColor(match: any, index: number): any {
    const baseColor = 'white'; // You can set a default background color here
    const partidosFinal = this.eliminatoriaDates.filter((date) => date.partidos.some((p) => p.fase === 2))[0];
    if (match.fase === 2 && match === partidosFinal.partidos[0]) {
      const style = {
        'background-color': '#ddffdb',
        'font-weight': 'bold',
        'color': 'black',
        'border-radius': '10px',
      }
      return style; // Replace with your desired highlight color
    }
    return { 'background-color': baseColor };
  }
}