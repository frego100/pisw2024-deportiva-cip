import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClasificacionService } from '../services/clasificacion.service';

@Component({
  selector: 'app-clasificacion-usuario',
  templateUrl: './clasificacion-usuario.component.html',
  styleUrls: ['./clasificacion-usuario.component.css']
})
export class ClasificacionUsuarioComponent implements OnInit {

  selectedView: string = 'admin';
  displayedColumns: string[] = ['position', 'team', 'pts', 'j', 'g', 'e', 'p', 'gf', 'gc', 'dif'];
  isGrouped: boolean = false;
  selectedPhase: string = '1';

  allTeamsDataSource: any[] = []; // Datos de equipos genéricos
  groups: any[] = [];

  semiFinalMatches: { team1: string; team2: string; }[] = [];
  finalMatch: { team1: string; team2: string; } = { team1: '', team2: '' };

  constructor(private clasificacionService: ClasificacionService, private router: Router) {}

  ngOnInit(): void {
    //this.loadClasificacionData();
  }

  // Carga de datos de clasificación
  /*loadClasificacionData(): void {
    this.clasificacionService.getClasificacionData().subscribe(data => {
      
      this.allTeamsDataSource = data.detalle.map(d => ({
        id:d.clasificacionDet.claDetEquipoId,
        position: d.posicion,
        team: `Equipo ${d.clasificacionDet.claDetEquipoId}`, 
        pts: d.clasificacionDet.claDetPuntos ?? 0,
        j: d.clasificacionDet.claDetJuegos ?? 0,
        g: d.clasificacionDet.claDetGanados ?? 0,
        e: d.clasificacionDet.claDetEmpates ?? 0,
        p: d.clasificacionDet.claDetPerdidos ?? 0,
        gf: d.clasificacionDet.claDetGolesFavor ?? 0,
        gc: d.clasificacionDet.claDetGolesContra ?? 0,
        dif: d.clasificacionDet.claDetDiferenciaGoles ?? 0
      }));
      
      this.updatePhaseData(); 
    });
  }*/

  onPhaseChange(event: any): void {
    this.selectedPhase = event.target.value;
    this.updatePhaseData();
  }

  toggleGroup(event: any): void {
    this.isGrouped = event.checked;
  }

  updatePhaseData(): void {
    if (this.selectedPhase === '2') {
      const topTeams = this.allTeamsDataSource.slice(0, 4); // Ajustar según el criterio de selección
      this.semiFinalMatches = [
        { team1: topTeams[0].team, team2: topTeams[1].team },
        { team1: topTeams[2].team, team2: topTeams[3].team }
      ];
      this.finalMatch = { team1: '', team2: '' }; // Actualizar según los resultados
    }
  }

  setTeamData(row: any): void {
    this.clasificacionService.setSelectedTeam({
      name: row.team,
      wins: row.g,
      draws: row.e,
      loses: row.p,
      id:row.id
    });
    
    this.router.navigate(['/equipo-vista']);
  }

  onRowClick(row: any): void {
    this.setTeamData(row);
  }

  onRadioChange(event: Event): void {
    const inputElement = event.target as HTMLInputElement;
    this.selectedView = inputElement.value;
  }
}

