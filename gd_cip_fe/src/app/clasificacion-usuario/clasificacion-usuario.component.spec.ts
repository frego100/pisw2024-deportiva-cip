import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasificacionUsuarioComponent } from './clasificacion-usuario.component';

describe('ClasificacionUsuarioComponent', () => {
  let component: ClasificacionUsuarioComponent;
  let fixture: ComponentFixture<ClasificacionUsuarioComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ClasificacionUsuarioComponent]
    });
    fixture = TestBed.createComponent(ClasificacionUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
