import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipoVistaComponent } from './equipo-vista.component';

describe('EquipoVistaComponent', () => {
  let component: EquipoVistaComponent;
  let fixture: ComponentFixture<EquipoVistaComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EquipoVistaComponent]
    });
    fixture = TestBed.createComponent(EquipoVistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
