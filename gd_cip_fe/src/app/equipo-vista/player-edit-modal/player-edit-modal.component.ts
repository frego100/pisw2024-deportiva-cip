// player-edit-modal.component.ts
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-player-edit-modal',
    templateUrl: './player-edit-modal.component.html',
    styleUrls: ['./player-edit-modal.component.css']
})
export class PlayerEditModalComponent {
    @Input() editedPlayer: any; // Datos del jugador a editar
    @Output() savePlayerChanges = new EventEmitter<any>();
    @Output() closeModalEvent = new EventEmitter<void>();

    saveChanges() {
        // Lógica para guardar los cambios 
        this.savePlayerChanges.emit(this.editedPlayer);
        this.closeModal();
    }

    closeModal() {
        this.closeModalEvent.emit();
    }
}
