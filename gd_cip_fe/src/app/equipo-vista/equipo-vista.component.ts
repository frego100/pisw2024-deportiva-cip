import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Equipo } from '../services/equipo.service';
import { forkJoin, Subject, takeUntil, switchMap, of, map, EMPTY, from, mergeMap, toArray, Observable, tap } from 'rxjs';
import { ClasificacionService } from '../services/clasificacion.service';
import { Alineacion, EquipoVistaService, Falta, Gol, JugadaPartido, Jugador, Partido, Tarjeta } from '../services/equipo-vista.service';
import { PartidosService } from '../services/partidos.service';
import { GlobalService } from '../services/global.service';
import { Campeonato } from '../services/lista-campeonatos.service';

@Component({
  selector: 'app-equipo-vista',
  templateUrl: './equipo-vista.component.html',
  styleUrls: ['./equipo-vista.component.css']
})

export class EquipoVistaComponent implements OnInit, OnDestroy {
  @Input() campeonato!: Campeonato;
  private campeonatoId!: number;
  private destroy$ = new Subject<void>();

  selectedView: string = 'admin';
  team = { name: '', wins: 0, draws: 0, losses: 0, image: '/assets/team1-icon.png' };
  players: any[] = [];
  matches: any[] = [];

  equiposMap: Map<number, Equipo> = new Map();

  private filteredMatches: any[] = []; // Partidos filtrados por equipo

  constructor(
    private clasificacionService: ClasificacionService,
    private gs: GlobalService,
    private location: Location,
    private equipoVistaService: EquipoVistaService,
    private partidosService: PartidosService
  ) {}

  ngOnInit(): void {

    this.gs.equiposEnCampeonato$
      .pipe(
        takeUntil(this.destroy$)
      )
      .subscribe(equipos => {
        // Almacenar los equipos en el mapa
        this.equiposMap = new Map(equipos.map(equipo => [equipo.id, equipo]));
        console.log('Equipos cargados en el mapa:', this.equiposMap);
      });

    this.gs.campeonatoSelected$
      .pipe(
        takeUntil(this.destroy$),
        switchMap(id => {
          console.log('Campeonato seleccionado:', id);

          if (id === -1) {
            console.warn('ID de campeonato no disponible');
            return of([]);
          }

          this.campeonatoId = id;

          const selectedTeam = this.clasificacionService.getSelectedTeam();
          if (!selectedTeam) return EMPTY;

          return forkJoin({
            equipoData: this.loadEquipoData(selectedTeam),
            partidos: this.loadPartidos(selectedTeam),
          });
        })
      )
      .subscribe({
        next: data => console.log('Datos cargados correctamente', data),
        error: (err) => console.error('Error al cargar los datos:', err)
      });
  }

  loadEquipoData(selectedTeam: any): Observable<any> {
    return of(selectedTeam).pipe(
      tap(team => {
        this.team = {
          name: team.name,
          wins: team.wins,
          draws: team.draws,
          losses: team.loses,
          image: '/assets/team1-icon.png'
        };
      })
    );
  }

  loadPartidos(selectedTeam: any): Observable<any> {
    console.log('LOad Partidos')

    return this.partidosService.getPartidosByCampeonato(this.campeonatoId).pipe(
      tap(partidos => {
        // Filtrar partidos por equipo
        this.filteredMatches = partidos.filter(match =>
          match.equipo1Id && match.equipo2Id &&
          (match.equipo1Id=== selectedTeam.id || match.equipo2Id === selectedTeam.id)
          
        );

        
        // Mapear para incluir iconos
        this.matches = this.filteredMatches.map(match => ({
          ...match,
          equipo1: { ...match.equipo1Id, nombre: this.getEquipoNombre(match.equipo1Id) , icon1: '/assets/team1-icon.png' },
          equipo2: { ...match.equipo2Id, nombre: this.getEquipoNombre(match.equipo2Id), icon2: '/assets/team2-icon.png' }
        }));

        console.log('partidos de equipo', this.matches)
        

      }),
      // Cargar jugadores y actualizar estadísticas
      switchMap(() => this.loadJugadorYActualizarStats(selectedTeam))
    );
  }

  loadJugadorYActualizarStats(selectedTeam: any): Observable<any> {

    console.log('loadJugadorYActualizarStats')
  const equipoId = selectedTeam.id;

  console.log('equipoId', equipoId)
  // Verificar que el Map contiene datos para el campeonato actual
  const jugadoresCampeonato = this.gs.jugadoresEnCampeonato.get(equipoId);
  console.log('Jugadores', jugadoresCampeonato)

  if (!jugadoresCampeonato) {
    console.error('No se encontraron jugadores para el campeonato:', this.campeonatoId);
    return EMPTY;
  }

  // Continuar con la actualización de estadísticas
  const partidoIds = this.filteredMatches.map(match => match.id);
  console.log('Partidos', partidoIds)

  return this.updatePlayerStats(partidoIds, selectedTeam).pipe(
    tap(datosCombinados => {
      const jugadoresEnAlineacion = datosCombinados.alineacion.map((alineado: { jugadorId: any }) => alineado.jugadorId);
      console.log('Alineacion', jugadoresEnAlineacion)
      // Filtrar jugadores por alineación
      const jugadoresFiltrados = jugadoresCampeonato.filter(jugador => 
        jugadoresEnAlineacion.includes(jugador.id)
      );

      console.log('Alineacion filtrada', jugadoresFiltrados)

      // Mapear los jugadores filtrados al formato esperado
      this.players = jugadoresFiltrados.map(jugador => ({
        id: jugador.id,
        icon: '/assets/perfil.jpg',
        name: jugador.name || 'Desconocido',
        nrocamiseta: jugador.number || '',
        posicion: jugador.position || 'Sin posicion',
        goals: datosCombinados.goles.filter((gol: { jugadorId: any }) => gol.jugadorId === jugador.id).length,
        matches: datosCombinados.alineacion.filter((alineado: { jugadorId: any }) => alineado.jugadorId === jugador.id).length,
        yellowCards: datosCombinados.tarjetasAmarillas.filter((tarjeta: { jugadorId: any }) => tarjeta.jugadorId === jugador.id).length,
        redCards: datosCombinados.tarjetasRojas.filter((tarjeta: { jugadorId: any }) => tarjeta.jugadorId === jugador.id).length,
        blueCards: datosCombinados.tarjetasAzules.filter((tarjeta: { jugadorId: any }) => tarjeta.jugadorId === jugador.id).length,
        fouls: datosCombinados.faltas.filter((falta: { jugadorId: any }) => falta.jugadorId === jugador.id).length,
        assists: datosCombinados.goles.filter((gol: { asistenciaId: any }) => gol.asistenciaId === jugador.id).length,
      }));
    })
  );
}

  
  

  updatePlayerStats(partidoIds: number[], selectedTeam: any): Observable<any> {
    return from(partidoIds).pipe(
      mergeMap(id => this.partidosService.getPartidoEquipo(id, selectedTeam.id), 5),
      toArray(),
      map(partidoDataArray => this.combinarPartidoData(partidoDataArray)),
      tap(datosCombinados => {       
        this.players.forEach(player => {
          console.log('player', player)
          player.goals = datosCombinados.goles.filter((gol: { jugadorId: any; }) => gol.jugadorId === player.id).length;
          player.matches = datosCombinados.alineacion.filter((jugada: { jugadorId: any; }) => jugada.jugadorId === player.id).length;
          player.yellowCards = datosCombinados.tarjetasAmarillas.filter((tarjeta: { jugadorId: any; }) => tarjeta.jugadorId === player.id).length;
          player.redCards = datosCombinados.tarjetasRojas.filter((tarjeta: { jugadorId: any; }) => tarjeta.jugadorId === player.id).length;
          player.blueCards = datosCombinados.tarjetasAzules.filter((tarjeta: { jugadorId: any; }) => tarjeta.jugadorId === player.id).length;
          player.fouls = datosCombinados.faltas.filter((falta: { jugadorId: any; }) => falta.jugadorId === player.id).length;
          player.assists = datosCombinados.goles.filter((gol: { asistenciaId: any; }) => gol.asistenciaId === player.id).length;
        });
        console.log('datos combinados', datosCombinados)
      })
    );
  }

  combinarPartidoData(partidoDataArray: any[]) {
    return partidoDataArray.reduce((acc, data) => {
      for (const key of Object.keys(data)) {
        if (Array.isArray(acc[key])) {
          acc[key].push(...data[key]);
        }
      }
      return acc;
    }, {
      goles: [], tarjetasAmarillas: [], tarjetasRojas: [],
      tarjetasAzules: [], faltas: [], alineacion: []
    });
  }
  

  goBack(): void {
    this.location.back();
  }

  onRadioChange(event: Event) {
    const inputElement = event.target as HTMLInputElement;
    this.selectedView = inputElement.value;
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

   // Método para obtener el nombre del equipo dado su ID
   getEquipoNombre(id: number): string {
    return this.equiposMap.get(id)?.nombre || 'Equipo Desconocido';
  }

}
