import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GestInfoCampComponent } from 'src/app/gest-info-camp/gest-info-camp.component';

@Component({
  selector: 'app-match-summary',
  templateUrl: './match-summary.component.html',
  styleUrls: ['./match-summary.component.css']
})
export class MatchSummaryComponent {
  @Input() match: any;
  
  @Input()
  viewMode: string = 'user'; //Variable para cambiar de entre user y admin

  constructor(public dialog: MatDialog) {}


  openModal() { //Solo abre el modal de cambiar estado (gest-info-camp), se supone que ahi debe de ir el modal hecho para editar los resultados del partido
    const dialogRef = this.dialog.open(GestInfoCampComponent, {
      width: '250px',
      data: { state: this.match.state }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.match.state = result;
      }
    });
  }
//Tambien pertenece al cambio de estado, que pertenece  a gest-info-camp
  getStateClass(state: string) {
    return {
      'no-realizado': state === 'NO REALIZADO',
      'preparacion':state === 'PREPARACION',
      'en-vivo': state === 'EN VIVO',
      'finalizado': state === 'REALIZADO'
    };
  }

}
