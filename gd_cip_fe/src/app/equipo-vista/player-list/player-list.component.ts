import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PlayerEditModalComponent } from '../player-edit-modal/player-edit-modal.component'; 

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.css']
})
export class PlayerListComponent {
  @Input()
  players: any[] = [];

  @Input()
  viewMode: string = 'user'; //solo maneja las vistas

  constructor(public dialog: MatDialog) {} /*----------Mat dialog para abrir el modal de editar--------*/

  openEditModal() {
    this.dialog.open(PlayerEditModalComponent);// Clonar el objeto para evitar cambios directos
    // Lógica para abrir el modal (puedes usar Angular Material Dialog)
  }
}
