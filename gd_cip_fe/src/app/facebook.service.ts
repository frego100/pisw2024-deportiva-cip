import { Injectable } from '@angular/core';

declare var FB: any;

@Injectable({
  providedIn: 'root'
})
export class FacebookService {

  constructor() { 
    (window as any).fbAsyncInit = function() {
      FB.init({
        appId      : '1621008412083335', // Reemplaza con tu App ID
        cookie     : true,
        xfbml      : true,
        version    : 'v11.0'
      });
    };

    (function(d, s, id) {
      let js: HTMLScriptElement;
      const fjs = d.getElementsByTagName(s)[0] as HTMLScriptElement;
      if (d.getElementById(id)) { return; }
      js = d.createElement(s) as HTMLScriptElement; js.id = id;
      js.src = "https://connect.facebook.net/en_US/sdk.js";
      if (fjs && fjs.parentNode) {
        fjs.parentNode.insertBefore(js, fjs);
      }
    }(document, 'script', 'facebook-jssdk'));
  }

  login(): Promise<any> {
    return new Promise((resolve, reject) => {
      FB.login((response: any) => {
        if (response.authResponse) {
          FB.api('/me', { fields: 'name,email,picture' }, (userInfo: any) => {
            resolve(userInfo);
          });
        } else {
          reject('User cancelled login or did not fully authorize.');
        }
      }, { scope: 'public_profile,email' });
    });
  }

  logout(): Promise<any> {
    return new Promise((resolve, reject) => {
      FB.logout((response: any) => {
        if (response) {
          resolve('User logged out.');
        } else {
          reject('Error logging out.');
        }
      });
    });
  }
}
