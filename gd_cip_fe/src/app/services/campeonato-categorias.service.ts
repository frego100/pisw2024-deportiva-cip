import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export interface CampeonatoCategorias {
  id: number
  tituloCampeonato: string
  fechaInicio: string
  fechaFin: string
  organizadorNombre: string
  organizadorEmail: string
  organizadorNumero: string
  tituloHonorifico: string
  urlImagen: string
}

@Injectable({
  providedIn: 'root'
})
export class CampeonatoCategoriasService {
  private endpoint = `${this.gs.rootPath}/campeonatoCategorias`

  constructor(
    private gs: GlobalService,
    private http: HttpClient,
  ) { }

  findAll(): Observable<CampeonatoCategorias[]> {
    return this.http.get<CampeonatoCategorias[]>(this.endpoint)
  }

  save(o: any): Observable<CampeonatoCategorias> {
    return this.http.post<CampeonatoCategorias>(this.endpoint, o)
  }

  findById(id: number): Observable<CampeonatoCategorias> {
    return this.http.get<CampeonatoCategorias>(`${this.endpoint}/${id}`)
  }

  deleteById(id: number): Observable<void> {
    return this.http.delete<void>(`${this.endpoint}/${id}`)
  }
}
