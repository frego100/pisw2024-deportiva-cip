import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Observable } from 'rxjs';

export interface VoleyMatchDTO {
  id: number;
  fecha: string | null;
  equipo1Nombre?: string;
  equipo2Nombre?: string;
  puntaje11: number;
  puntaje12: number;
  puntaje21: number;
  puntaje22: number;
  puntaje31: number;
  puntaje32: number;
  puntaje41: number;
  puntaje42: number;
  puntaje51: number;
  puntaje52: number;
  campeonatoId: number;
  estado: string;
  equipo1Id: number;
  equipo2Id: number;
  numeroSets: number;
  fase: number; //GlobalService.Fase
  grupo: string;
}

export interface VoleyMatchDetail {
  puntos: any[]
  alineacion: any[]
  sustituciones: any[]
}

@Injectable({
  providedIn: 'root'
})
export class PartidosVolleyService {
  private apiUrl = `${this.gs.rootPath}`;
  private endpoint = "partidoVoley" // Endpoint de partidos de voley

  constructor(
    private http: HttpClient,
    private gs: GlobalService,
  ) { }

  findByCampeonatoId(id: number): Observable<VoleyMatchDTO[]> {
    return this.http.get<VoleyMatchDTO[]>(`${this.apiUrl}/${this.endpoint}/byCampeonato/${id}`)
  }

  findById(id: number): Observable<VoleyMatchDTO> {
    return this.http.get<VoleyMatchDTO>(`${this.apiUrl}/${this.endpoint}/${id}`)
  }

  deleteById(id: number) {
    return this.http.delete(`${this.apiUrl}/${this.endpoint}/${id}`)
  }

  save(o: VoleyMatchDTO): Observable<VoleyMatchDTO> {
    return this.http.post<VoleyMatchDTO>(`${this.apiUrl}/${this.endpoint}`, o)
  }

  savePartidoDetalle(o: VoleyMatchDetail): Observable<VoleyMatchDetail> {
    return this.http.post<VoleyMatchDetail>(`${this.apiUrl}/${this.endpoint}/detalle`, o)
  }

  findPartidoDetalle(partidoId: number, equipoId: number): Observable<VoleyMatchDetail> {
    return this.http.get<VoleyMatchDetail>(`${this.apiUrl}/${this.endpoint}/detalle?partidoId=${partidoId}&equipoId=${equipoId}`)
  }

  deletePartidoDetalle(o: VoleyMatchDetail): Observable<void> {
    return this.http.delete<void>(`${this.apiUrl}/${this.endpoint}/detalle`, {
      body: o
    })
  }
}
