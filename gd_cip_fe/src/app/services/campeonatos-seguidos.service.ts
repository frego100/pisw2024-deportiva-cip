import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export interface CampeonatosSeguidos {
  id?: number,
  campeonatoId: number,
  usuarioCipId: number,
}

@Injectable({
  providedIn: 'root'
})
export class CampeonatosSeguidosService {
  enpoint = `${this.gs.rootPath}/campeonatosSeguidos`

  constructor(
    private gs: GlobalService,
    private http: HttpClient,
  ) { }

  getCampeonatosSeguidos(): Observable<CampeonatosSeguidos[]> {
    let id = sessionStorage.getItem("usuarioCipId")
    return this.http.get<CampeonatosSeguidos[]>(`${this.enpoint}/${id}`);
  }

  findByUsuarioCip(id: number): Observable<CampeonatosSeguidos[]>{
    return this.http.get<CampeonatosSeguidos[]>(`${this.enpoint}/${id}`)
  }

  save(o: CampeonatosSeguidos): Observable<CampeonatosSeguidos>{
    return this.http.post<CampeonatosSeguidos>(`${this.enpoint}`, o)
  }

  deleteById(id: number | undefined): Observable<void>{
    return this.http.delete<void>(`${this.enpoint}/${id}`)
  }

  deleteAllById(id_: number[]): Observable<void>{
    return this.http.delete<void>(`${this.enpoint}`, {
      body: id_
    })
  }

  isCampeonatoFav(o: CampeonatosSeguidos): boolean {
    return true
  }
}
