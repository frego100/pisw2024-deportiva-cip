import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Observable } from 'rxjs';

export interface Campeonato {
  id: number;
  tituloCampeonato: string;
  tipoCampeonato: number;
  fechaInicio: string;
  fechaFin: string;
  campeonatoId: number;
  organizadorNombre: string;
  organizadorEmail: string;
  organizadorNumero: string;
  urlImagen: string;
}

@Injectable({
  providedIn: 'root'
})
export class ListaCampeonatosService {

  private campeonatoUrl = this.gs.rootPath + '/campeonato'; // Deberías obtener esta URL de tu servicio global o directamente en el constructor

  constructor(private http: HttpClient, private gs:GlobalService) {}

  getCampeonatos(): Observable<Campeonato[]> {
    return this.http.get<Campeonato[]>(this.campeonatoUrl);
  }

  getCampeonato(id: number): Observable<Campeonato> {
    return this.http.get<Campeonato>(`${this.campeonatoUrl}/${id}`);
  }

  addCampeonato(campeonato: Campeonato): Observable<Campeonato> {
    return this.http.post<Campeonato>(this.campeonatoUrl, campeonato);
  } 

  updatePlayer(campeonato: Campeonato): Observable<Campeonato> {
    return this.http.post<Campeonato>(`${this.campeonatoUrl}`, campeonato);
  }

  deleteCampeonato(idCamp: number): Observable<void> {
    return this.http.delete<void>(`${this.campeonatoUrl}/${idCamp}`);
  }

}
