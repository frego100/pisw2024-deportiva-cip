import { TestBed } from '@angular/core/testing';

import { EquipoVistaService } from './equipo-vista.service';

describe('EquipoVistaService', () => {
  let service: EquipoVistaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EquipoVistaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
