import { TestBed } from '@angular/core/testing';

import { TipoCampService } from './tipo-camp.service';

describe('TipoCampService', () => {
  let service: TipoCampService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TipoCampService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
