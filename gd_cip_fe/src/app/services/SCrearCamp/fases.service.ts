import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FasesService {

  private apiUrl = 'http://localhost:8080/fases'; // Cambia esta URL según sea necesario

  constructor(private http: HttpClient) { }

  getFases(): Observable<any[]> {
    return this.http.get<any[]>(this.apiUrl);
  }
}
