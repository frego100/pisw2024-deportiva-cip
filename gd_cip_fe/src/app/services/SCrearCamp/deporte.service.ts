import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeporteService {
  private apiUrl = 'http://localhost:8080/deporte';

  constructor(private http: HttpClient) { }

  //Servicio GET
  getDeportes(): Observable<any> {
    return this.http.get<any>(this.apiUrl);
  }

}
