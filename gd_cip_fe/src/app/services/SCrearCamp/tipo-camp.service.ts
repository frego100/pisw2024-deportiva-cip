import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TipoCampService {

  private apiUrl = 'http://localhost:8080/tipoCampeonato'; // Cambia esta URL según sea necesario

  constructor(private http: HttpClient) { }

  getTiposCampeonato(): Observable<any[]> {
    return this.http.get<any[]>(this.apiUrl);
  }
}
