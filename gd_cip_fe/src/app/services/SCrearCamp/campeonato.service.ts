import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CampeonatoService {

  private baseUrl = 'http://localhost:8080'; // Ajusta la URL base según tu configuración

  constructor(private http: HttpClient) { }

  createCampeonato(campeonato: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/campeonato`, campeonato);
  }
}
