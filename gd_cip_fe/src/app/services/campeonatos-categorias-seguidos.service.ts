import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface CampeonatosCategoriasSeguidos {
  id?: number,
  campeonatoCategoriasId: number,
  usuarioCipId: number,
}

@Injectable({
  providedIn: 'root'
})
export class CampeonatosCategoriasSeguidosService {

  enpoint = `${this.gs.rootPath}/campeonatosCategoriasSeguidos`

  constructor(
    private gs: GlobalService,
    private http: HttpClient,
  ) { }

  getCampeonatoCaegoriaSeguidos(): Observable<CampeonatosCategoriasSeguidos[]> {
    let id = sessionStorage.getItem("usuarioCipId")
    return this.http.get<CampeonatosCategoriasSeguidos[]>(`${this.enpoint}/${id}`);
  }

  findByUsuarioCip(id: number): Observable<CampeonatosCategoriasSeguidos[]>{
    return this.http.get<CampeonatosCategoriasSeguidos[]>(`${this.enpoint}/${id}`)
  }

  save(o: CampeonatosCategoriasSeguidos): Observable<CampeonatosCategoriasSeguidos>{
    return this.http.post<CampeonatosCategoriasSeguidos>(`${this.enpoint}`, o)
  }

  deleteById(id: number | undefined): Observable<void>{
    return this.http.delete<void>(`${this.enpoint}/${id}`)
  }

  deleteAllById(id_: number[]): Observable<void>{
    return this.http.delete<void>(`${this.enpoint}`, {
      body: id_
    })
  }
}
