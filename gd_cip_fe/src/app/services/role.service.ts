import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { Auth0Service } from '../auth0.service';

@Injectable({
  providedIn: 'root'
})
export class RoleService {
  private currentRoleSubject = new BehaviorSubject<string | null>(null);
  currentRole$: Observable<string | null> = this.currentRoleSubject.asObservable();

  constructor(
    private auth0Service: Auth0Service
  ) {
    this.initializeUserRole();
  }

  private initializeUserRole(): void {
    this.auth0Service.getCurrentUserRole().subscribe(
      role => {
        this.currentRoleSubject.next(role);
      },
      error => {
        console.error('Error inicializando rol:', error);
        this.currentRoleSubject.next(null);
      }
    );
  }

  getCurrentRole(): string | null {
    return this.currentRoleSubject.value;
  }

  hasRole(role: string): boolean {
    return this.getCurrentRole() === role;
  }

  // Método para manejar el rol de CIP
  setCIPRole(): void {
    this.currentRoleSubject.next('CIP');
  }
}
