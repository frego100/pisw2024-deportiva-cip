import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';

export interface Usuario {
  id?: number;
  email: string;
  contraseña: string;
  nombre: string;
  apellidos: string;
  fechaNacimiento: string;
  dni: string;
  rol: string;
}

@Injectable({
  providedIn: 'root'
})
export class UsuarioAdminService {

  private urlApi = `${this.gs.rootPath}/usuario`

  constructor(
    private gs: GlobalService,
    private http: HttpClient,
  ) { }

  loginAdmin(v: any){
    return this.http.post<Usuario>(`${this.urlApi}/login`, v)
  }

  //INSERT -> No especificar id crea una nueva entrada
  //UPDATE -> Especificar id actualiza la entrada
  insertAdmin(v: any){
    return this.http.post(`${this.urlApi}`, v)
  }

  getByEmail(v: any){
    return this.http.post(`${this.urlApi}/getEmail`, v)
  }

  getAll(){
    return this.http.get<Usuario[]>(`${this.urlApi}`)
  }

  deleteById(id: number){
    return this.http.delete(`${this.urlApi}/${id}`)
  }
}
