import { TestBed } from '@angular/core/testing';

import { CampeonatosCategoriasSeguidosService } from './campeonatos-categorias-seguidos.service';

describe('CampeonatosCategoriasSeguidosService', () => {
  let service: CampeonatosCategoriasSeguidosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampeonatosCategoriasSeguidosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
