import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Observable,BehaviorSubject } from 'rxjs';


interface ClasificacionDet {
  claDetDiferenciaPuntos: number;
  claDetId: number;
  claDetPuntos: number;
  claDetJuegos: number;
  claDetGanados: number;
  claDetEmpates: number;
  claDetPerdidos: number;
  claDetGolesFavor: number;
  claDetGolesContra: number;
  claDetDiferenciaGoles: number;
  claDetAprovechamiento: number;
  claDetPuntosExtra: number;
  claDetEquipoId: number;
  claDetCabeceraId:number;
}

interface ClasificacionDetalle {
  posicion: number;
  clasificacionDet: ClasificacionDet;
}

export interface ClasificacionData {
  cabecera: {
    claCabId: number;
    claCabGrupoId: number;
  };
  detalle: ClasificacionDetalle[];
}


@Injectable({
  providedIn: 'root'
})
export class ClasificacionService {

  private apiUrl = `${this.gs.rootPath}/clasificacion`; // Ruta api
  private eqUrl = `${this.gs.rootPath}/equipo`;

  private selectedTeamSource = new BehaviorSubject<any>(null);
  selectedTeam$ = this.selectedTeamSource.asObservable();
  
  constructor(
    private http: HttpClient,
    private gs: GlobalService
  ) { 
    
  }

  getClasificacionData(campeonatoId:number): Observable<ClasificacionData> {
    const url = `${this.apiUrl}/${campeonatoId}`
    return this.http.get<ClasificacionData>(url);
  }
  
  

  getAllEquipos(): Observable<any[]> {
    return this.http.get<any[]>(this.eqUrl);
  }


  setSelectedTeam(team: any): void {
    this.selectedTeamSource.next(team);
  }

  getSelectedTeam(): any {
    return this.selectedTeamSource.getValue();
  }
}
