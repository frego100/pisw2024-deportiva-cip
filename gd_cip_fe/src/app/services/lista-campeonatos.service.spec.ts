import { TestBed } from '@angular/core/testing';

import { ListaCampeonatosService } from './lista-campeonatos.service';

describe('ListaCampeonatosService', () => {
  let service: ListaCampeonatosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListaCampeonatosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
