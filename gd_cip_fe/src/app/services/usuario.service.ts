import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { AuthService } from '@auth0/auth0-angular';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

export interface UsuarioCIP{
  id: number,
  cip: string,
  nombre: string,
  capitulo?: string,
  dni?: string,
}

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private usuarioEp = `${this.gs.rootPath}/usuario`;
  private usuarioCipEp = `${this.gs.rootPath}/usuarioCIP`;

  constructor(
    private gs: GlobalService,
    private http: HttpClient,
    private auth: AuthService
  ) { }

  findByCip(cip: any): Observable<UsuarioCIP> {
    return this.http.get<UsuarioCIP>(`${this.usuarioCipEp}/cip/${cip}`);
  }

  findById(id: number): Observable<UsuarioCIP> {
    return this.http.get<UsuarioCIP>(`${this.usuarioCipEp}/${id}`);
  }

  save(o: UsuarioCIP): Observable<UsuarioCIP> {
    return this.http.post<UsuarioCIP>(`${this.usuarioCipEp}`, o);
  }

  findAll(): Observable<UsuarioCIP[]> {
    return this.http.get<UsuarioCIP[]>(`${this.usuarioCipEp}`);
  }

  deleteById(id: number): Observable<void> {
    return this.http.delete<void>(`${this.usuarioCipEp}/${id}`);
  }

  // Método para verificar si el usuario está autenticado por CIP o Auth0
  isAuthenticated(): Observable<boolean> {
    return this.auth.isAuthenticated$.pipe(
      map(isAuth0Authenticated => isAuth0Authenticated),
      catchError(() => of(false))
    );
  }
}
