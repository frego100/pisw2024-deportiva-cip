import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Observable } from 'rxjs';

export interface Equipo {
  id: number,
  email: string,
  nombre: string,
  entrenador: string,
  grupoId: number,
  campeonatoId: number,
  delegadoNombre1: string,
  delegadoTelefono1: string,
  delegadoNombre2: string,
  delegadoTelefono2: string,
  delegadoNombre3: string,
  delegadoTelefono3: string,
  delegadoNombre4: string,
  delegadoTelefono4: string,
  delegadoNombre5: string,
  delegadoTelefono5: string,
}

@Injectable({
  providedIn: 'root'
})
export class EquipoService {

  private equipoUrl = this.gs.rootPath + '/equipo'; // Deberías obtener esta URL de tu servicio global o directamente en el constructor

  constructor(private http: HttpClient, private gs: GlobalService) { }

  getEquipos(): Observable<Equipo[]> {
    return this.http.get<Equipo[]>(this.equipoUrl);
  }

  getEquipo(id: number): Observable<Equipo> {
    return this.http.get<Equipo>(`${this.equipoUrl}/${id}`);
  }

  addEquipo(equipo: Equipo): Observable<Equipo> {
    return this.http.post<Equipo>(this.equipoUrl, equipo);
  }

  updateEquipo(equipo: Equipo): Observable<Equipo> {
    return this.http.post<Equipo>(`${this.equipoUrl}`, equipo);
  }

  deleteEquipo(equipoId: number): Observable<void> {
    return this.http.delete<void>(`${this.equipoUrl}/${equipoId}`);
  }
}