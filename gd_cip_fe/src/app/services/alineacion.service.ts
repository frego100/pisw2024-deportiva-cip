import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';

@Injectable({
  providedIn: 'root'
})
export class AlineacionService {

  endpoint = "alineacion"

  constructor(
    private gs: GlobalService,
    private http: HttpClient,
  ) { }

  deleteById(idx: number) {
    this.http.delete(`${this.gs.rootPath}/${this.endpoint}/${idx}`)
  }
}
