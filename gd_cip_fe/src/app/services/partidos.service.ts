import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalService } from './global.service';

export interface PartidoResponseDTO extends PartidoDTO {
  equipo1: {
    id: number,
    email: string,
    nombre: string,
    entrenador: string,
    grupoId: number,
    campeonatoId: number
  };
  equipo2: {
    id: number,
    email: string,
    nombre: string,
    entrenador: string,
    grupoId: number,
    campeonatoId: number
  };
};

export interface PartidoDetalleRequestDTO {
  goles: any[];
  tarjetasAmarillas: any[];
  tarjetasRojas: any[];
  tarjetasAzules: any[];
  faltas: any[];
  alineacion: any[];
  sustituciones: any[];
  golesContra: any[];
  jugadasPartido: any[];
};

export interface PartidoDTO {
  id: number,
  campeonatoId: number,
  equipo1Id: number,
  equipo2Id: number,
  fecha: string | null,
  tipoResultado: string,
  marcadorEquipo1: number,
  marcadorEquipo2: number,
  mejorJugadorId: number | null,
  mejorPorteroId: number | null,
  fase: number, //GlobalService.Fases
  grupo: string,
}

@Injectable({
  providedIn: 'root'
})
export class PartidosService {
  private apiUrl = `${this.gs.rootPath}`; // Cambia esta URL según sea necesario
  private endpoint = "partido/detalle"

  constructor(
    private http: HttpClient,
    private gs: GlobalService,
  ) { }

  getPartidosByCampeonato(campeonatoId: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiUrl}/partido/byCampeonatoId/${campeonatoId}`);
  }

  getPartidoById(partidoId: number): Observable<any> {
    return this.http.get<any[]>(`${this.apiUrl}/partido/${partidoId}`);
  }

  addPartido(partido: any): Observable<any> {
    return this.http.post<any>(`${this.apiUrl}/partido?faseId=1`, partido);
  }

  getPartidoEquipo(partidoId: number, equipoId: number): Observable<any> {
    const url = `${this.apiUrl}/partido/detalle?partidoId=${partidoId}&equipoId=${equipoId}`;

    return this.http.get(url);
  }

  updatePartido(id: number, partido: any): Observable<any> {
    return this.http.post(`${this.apiUrl}/partido/detalle?partidoId=${id}`, partido);
  }

  savePartido(id: number, partido: PartidoDTO):Observable<PartidoDTO>{
    return this.http.post<PartidoDTO>(`${this.apiUrl}/partido`, partido)
  }

  savePartidoDet(partidoDetalle: any) {
    return this.http.post(`${this.apiUrl}/partido/detalle`, partidoDetalle)
  }

  deletePartido(id: number) {
    return this.http.delete(`${this.apiUrl}/partido/${id}`)
  }

  deletePartidoDet(eliminarDetallePartido: any) {
    return this.http.delete(`${this.gs.rootPath}/${this.endpoint}`, {
      body: eliminarDetallePartido
    })
  }
}
