import { TestBed } from '@angular/core/testing';

import { CampeonatosSeguidosService } from './campeonatos-seguidos.service';

describe('CampeonatosSeguidosService', () => {
  let service: CampeonatosSeguidosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampeonatosSeguidosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
