import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalService } from './global.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FileUploadServiceService {

  private url = `${this.gs.rootPath}/upload`

  constructor(
    private http: HttpClient,
    private gs: GlobalService,
  ) { }

  uploadFile(file: File): Observable<any> {
    const formData: FormData = new FormData()
    formData.append('file', file, file.name)

    return this.http.post(this.url, formData)
  }
}
