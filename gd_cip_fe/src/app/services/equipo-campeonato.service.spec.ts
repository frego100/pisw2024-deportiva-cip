import { TestBed } from '@angular/core/testing';

import { EquipoCampeonatoService } from './equipo-campeonato.service';

describe('EquipoCampeonatoService', () => {
  let service: EquipoCampeonatoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EquipoCampeonatoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
