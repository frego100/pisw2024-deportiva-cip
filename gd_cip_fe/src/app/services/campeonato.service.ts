import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Observable } from 'rxjs';
import { Equipo } from './equipo.service';
import { PartidoDTO } from './partidos.service';
import { EquipoCampeonato } from './equipo-campeonato.service';
import { Player } from './player.service';

export interface Campeonato {
  id: number
  tituloCampeonato: string
  tipoCampeonato?: number
  fechaInicio: string
  fechaFin: string
  campeonatoId?: number
  organizadorNombre: string
  organizadorEmail: string
  organizadorNumero: string
  tituloHonorifico: string
  urlImagen: string
  campeonatoCategoriasId?: string
}

@Injectable({
  providedIn: 'root'
})

export class CampeonatoService {
  private campeonatoUrl = this.gs.rootPath + '/campeonato'; // Deberías obtener esta URL de tu servicio global o directamente en el constructor
  private campeonatoFavUrl = this.gs.rootPath + '/campeonatoFavs';
  private CampeonatosFavoritosList: Campeonato [] = []

  constructor(
    private http: HttpClient,
    private gs: GlobalService,
  ) { }

  getCampeonatos(): Observable<Campeonato[]> {
    return this.http.get<Campeonato[]>(this.campeonatoUrl);
  }

  getCampeonato(id: number): Observable<Campeonato> {
    return this.http.get<Campeonato>(`${this.campeonatoUrl}/${id}`);
  }

  getByCampeonatoCategoriasId(id: number): Observable<Campeonato[]> {
    return this.http.get<Campeonato[]>(`${this.campeonatoUrl}/byCampeonatoCategoriasId/${id}`)
  }

  addCampeonato(campeonato: Campeonato): Observable<Campeonato> {
    return this.http.post<Campeonato>(this.campeonatoUrl, campeonato);
  }

  updateCampeonato(campeonato: Campeonato): Observable<Campeonato> {
    return this.http.post<Campeonato>(`${this.campeonatoUrl}`, campeonato);
  }

  deleteCampeonatoById(campeonatoId: number): Observable<void> {
    return this.http.delete<void>(`${this.campeonatoUrl}/${campeonatoId}`);
  }

  deleteCampeonato(campeonato: Campeonato): Observable<void> {
    return this.http.delete<void>(`${this.campeonatoUrl}`, {
      body: campeonato
    })
  }

  getEquiposEnCampeonato(equipos: Equipo[], equiposCampeonato: EquipoCampeonato[]): Equipo[]{
    //console.log("equipoCampeonato", equipoCampeonatoData)
    //console.log("equiposData", equiposData)
    let equiposRes: any[] = []
    equiposCampeonato.forEach(element => {
      equiposRes.push(equipos.filter(equipo => equipo.id == element.equipoId)[0])
    })

    return equiposRes
  }

  getJugadoresEnCampeonato(equiposEnCampeonato: Equipo[], jugadores: Player[]): Map<number, Player[]>{
    const res = new Map<number, Player[]>()

    equiposEnCampeonato.forEach(element => {
      res.set(element.id, [])
    });

    jugadores.forEach(element => {
      res.get(element.equipoID)?.push(element)
    })

    return res
  }

  isCampeonatoFav(campeonato: Campeonato): boolean {
    return this.CampeonatosFavoritosList.some(cmpfav => cmpfav.id === campeonato.id)
  }

  deleteFromCampFavs(campeonato: Campeonato): void {
    const index = this.CampeonatosFavoritosList.findIndex(cmpfav => cmpfav.id === campeonato.id)
    if(index !== -1){
      this.CampeonatosFavoritosList.splice(index, 1)
      console.log('campeonato ELIMINADO de favs ', campeonato.tituloCampeonato)
      console.log(this.CampeonatosFavoritosList)
    }
  }
  
  addToCampFavs(campeonato: Campeonato): void {
    this.CampeonatosFavoritosList.push(campeonato)
    console.log('campeonato AÑADIDO a favs ', campeonato.tituloCampeonato)
    console.log(this.CampeonatosFavoritosList)
  }

  // addToCampFavs2(o: Campeonato): Observable<Campeonato>{
  //   return this.http.post<Campeonato>(`${this.campeonatoFavUrl}`, o)
  //   console.log("añadido")
  // }

  // deleteFromCampFavs2(id: number): Observable<void>{
  //   const index = this.CampeonatosFavoritosList.findIndex(campeonato => campeonato.id === campeonato.id)
  //   if(index !== -1){
  //     return this.http.delete<void>(`${this.CampeonatosFavoritosList}/${id}`)
  //   }
  // }



}
