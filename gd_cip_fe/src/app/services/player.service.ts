import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from './global.service';
import { Observable } from 'rxjs';

export interface Player {
  id?: number;
  email: string;
  name: string;
  shortName: string;// BORRAR
  codCIP: number;
  position: string;
  number: string; //BORRAR
  codFam: string;
  document: string;
  birthDate: Date;
  phone: string;
  equipoID: number;
  capitulo?: string;
}

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private urlApi = `${this.gs.rootPath}/jugador`;

  constructor(
    private gs: GlobalService,
    private http: HttpClient
  ) { }

  getPlayers(): Observable<Player[]> {
    return this.http.get<Player[]>(this.urlApi);
  }

  getPlayer(id: number): Observable<Player> {
    return this.http.get<Player>(`${this.urlApi}/${id}`);
  }

  addPlayer(player: Player): Observable<Player> {
    return this.http.post<Player>(this.urlApi, player);
  }

  updatePlayer(player: Player): Observable<Player> {
    return this.http.post<Player>(`${this.urlApi}`, player);
  }

  deletePlayer(playerId: number): Observable<void> {
    return this.http.delete<void>(`${this.urlApi}/${playerId}`);
  }
}
