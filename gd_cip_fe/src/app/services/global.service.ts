import { EventEmitter, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { PartidoDTO } from './partidos.service';
import { Equipo } from './equipo.service';
import { Player } from './player.service';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  public rootPath = "https://springboot-2-0.onrender.com"
  //public rootPath = "http://localhost:8080"
  public adminView = false
  public nombreUsuario = "Usuario"
  public partidoId = 0
  public isCIPAuthenticated = false;
  public partidoFutbolDTO: any = {}

  public alineacion1: any[] = []
  public alineacion2: any[] = []

  private jugadoresSubject = new BehaviorSubject<any>([]);
  jugadores$ = this.jugadoresSubject.asObservable();

  private equipo1IdSubject = new BehaviorSubject<number>(0)
  equipo1Id$ = this.equipo1IdSubject.asObservable()

  private equipo2IdSubject = new BehaviorSubject<number>(0)
  equipo2Id$ = this.equipo2IdSubject.asObservable()

  private updatePartidoSubject = new Subject<void>()
  updatePartido$ = this.updatePartidoSubject.asObservable()

  private campeonatoSource = new BehaviorSubject<number>(-1);
  campeonatoSelected$ = this.campeonatoSource.asObservable();

  private partidosSubject = new BehaviorSubject<any>(null)
  partidosSubject$ = this.partidosSubject.asObservable()

  private partidoVoleyIdSubject = new BehaviorSubject<number>(0)
  partidoVoleyId$ = this.partidoVoleyIdSubject.asObservable()

  private equiposEnCampeonatoSubject = new BehaviorSubject<Equipo[]>([])
  equiposEnCampeonato$ = this.equiposEnCampeonatoSubject.asObservable()

  //Service de equipos clasificados
  private equiposClasificadosSubject = new BehaviorSubject<any[]>([]);
  equiposClasificados$ = this.equiposClasificadosSubject.asObservable();
  private equiposClasificadosMap = new Map<number, BehaviorSubject<any[]>>();

  //Evento que notifica la finalizacion de la fase en tablaPartidos
  private faseFinalizadaSource = new BehaviorSubject<Map<number, boolean>>(new Map());
  faseFinalizada$ = this.faseFinalizadaSource.asObservable();

  //Para pasarme el listado de partidos geenrados al crear eliminatorias
  private eliminatoriaDatesSubject = new BehaviorSubject<any[]>([]);
  eliminatoriaDates$ = this.eliminatoriaDatesSubject.asObservable();

  //<equipoId => listaJugadores>
  jugadoresEnCampeonato = new Map<number, Player[]>()

  private jugadoresEnAlineacion1Subject = new BehaviorSubject<Player[]>([])
  jugadoresEnAlineacion1$ = this.jugadoresEnAlineacion1Subject.asObservable()

  private jugadoresEnAlineacion2Subject = new BehaviorSubject<Player[]>([])
  jugadoresEnAlineacion2$ = this.jugadoresEnAlineacion2Subject.asObservable()

  private setSeleccionadoSubject = new BehaviorSubject<number>(1) //sets[1 -1] -> primer set
  setSeleccionado$ = this.setSeleccionadoSubject.asObservable()

  public partidoFutbolEstado = {
    REALIZADO: "REALIZADO",
    NO_REALIZADO: "PREPARACION",
    EN_VIVO: "EN VIVO"
  }

  public Deporte = {
    FUTBOL: 1,
    VOLEY: 2,
    BALONCESTO: 3
  }

  public tipoEditableBox = {
    ALINEACION: 1,
    GENERAL: 2,
    PUNTOS_VOLEY: 3,
    SUSTITUCION: 4,
    CARD_BOX: 5,
  }

  public Fase = {
    GRUPOS: 1,
    FINAL: 2,
    SEMIFINAL: 3,
    CUARTOS: 4,
    OCTAVOS: 5,
  }

  constructor() { }

  setCampeonatoSelected(idCampeonato: number): void {
    this.campeonatoSource.next(idCampeonato);
  }

  updateJugadores(newValue: any) {
    this.jugadoresSubject.next(newValue);
  }

  getJugadores() {
    return this.jugadoresSubject.getValue()
  }

  emitUpdatePartidos() {
    this.updatePartidoSubject.next()
  }

  updateEquipo1Id(value: number) {
    this.partidoFutbolDTO.equipo1Id = value
    this.equipo1IdSubject.next(value)
  }

  updateEquipo2Id(value: number) {
    this.partidoFutbolDTO.equipo2Id = value
    this.equipo2IdSubject.next(value)
  }

  // Método para agregar o actualizar equipos
  updateEquipoCampeonato(equipoCampeonato_: Equipo[]): void {
    this.equiposEnCampeonatoSubject.next(equipoCampeonato_)
  }


  updatePartidos(partidos: any) {
    this.partidosSubject.next(partidos)
  }

  updatePartidoVoleyId(id: number) {
    this.partidoVoleyIdSubject.next(id)
  }

  updateJugadoresEnAlineacion1(jugadores: Player[]) {
    this.jugadoresEnAlineacion1Subject.next(jugadores)
  }

  updateJugadoresEnAlineacion2(jugadores: Player[]) {
    this.jugadoresEnAlineacion2Subject.next(jugadores)
  }
  
  getJugadoresEnAlineacion1(){
    return this.jugadoresEnAlineacion1Subject.getValue()
  }

  getJugadoresEnAlineacion2(){
    return this.jugadoresEnAlineacion2Subject.getValue()
  }

  updateSetSeleccionado(set: number) {
    this.setSeleccionadoSubject.next(set)
  }

  //actualizar equipos clasificados
  //Obtener el observable de equipos clasificados para un campeonato
  getEquiposClasificados$(campeonatoId: number): Observable<any[]> {
    if (!this.equiposClasificadosMap.has(campeonatoId)) {
      this.equiposClasificadosMap.set(campeonatoId, new BehaviorSubject<any[]>([]));
    }
    return this.equiposClasificadosMap.get(campeonatoId)!.asObservable();
  }

  // Método para actualizar los equipos clasificados para un campeonato
  updateEquiposClasificados(campeonatoId: number, equipos: any[]): void {
    if (!this.equiposClasificadosMap.has(campeonatoId)) {
      this.equiposClasificadosMap.set(campeonatoId, new BehaviorSubject<any[]>([]));
    }
    this.equiposClasificadosMap.get(campeonatoId)!.next(equipos);
  }

  // Método para obtener los equipos clasificados actuales para un campeonato
  getEquiposClasificados(campeonatoId: number): any[] {
    return this.equiposClasificadosMap.get(campeonatoId)?.value || [];
  }

  //Finalizacion de la fase
  finalizarFase(campeonatoId: number): void {
    const currentMap = this.faseFinalizadaSource.getValue();
    currentMap.set(campeonatoId, true); // Marca la fase como finalizada para este campeonato
    this.faseFinalizadaSource.next(currentMap);
  }

  isFaseFinalizada(campeonatoId: number): boolean {
    return this.faseFinalizadaSource.value.get(campeonatoId) ?? false;
  }

  //Partidos generados en eliminatorias
  updateEliminatoriaDates(dates: any[]) {
    this.eliminatoriaDatesSubject.next(dates);
  }

  logout(): void {
    sessionStorage.removeItem("usuarioCipId")
    this.isCIPAuthenticated = false;
    this.nombreUsuario = '';
    this.adminView = false;
  }
}
