import { TestBed } from '@angular/core/testing';

import { PartidosVolleyService } from './partidos-volley.service';

describe('PartidosVolleyService', () => {
  let service: PartidosVolleyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PartidosVolleyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
