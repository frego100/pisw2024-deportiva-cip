import { TestBed } from '@angular/core/testing';

import { CampeonatoCategoriasService } from './campeonato-categorias.service';

describe('CampeonatoCategoriasService', () => {
  let service: CampeonatoCategoriasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CampeonatoCategoriasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
