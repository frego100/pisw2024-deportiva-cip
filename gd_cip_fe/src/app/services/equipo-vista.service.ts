import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { GlobalService } from './global.service';

interface ClasificacionDet {
  claDetId: number;
  claDetPuntos: number;
  claDetJuegos: number;
  claDetGanados: number;
  claDetEmpates: number;
  claDetPerdidos: number;
  claDetGolesFavor: number;
  claDetGolesContra: number;
  claDetDiferenciaGoles: number;
  claDetAprovechamiento: number;
  claDetPuntosExtra: number;
  claDetEquipoId: number;
}

interface ClasificacionDetalle {
  posicion: number;
  clasificacionDet: ClasificacionDet;
}

interface ClasificacionData {
  cabecera: {
    claCabId: number;
    claCabGrupoId: number;
  };
  detalle: ClasificacionDetalle[];
}

export interface Gol {
  puntos: number;
  id: number;
  jugadorId: number;
  asistenciaId: number;
  descripcion: string;
  tiempo: number;
  minuto: number;
  segundo: number;
  enContra: number;
  partidoId: number;
  equipoId: number;
}

export interface Tarjeta {
  id: number;
  jugadorId: number;
  tiempo: number;
  minuto: number;
  segundo: number;
  descripcion: string;
  color: number;
  partidoId: number;
  equipoId: number;
}

export interface Falta {
  id: number;
  jugadorId: number;
  tiempo: number;
  minuto: number;
  segundo: number;
  partidoId: number;
  equipoId: number;
}

export interface Alineacion {
  id: number;
  jugadorId: number;
  numero: number;
  partidoId: number;
  equipoId: number;
}

export interface Sustitucion {
  id: number;
  jugadorEntraId: number;
  jugadorSaleId: number;
  descripcion: string;
  tiempo: number;
  minuto: number;
  segundo: number;
  partidoId: number;
  equipoId: number;
}

export interface JugadaPartido {
  id: number;
  jugadorId: number;
  descripcion: string;
  tiempo: number;
  minuto: number;
  segundo: number;
  partidoId: number;
  equipoId: number;
}

export interface DatosEquipo {
  goles: Gol[];
  jugadasPartido: JugadaPartido[];
  tarjetasAmarillas: Tarjeta[];
  tarjetasRojas: Tarjeta[];
  tarjetasAzules: Tarjeta[];
  faltas: Falta[];
  alineacion: Alineacion[];
  sustituciones: Sustitucion[];
  golesContra: Gol[];
  portero: Alineacion[];
}

export interface Jugador {
  id: number;
  email: string;
  name: string;
  shortName: string;
  position: string;
  document: string;
  phone: string;
  birthDate: string;
  number: string;
  equipoID: number;
}


export interface Partido {
  goles: any;
  jugadasPartido: any;
  tarjetasAmarillas: any;
  tarjetasRojas: any;
  tarjetasAzules: any;
  faltas: any;
  faseId: number;
  equipo1: Equipo;
  equipo2: Equipo;
  fecha: string;
  tipoResultado: 'FINALIZADO' | 'PENDIENTE';
  id: number;
}

export interface Equipo {
  id: number;
  email: string;
  nombre: string;
  entrenador: string;
  grupoId: number;
  campeonatoId: number;
}

@Injectable({
  providedIn: 'root'
})
export class EquipoVistaService {

  private partUrl = `${this.gs.rootPath}/partido/detalle`;

  private apiUrl = `${this.gs.rootPath}`;


  constructor(
    private http: HttpClient,  
    private gs: GlobalService,
  ) { }

  getPartidoData(partidoId: any, equipoId: number): Observable<DatosEquipo> {
    const url = `${this.partUrl}?partidoId=${partidoId}&equipoId=${equipoId}`;
    return this.http.get<DatosEquipo>(url);
  }


  getAllPartidosByCampeonato(campeonatoId:number):Observable<any[]>{
    return this.http.get<any[]>(`${this.apiUrl}/partido/byCampeonatoId/${campeonatoId}`);
  }

}
