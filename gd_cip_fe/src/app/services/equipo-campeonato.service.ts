import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalService } from './global.service';

export interface EquipoCampeonato {
  id: number
  equipoId: number
  campeonatoId: number
  grupoId: number | null
}

@Injectable({
  providedIn: 'root'
})
export class EquipoCampeonatoService {
  private endpoint = "equipoCampeonato"
  private url = `${this.gs.rootPath}/${this.endpoint}`

  constructor(
    private http: HttpClient,
    private gs: GlobalService,
  ) { }

  saveAll(o_: EquipoCampeonato[]): Observable<EquipoCampeonato[]>{
    return this.http.post<EquipoCampeonato[]>(`${this.url}/all`, o_)
  }

  deleteAll(o_: EquipoCampeonato[]): Observable<EquipoCampeonato[]>{
    return this.http.delete<EquipoCampeonato[]>(`${this.url}/all`, {
      body: o_
    })
  }

  delete(o: EquipoCampeonato): Observable<EquipoCampeonato>{
    return this.http.delete<EquipoCampeonato>(`${this.url}`, {
      body: o
    })
  }

  updateEquipoCampeonato(o_: EquipoCampeonato): Observable<EquipoCampeonato> {
    return this.http.post<EquipoCampeonato>(`${this.url}`, o_);
  }

  

  findByCampeonatoId(id: number): Observable<EquipoCampeonato[]>{
    return this.http.get<EquipoCampeonato[]>(`${this.url}/${id}`)
  }
}
