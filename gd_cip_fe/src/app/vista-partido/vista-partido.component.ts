import { Component, Input, OnInit } from '@angular/core';
import { EquipoVistaService, DatosEquipo, Partido, Jugador, Gol } from '../services/equipo-vista.service';
import { PartidosService } from "../services/partidos.service";
import { PlayerService, Player } from "../services/player.service";

// Definición de la interfaz PlayerData
interface PlayerData {
  numero: string;
  nombre: string;
  goles: number;
  golesContra: number;
  faltas: number;
  tarj_red: number;
  tarj_yellow: number;
  tarj_blue: number;
  cambios: 'entrada' | 'salida' | null;
}

@Component({
  selector: 'app-vista-partido',
  templateUrl: './vista-partido.component.html',
  styleUrls: ['./vista-partido.component.css'],
})

export class VistaPartidoComponent implements OnInit{

  // Propiedades para almacenar datos de jugadores
  playersEquip1: PlayerData[] = [];
  playersEquip2: PlayerData[] = [];

  numPartido: number = 0; //titulo, se saca del ID partido
  nameTeam1: string = '';
  nameTeam2: string = '';
  estadoPartido: string = '';

  partido: any;
  equipo1: any = [];
  equipo2: any = [];
  
  golesE1: Gol[] = []; 
  jugadoresE1: { [key: number]: string } = {};
  golesContraE1: Gol[] = [];
  faltasE1: any[] = [];
  tarjetasAmarillasE1: any[] = [];
  tarjetasRojasE1: any[] = [];
  tarjetasAzulesE1: any[] = [];
  sustitucionesEntraE1: any[] = [];
  sustitucionesSaleE1: any[] = [];
  
  golesE2: Gol[] = [];
  jugadoresE2: { [key: number]: string } = {};
  golesContraE2: Gol[] = [];
  faltasE2: any[] = [];
  tarjetasAmarillasE2: any[] = [];
  tarjetasRojasE2: any[] = [];
  tarjetasAzulesE2: any[] = [];
  sustitucionesEntraE2: any[] = [];
  sustitucionesSaleE2: any[] = [];
  
  constructor(
    private partidosService: PartidosService,
    private equipoVistaService: EquipoVistaService,
    private playerService: PlayerService
  ) {}

  ngOnInit(): void {
    /*
    const partidoId = 1;  // Cambiar de donde se obtenga !!!!!!
    const equipo1Id = 1;
    const equipo2Id = 2;

    this.partidosService.getPartidoById(partidoId).subscribe(data => {
      this.partido = data;
      this.loadTeamsDetails(partidoId, equipo1Id, equipo2Id);
    });

    // Obtener datos del equipo 1
    this.equipoVistaService.getPartidoData(partidoId, equipo1Id).subscribe(data => {
      if (data) {
        //console.log('Datos equipo 1:', data);
        this.processPlayerData(data, this.playersEquip1);
      }
    });
    
    // Obtener datos del equipo 2
    this.equipoVistaService.getPartidoData(partidoId, equipo2Id).subscribe(data => {
      if (data) {
        //console.log('Datos equipo 2:', data);
        this.processPlayerData(data, this.playersEquip2);
      }
    });
    */
  }

  loadTeamsDetails(partidoId: number, equipo1Id: number, equipo2Id: number): void {
    this.partidosService.getPartidoById(partidoId).subscribe((data: any) => {
      if (data && data.length > 0) {
        const partido = data[partidoId];
        this.numPartido = partido.id;
        this.nameTeam1 = partido.equipo1.nombre; 
        this.nameTeam2 = partido.equipo2.nombre;
        this.estadoPartido = partido.tipoResultado;
      }

      // Obtener datos del equipo 1
      this.equipoVistaService.getPartidoData(partidoId, equipo1Id).subscribe(data => {
        this.golesE1 = data.goles;  // Almacena los goles obtenidos del equipo 1
        this.golesContraE1 = data.golesContra;  // Almacena los goles en contra del equipo 1
        this.faltasE1 = data.faltas;  // Almacena las faltas del equipo 1
        this.tarjetasAmarillasE1 = data.tarjetasAmarillas;  // Almacena las tarjetas amarillas del equipo 1
        this.tarjetasRojasE1 = data.tarjetasRojas;
        this.tarjetasAzulesE1 = data.tarjetasAzules;

        //console.log('Goles Equipo 1:', this.golesE1);  // Verificar los datos de goles

        // Obtener detalles de jugadores involucrados en goles
        this.golesE1.forEach(gol => {
          this.playerService.getPlayer(gol.jugadorId).subscribe(player => {
            this.jugadoresE1[gol.jugadorId] = player.name;
            //console.log('Jugador Equipo 1:', player);  // Verificar los datos del jugador
          });
        });
        
        // Obtener detalles de jugadores involucrados en goles en contra
        this.golesContraE1.forEach(gol => {
          this.playerService.getPlayer(gol.jugadorId).subscribe(player => {
            this.jugadoresE1[gol.jugadorId] = player.name;
          });
        });

        // Obtener detalles de jugadores involucrados en faltas
        this.faltasE1.forEach(falta => {
          this.playerService.getPlayer(falta.jugadorId).subscribe(player => {
            this.jugadoresE1[falta.jugadorId] = player.name;
          });
        });

        // Obtener detalles de jugadores que recibieron tarjetas amarillas
        this.tarjetasAmarillasE1.forEach(tarjeta => {
          this.playerService.getPlayer(tarjeta.jugadorId).subscribe(player => {
            this.jugadoresE1[tarjeta.jugadorId] = player.name;
          });
        });

        // Obtener detalles de jugadores involucrados en las tarjetas rojas
        this.tarjetasRojasE1.forEach(tarjeta => {
          this.playerService.getPlayer(tarjeta.jugadorId).subscribe(player => {
            this.jugadoresE1[tarjeta.jugadorId] = player.name;
          });
        });

        // Obtener detalles de jugadores involucrados en las tarjetas azules
        this.tarjetasAzulesE1.forEach(tarjeta => {
          this.playerService.getPlayer(tarjeta.jugadorId).subscribe(player => {
            this.jugadoresE1[tarjeta.jugadorId] = player.name;
          });
        });

        this.sustitucionesEntraE1 = data.sustituciones.map(sustitucion => ({
          jugadorEntraId: sustitucion.jugadorEntraId,
          descripcion: sustitucion.descripcion,
          tiempo: sustitucion.tiempo,
          minuto: sustitucion.minuto,
          segundo: sustitucion.segundo
        }));
      
        this.sustitucionesSaleE1 = data.sustituciones.map(sustitucion => ({
          jugadorSaleId: sustitucion.jugadorSaleId,
          descripcion: sustitucion.descripcion,
          tiempo: sustitucion.tiempo,
          minuto: sustitucion.minuto,
          segundo: sustitucion.segundo
        }));
      
        // Obtener detalles de los jugadores que entran
        this.sustitucionesEntraE1.forEach(sustitucion => {
          this.playerService.getPlayer(sustitucion.jugadorEntraId).subscribe(player => {
            this.jugadoresE1[sustitucion.jugadorEntraId] = player.name;
          });
        });
      
        // Obtener detalles de los jugadores que salen
        this.sustitucionesSaleE1.forEach(sustitucion => {
          this.playerService.getPlayer(sustitucion.jugadorSaleId).subscribe(player => {
            this.jugadoresE1[sustitucion.jugadorSaleId] = player.name;
          });
        });
      });

      // Obtener datos del equipo 2
      this.equipoVistaService.getPartidoData(partidoId, equipo2Id).subscribe(data => {
        this.golesE2 = data.goles;  // Almacena los goles obtenidos del equipo 2
        this.golesContraE2 = data.golesContra;  // Almacena los goles en contra del equipo 2
        this.faltasE2 = data.faltas;  // Almacena las faltas del equipo 2
        this.tarjetasAmarillasE2 = data.tarjetasAmarillas;
        this.tarjetasRojasE2 = data.tarjetasRojas;
        this.tarjetasAzulesE2 = data.tarjetasAzules;
        
        //console.log('Goles Equipo 2:', this.golesE2);  // Verificar los datos de goles
        
        // Obtener detalles de jugadores involucrados en goles
        this.golesE2.forEach(gol => {
          this.playerService.getPlayer(gol.jugadorId).subscribe(player => {
            this.jugadoresE2[gol.jugadorId] = player.name;
            //console.log('Jugador Equipo 2:', player);  // Verificar los datos del jugador
          });
        });

        // Obtener detalles de jugadores involucrados en goles en contra
        this.golesContraE2.forEach(gol => {
          this.playerService.getPlayer(gol.jugadorId).subscribe(player => {
            this.jugadoresE2[gol.jugadorId] = player.name;
          });
        });

        // Obtener detalles de jugadores involucrados en faltas
        this.faltasE2.forEach(falta => {
          this.playerService.getPlayer(falta.jugadorId).subscribe(player => {
            this.jugadoresE2[falta.jugadorId] = player.name;
          });
        });

        // Obtener detalles de jugadores que recibieron tarjetas amarillas
        this.tarjetasAmarillasE2.forEach(tarjeta => {
          this.playerService.getPlayer(tarjeta.jugadorId).subscribe(player => {
            this.jugadoresE2[tarjeta.jugadorId] = player.name;
          });
        });

        // Obtener detalles de jugadores involucrados en las tarjetas rojas
        this.tarjetasRojasE2.forEach(tarjeta => {
          this.playerService.getPlayer(tarjeta.jugadorId).subscribe(player => {
            this.jugadoresE2[tarjeta.jugadorId] = player.name;
          });
        });

        // Obtener detalles de jugadores involucrados en las tarjetas azules
        this.tarjetasAzulesE2.forEach(tarjeta => {
          this.playerService.getPlayer(tarjeta.jugadorId).subscribe(player => {
            this.jugadoresE2[tarjeta.jugadorId] = player.name;
          });
        });

        this.sustitucionesEntraE2 = data.sustituciones.map(sustitucion => ({
          jugadorEntraId: sustitucion.jugadorEntraId,
          descripcion: sustitucion.descripcion,
          tiempo: sustitucion.tiempo,
          minuto: sustitucion.minuto,
          segundo: sustitucion.segundo
        }));
      
        this.sustitucionesSaleE2 = data.sustituciones.map(sustitucion => ({
          jugadorSaleId: sustitucion.jugadorSaleId,
          descripcion: sustitucion.descripcion,
          tiempo: sustitucion.tiempo,
          minuto: sustitucion.minuto,
          segundo: sustitucion.segundo
        }));
      
        // Obtener detalles de los jugadores que entran
        this.sustitucionesEntraE2.forEach(sustitucion => {
          this.playerService.getPlayer(sustitucion.jugadorEntraId).subscribe(player => {
            this.jugadoresE2[sustitucion.jugadorEntraId] = player.name;
          });
        });
      
        // Obtener detalles de los jugadores que salen
        this.sustitucionesSaleE2.forEach(sustitucion => {
          this.playerService.getPlayer(sustitucion.jugadorSaleId).subscribe(player => {
            this.jugadoresE2[sustitucion.jugadorSaleId] = player.name;
          });
        });
      });
    
    }, error => {
      //console.error('Error al obtener los datos del partido:', error);
    });
    
  }

  // Método para procesar los datos de los jugadores
  private processPlayerData(data: DatosEquipo, playersArray: PlayerData[]): void {
    // Procesar goles
    data.goles.forEach(gol => {
      this.playerService.getPlayer(gol.jugadorId).subscribe(player => {
        //console.log('Jugador de goles:', player);  // Verifica el jugador
        let playerData = playersArray.find(p => p.numero === player.number);
        if (!playerData) {
          playerData = {
            numero: player.number,
            nombre: player.name,
            goles: 0,
            golesContra: 0,
            faltas: 0,
            tarj_red: 0,
            tarj_yellow: 0,
            tarj_blue: 0,
            cambios: null
          };
          playersArray.push(playerData);
        }
        playerData.goles += 1;
        //console.log('Jugador con goles actualizados:', playerData);  // Verifica el jugador con goles actualizados
      });
    });

    // Procesar goles en contra
    data.golesContra.forEach(gol => {
      this.playerService.getPlayer(gol.jugadorId).subscribe(player => {
        let playerData = playersArray.find(p => p.numero === player.number);
        if (!playerData) {
          playerData = {
            numero: player.number,
            nombre: player.name,
            goles: 0,
            golesContra: 0,
            faltas: 0,
            tarj_red: 0,
            tarj_yellow: 0,
            tarj_blue: 0,
            cambios: null
          };
          playersArray.push(playerData);
        }
        playerData.golesContra += 1;
      });
    });

    // Procesar faltas
    data.faltas.forEach(falta => {
      this.playerService.getPlayer(falta.jugadorId).subscribe(player => {
        let playerData = playersArray.find(p => p.numero === player.number);
        if (!playerData) {
          playerData = {
            numero: player.number,
            nombre: player.name,
            goles: 0,
            golesContra: 0,
            faltas: 0,
            tarj_red: 0,
            tarj_yellow: 0,
            tarj_blue: 0,
            cambios: null
          };
          playersArray.push(playerData);
        }
        playerData.faltas += 1;
      });
    });

    // Procesar tarjetas amarillas
    data.tarjetasAmarillas.forEach(tarjeta => {
      this.playerService.getPlayer(tarjeta.jugadorId).subscribe(player => {
        let playerData = playersArray.find(p => p.numero === player.number);
        if (!playerData) {
          playerData = {
            numero: player.number,
            nombre: player.name,
            goles: 0,
            golesContra: 0,
            faltas: 0,
            tarj_red: 0,
            tarj_yellow: 0,
            tarj_blue: 0,
            cambios: null
          };
          playersArray.push(playerData);
        }
        playerData.tarj_yellow += 1;
      });
    });

    // Procesar tarjetas rojas
    data.tarjetasRojas.forEach(tarjeta => {
      this.playerService.getPlayer(tarjeta.jugadorId).subscribe(player => {
        let playerData = playersArray.find(p => p.numero === player.number);
        if (!playerData) {
          playerData = {
            numero: player.number,
            nombre: player.name,
            goles: 0,
            golesContra: 0,
            faltas: 0,
            tarj_red: 0,
            tarj_yellow: 0,
            tarj_blue: 0,
            cambios: null
          };
          playersArray.push(playerData);
        }
        playerData.tarj_red += 1;
      });
    });

    // Procesar tarjetas azules
    data.tarjetasAzules.forEach(tarjeta => {
      this.playerService.getPlayer(tarjeta.jugadorId).subscribe(player => {
        let playerData = playersArray.find(p => p.numero === player.number);
        if (!playerData) {
          playerData = {
            numero: player.number,
            nombre: player.name,
            goles: 0,
            golesContra: 0,
            faltas: 0,
            tarj_red: 0,
            tarj_yellow: 0,
            tarj_blue: 0,
            cambios: null
          };
          playersArray.push(playerData);
        }
        playerData.tarj_blue += 1;
      });
    });

    // Procesar sustituciones
    data.sustituciones.forEach(sustitucion => {
      this.playerService.getPlayer(sustitucion.jugadorEntraId).subscribe(player => {
        let playerData = playersArray.find(p => p.numero === player.number);
        if (!playerData) {
          playerData = {
            numero: player.number,
            nombre: player.name,
            goles: 0,
            golesContra: 0,
            faltas: 0,
            tarj_red: 0,
            tarj_yellow: 0,
            tarj_blue: 0,
            cambios: 'entrada'
          };
          playersArray.push(playerData);
        } else {
          playerData.cambios = 'entrada';
        }
      });

      this.playerService.getPlayer(sustitucion.jugadorSaleId).subscribe(player => {
        let playerData = playersArray.find(p => p.numero === player.number);
        if (!playerData) {
          playerData = {
            numero: player.number,
            nombre: player.name,
            goles: 0,
            golesContra: 0,
            faltas: 0,
            tarj_red: 0,
            tarj_yellow: 0,
            tarj_blue: 0,
            cambios: 'salida'
          };
          playersArray.push(playerData);
        } else {
          playerData.cambios = 'salida';
        }
      });
    });
  }  
  
  getArray(count: number): any[] {
    return new Array(count);
  }

}
