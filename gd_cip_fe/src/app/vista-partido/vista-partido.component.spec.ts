import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaPartidoComponent } from './vista-partido.component';

describe('VistaPartidoComponent', () => {
  let component: VistaPartidoComponent;
  let fixture: ComponentFixture<VistaPartidoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VistaPartidoComponent]
    });
    fixture = TestBed.createComponent(VistaPartidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
