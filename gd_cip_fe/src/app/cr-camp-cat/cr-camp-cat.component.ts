import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CampeonatoService } from '../services/SCrearCamp/campeonato.service';

@Component({
  selector: 'app-cr-camp-cat',
  templateUrl: './cr-camp-cat.component.html',
  styleUrls: ['./cr-camp-cat.component.css']
})
export class CrCampCatComponent {

  nombreCamp: string = ''; // Nombrecampo

  constructor(
    public dialogRef: MatDialogRef<CrCampCatComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private campeonatoService: CampeonatoService
  ) {
    console.log('Datos recibidos:', this.data);
  }

  crearCampeonato() {

    if (!this.data || !this.data.tipoCampeonato) {
      console.error('Error: No se recibió el tipo de campeonato.');
      return;
    }

    const campeonato = {
      nombreCamp: this.nombreCamp,
      tipoCampeonato: {
        idTC: this.data.tipoCampeonato.idTC,
        tipo: this.data.tipoCampeonato.tipo
      }
    };
    
    this.campeonatoService.createCampeonato(campeonato).subscribe(
      response => {
        console.log('Campeonato creado:', response);
        this.dialogRef.close(response);
      },
      error => {
        console.error('Error al crear el campeonato:', error); // Mostrar detalles del error en la consola
      }
    );
  }

}
