import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrCampCatComponent } from './cr-camp-cat.component';

describe('CrCampCatComponent', () => {
  let component: CrCampCatComponent;
  let fixture: ComponentFixture<CrCampCatComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CrCampCatComponent]
    });
    fixture = TestBed.createComponent(CrCampCatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
