import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { FasesService } from '../services/SCrearCamp/fases.service';
import { CategoriasService } from '../services/SCrearCamp/categorias.service';


@Component({
  selector: 'app-crear-campeonato',
  templateUrl: './crear-campeonato.component.html',
  styleUrls: ['./crear-campeonato.component.css']
})
export class CrearCampeonatoComponent implements OnInit {

  catNombre: string = '';
  fases: any[] = [];
  selectedFase: any;

  categoria: any = {
    catNombre: '',
    deporte: null,
    fases: { idfases: 1, nombre: 'Todos contra todos' },
    campeonato: null
  }


  constructor(
    private fasesService: FasesService,
    private categoriasService: CategoriasService,
    public dialogRef: MatDialogRef<CrearCampeonatoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {
    this.fasesService.getFases().subscribe(
      data => {
        this.fases = data;
      },
      error => {
        console.error('Error al obtener las fases:', error);
      }
    );
  }

  crearCategoria():void {
    const nuevaCategoria = {
      catNombre: this.catNombre,
      fases: this.selectedFase,
      deporte: this.data.deporte , // Incluyendo el deporte seleccionado
      campeonato: { idCamp: this.data.campeonatoId }
    };

    this.categoriasService.createCategorias(nuevaCategoria).subscribe(
      response => {
        console.log('Categoría creada exitosamente:', response);
        this.dialogRef.close(response);  // Cerrar el modal con el resultado
      },
      error => {
        console.error('Error al crear la categoría:', error);
      }
    );
  }

  onFaseChange(fase: any) {
    this.selectedFase = fase;
  }
}
