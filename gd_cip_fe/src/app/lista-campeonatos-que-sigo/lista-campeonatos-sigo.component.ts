import {
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
  Input,
  OnDestroy,
} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CampeonatoService, Campeonato } from '../services/campeonato.service';
import { CampeonatosSeguidosService, CampeonatosSeguidos } from '../services/campeonatos-seguidos.service';
import { CampeonatosCategoriasSeguidosService, CampeonatosCategoriasSeguidos } from '../services/campeonatos-categorias-seguidos.service';
import { Equipo, EquipoService } from '../services/equipo.service';
import { EquipoCampeonatoService } from '../services/equipo-campeonato.service';
import { EquipoCampeonato } from '../services/equipo-campeonato.service';
import { PartidoDTO, PartidosService } from '../services/partidos.service';
import { BehaviorSubject, combineLatest, Subject, takeUntil } from 'rxjs';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { CampeonatoCategoriasService, CampeonatoCategorias } from '../services/campeonato-categorias.service';


@Component({
  selector: 'app-lista-campeonatos-sigo',
  templateUrl: './lista-campeonatos-sigo.component.html',
  styleUrls: ['./lista-campeonatos-sigo.component.css'],
})
export class ListaCampeonatosQueSigoComponent implements OnInit, OnDestroy {
  @ViewChild('campeonatoDialog') campeonatoDialog!: TemplateRef<any>;
  @ViewChild('seleccionarEquiposDialog')
  seleccionarEquiposDialog!: TemplateRef<any>;
  @ViewChild('confirmation') confirmation!: ConfirmationComponent;

  @Input() esAdministrador: boolean = true;
  nuevoCampeonatoOpen = false;

  nombreCamp = 'Vacio';

  campeonatos: Campeonato[] = [];
  campeonatosSeguidos: CampeonatosSeguidos[] = [];
  campeonatosPorCategoriasSeguidos: CampeonatosCategoriasSeguidos[] = [];
  filteredCampeonatosFavs: Campeonato[] = [];
  filteredCampeonatos: Campeonato[] = [];
  campeonatoWithCategories: CampeonatoCategorias[] = [];
  filteredCampeonatosWithCategories: CampeonatoCategorias[] = [];
  searchTerm: string = '';
  filteredEquipos: Equipo[] = [];
  equipoSearchTerm: string = '';
  campeonatoForm: FormGroup;
  equipoForm: FormGroup;
  equipos: Equipo[] = [];
  isEditing: boolean = false;
  isCreatingWithCategories: boolean = false;
  isEditingWithCategories: boolean = false;
  currentCampeonatoId?: number;
  equiposForm: FormGroup;
  urlImagen: string = '';
  equiposJugaron = new Map<number, boolean>();
  campeonatoCategoriasForm: FormGroup;

  desplegablesCampeonatos: boolean[] = [];

  destroy$ = new Subject<void>();
  likee:boolean;                      //PRUEBA LIKE

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private router: Router,
    private equipoService: EquipoService,
    private CampeonatoService: CampeonatoService,
    private CampeonatosSeguidosService: CampeonatosSeguidosService,
    private CampeonatosCategoriasSeguidosService: CampeonatosCategoriasSeguidosService,
    private route: ActivatedRoute,
    private equipoCampeonatoService: EquipoCampeonatoService,
    private partidoService: PartidosService,
    private CampeonatoCategoriasService: CampeonatoCategoriasService,
    
   
  ) {
    this.likee=false;                      //PRUEBA LIKE

    this.campeonatoForm = this.fb.group({
      tituloCampeonato: ['', Validators.required],
      tipoCampeonato: ['', Validators.required],
      fechaInicio: ['', Validators.required],
      fechaFin: ['', Validators.required],
      organizadorNombre: ['', Validators.required],
      organizadorEmail: ['', [Validators.required, Validators.email]], // Corrige aquí
      organizadorNumero: ['', [Validators.pattern('^[0-9]{9}$')]],
      tituloHonorifico: [''],
    });

    this.campeonatoCategoriasForm = this.fb.group({
      tituloCampeonato: ['', Validators.required],
      fechaInicio: ['', Validators.required],
      fechaFin: ['', Validators.required],
      organizadorNombre: ['', Validators.required],
      organizadorEmail: ['', [Validators.required, Validators.email]], 
      organizadorNumero: ['', [Validators.pattern('^[0-9]{9}$')]],
      tituloHonorifico: [''],
    });

    this.equiposForm = this.fb.group({
      equipos: [[]],
    });

    this.equipoForm = this.fb.group({
      email: [''],
      nombre: [''],
      entrenador: [''],
      delegadoNombre1: [''],
      delegadoTelefono1: [''],
      delegadoNombre2: [''],
      delegadoTelefono2: [''],
      delegadoNombre3: [''],
      delegadoTelefono3: [''],
      delegadoNombre4: [''],
      delegadoTelefono4: [''],
      delegadoNombre5: [''],
      delegadoTelefono5: [''],
    });
  }

  like() {                               //PRUEBA LIKE
    this.likee=!this.likee
   }


   toggleFavCamp(campeonato: Campeonato) {    //funcion favoritos camp
    
    const newCampeonatoSeguido: CampeonatosSeguidos = { campeonatoId: campeonato.id, usuarioCipId: Number(sessionStorage.getItem("usuarioCipId"))};
    
    console.log('Creando nuevo campeonato fav con datos:', newCampeonatoSeguido);

    if(this.isCampeonatoFav(campeonato)){
      const currentCampFav = this.campeonatosSeguidos.find(campSeg => campSeg.campeonatoId === campeonato.id)
      
      this.CampeonatosSeguidosService.deleteById(currentCampFav?.id).subscribe(() => {
        console.log('Campeonato eliminado con éxito',currentCampFav);
        this.loadCampeonatosSeguidos();
      })
    } else {
      this.CampeonatosSeguidosService.save(newCampeonatoSeguido).subscribe(() => {
        this.loadCampeonatosSeguidos();
        console.log('Campeonato fav agregado');
    })
    }
  }
  

  isCampeonatoFav(campeonato: Campeonato): boolean {
    const exists = this.campeonatosSeguidos.some(campSeg => campSeg.campeonatoId === campeonato.id);
    return exists
  }


  toggleFavCampCategorias(campeonatoCat: Campeonato) {    //funcion favoritos camp
    
    const newCampeonatoSeguidoCat: CampeonatosCategoriasSeguidos = { campeonatoCategoriasId: campeonatoCat.id, usuarioCipId: Number(sessionStorage.getItem("usuarioCipId"))};
    console.log('Creando nuevo campeonato POR CATEGORIAA fav con datos:', newCampeonatoSeguidoCat);

    if(this.isCampeonatoFavCategorias(campeonatoCat)){
      const currentCampFavCat = this.campeonatosPorCategoriasSeguidos.find(campSeg => campSeg.campeonatoCategoriasId === campeonatoCat.id)

      this.CampeonatosCategoriasSeguidosService.deleteById(currentCampFavCat?.id).subscribe(() => {
        console.log('Campeonato POR CATEGORIAA eliminado con éxito',campeonatoCat.id);
        this.loadCampeonatosPorCategoriasSeguidos();
      })
    } else {
      this.CampeonatosCategoriasSeguidosService.save(newCampeonatoSeguidoCat).subscribe(() => {
        this.loadCampeonatosPorCategoriasSeguidos();
        console.log('Campeonato POR CATEGORIAA fav agregado');
    })
    }
  }

  isCampeonatoFavCategorias(campeonatoCat: Campeonato): boolean {
    const exists = this.campeonatosPorCategoriasSeguidos.some(campSegCat => campSegCat.campeonatoCategoriasId === campeonatoCat.id);
    return exists
  }



  ngOnInit(): void {
    console.log('ListaCampeonatosComponent.ngOnInit()');
    this.loadCampeonato();
    this.loadEquipos();
    this.loadCampeonatosSeguidos();
    this.loadCampeonatosWithCategories();
    this.loadCampeonatosPorCategoriasSeguidos();
  }

  ngOnDestroy(): void {
    console.log('ListaCampeonatosComponent.ngOnDestroy()');
    this.destroy$.next();
    this.destroy$.complete();
  }
  navigateToMenu2(id: number) {
    console.log(id);
    this.router.navigate(['/menu2'], { queryParams: { id: id.toString() } });
  }

  navigateToMenuCampeonatoCategorias(id: number): void {
    console.log(`Redirigiendo a menuCampeonatoCategorias con id: ${id}`);
    this.router.navigate(['/menu-campeonato-categorias'], { queryParams: { id: id.toString() } });
  }

  loadCampeonato(): void {
    this.CampeonatoService.getCampeonatos().subscribe((campeonatos) => {
      console.log(campeonatos);
      this.campeonatos = campeonatos;

      this.desplegablesCampeonatos = new Array(this.campeonatos.length).fill(
        false
      );
      console.log(this.desplegablesCampeonatos);
      this.filteredCampeonatos = campeonatos;
      this.filteredCampeonatosFavs = campeonatos;
    });
   
  }

  loadCampeonatosSeguidos(): void {
    this.CampeonatosSeguidosService.getCampeonatosSeguidos().subscribe(campeonatosSeguidos => {
      console.log(campeonatosSeguidos);
      this.campeonatosSeguidos = campeonatosSeguidos;
    });
  }

  loadCampeonatosPorCategoriasSeguidos(): void {
    this.CampeonatosCategoriasSeguidosService.getCampeonatoCaegoriaSeguidos().subscribe(campeonatosCatSeguidos => {
      console.log(campeonatosCatSeguidos);
      this.campeonatosPorCategoriasSeguidos = campeonatosCatSeguidos;
    });
  }

  loadCampeonatosWithCategories(): void {
    this.CampeonatoCategoriasService.findAll().subscribe(campeonatosWithCategories => {
      console.log("Campeonatos por cateogoria cargados inicialmente" , campeonatosWithCategories);
      this.campeonatoWithCategories = campeonatosWithCategories;
      this.filteredCampeonatosWithCategories = campeonatosWithCategories
    });
  }

  loadEquipos(): void {
    this.equipoService.getEquipos().subscribe(
      (equipos) => {
        console.log(equipos);
        this.equipos = equipos; // Asigna la respuesta a la lista de equipos
        // Inicializar el FormGroup para cada equipo
        this.equiposForm = this.fb.group({});
        this.filteredEquipos = equipos;

        this.equipos.forEach((equipo) => {
          this.equiposForm.addControl(
            equipo.id!.toString(),
            this.fb.control(false)
          ); // Asegúrate de que el ID sea una cadena
        });
      },
      (error) => {
        console.error('Error al cargar los equipos', error); // Manejo de errores
      }
    );
  }

  //Metodos de Dialogo
  private ajustarValidacionTipoCampeonato(): void {
    const tipoCampeonatoControl = this.campeonatoForm.get('tipoCampeonato');
  
    if (!this.isCreatingWithCategories && !this.isEditingWithCategories) {
      // Si es un campeonato normal, el campo debe ser obligatorio
      tipoCampeonatoControl?.setValidators([Validators.required]);
    } else {
      // Si es un campeonato con categorías, quitar el validador obligatorio
      tipoCampeonatoControl?.clearValidators();
    }
  
    // Asegurarse de que los cambios se apliquen
    tipoCampeonatoControl?.updateValueAndValidity();
  }

  openAddCampeonatoDialog(): void {
    this.isEditing = false;
    this.campeonatoForm.reset();
    this.dialog.open(this.campeonatoDialog);
  }

  openEditCampeonatoDialog(campeonato: Campeonato): void {
    this.isEditing = true;
    this.currentCampeonatoId = campeonato.id;
    this.campeonatoForm.patchValue(campeonato);
    this.dialog.open(this.campeonatoDialog);
  }

  openEditCampeonatoWithCategoriesDialog(campeonatoCategorias: CampeonatoCategorias): void {
    console.log("Entrando en modo edición con categorías");
    this.isEditing = false;
    this.isCreatingWithCategories = false;
    this.isEditingWithCategories = true;
    
    console.log("ID del campeonato actual:", campeonatoCategorias.id);
    this.currentCampeonatoId = campeonatoCategorias.id;
    
    console.log("Datos del campeonato para precargar:", campeonatoCategorias);
    this.campeonatoForm.patchValue(campeonatoCategorias);
    
    console.log("Formulario después de patchValue:", this.campeonatoCategoriasForm.value);
    
    this.ajustarValidacionTipoCampeonato(); // Ajustar validación
    console.log("Validación de tipo de campeonato ajustada");
    // Cargar categorías existentes en el campeonato
    this.dialog.open(this.campeonatoDialog);
}

  closeDialog(): void {
    this.dialog.closeAll();
  }

  //Manipulacion de Campeonatos
  saveCampeonato(): void {
    console.log('Estado actual del formulario:', this.campeonatoForm.value);

    // Verificar si el título del campeonato ya existe
    const tituloCampeonato = this.campeonatoForm.get('tituloCampeonato')?.value;
    const tituloDuplicado = this.campeonatos.some(
      (campeonato) => campeonato.tituloCampeonato === tituloCampeonato
    );

    if (tituloDuplicado) {
      alert(
        'El título del campeonato ya está en uso. Por favor, elige otro título.'
      );
      return; // Detener el guardado si el título está duplicado
    }

    // Agregar el nuevo campo solo una vez para evitar que se agregue cada vez que se guarda
    if (!this.campeonatoForm.contains('urlImagen')) {
      this.campeonatoForm.addControl('urlImagen', this.fb.control(''));
      console.log('Campo "urlImagen" agregado al formulario.');
    }

    // Obtener el valor de tipoCampeonato directamente desde el formulario
    const tipoCampeonatoSeleccionado =
      this.campeonatoForm.get('tipoCampeonato')?.value;
    console.log('Tipo de Campeonato Seleccionado:', tipoCampeonatoSeleccionado);

    let imagenUrl = '';

    if (tipoCampeonatoSeleccionado == '1') {
      imagenUrl = '../../assets/portadaSoccer.jpg';
    } else if (tipoCampeonatoSeleccionado == '2') {
      imagenUrl = '../../assets/portadaVoley.jpg';
    }
    console.log('Imagen URL:', imagenUrl);

    // Actualizar el campo urlImagen en el formulario
    this.campeonatoForm.patchValue({
      urlImagen: imagenUrl,
    });
    console.log('URL de imagen asignada a urlImagen:', imagenUrl);

    // Confirmación de edición o creación
    const confirmation = this.isEditing
      ? window.confirm(`¿Estás seguro de que deseas editar este campeonato?`)
      : window.confirm(`¿Estás seguro de que deseas agregar este campeonato?`);

    // Verificar la respuesta de la confirmación
    if (!confirmation) {
      console.log('Operación cancelada por el usuario.');
      return;
    }

    if (this.isEditing && this.currentCampeonatoId !== undefined) {
      const updateCampeonato: Campeonato = {
        id: this.currentCampeonatoId,
        ...this.campeonatoForm.value,
      };
      console.log(
        'Actualizando campeonato con ID:',
        this.currentCampeonatoId,
        'Con datos:',
        updateCampeonato
      );

      this.CampeonatoService.updateCampeonato(updateCampeonato).subscribe(
        () => {
          console.log('Campeonato actualizado correctamente.');
          this.loadCampeonato();
          this.dialog.closeAll();
        }
      );
    } else {
      const newCampeonato: Campeonato = { ...this.campeonatoForm.value };
      console.log('Creando nuevo campeonato con datos:', newCampeonato);

      this.CampeonatoService.addCampeonato(newCampeonato).subscribe(() => {
        console.log('Nuevo campeonato creado correctamente.');
        this.loadCampeonato();
        this.dialog.closeAll();
      });
    }
    this.confirmation.show();
  }

  filterCampeonatos(): void {
    console.log('Término de búsqueda:', this.searchTerm);

    this.filteredCampeonatos = this.campeonatos.filter((campeonato) =>
      campeonato.tituloCampeonato
        .toLowerCase()
        .includes(this.searchTerm.toLowerCase())
    );
  }

  filterCampeonatosCategorias(): void {
    console.log('Término de búsqueda:', this.searchTerm);

    this.filteredCampeonatosWithCategories = this.campeonatoWithCategories.filter(campeonatoCategorias =>
      campeonatoCategorias.tituloCampeonato.toLowerCase().includes(this.searchTerm.toLowerCase())
    );
  }

  filterCampeonatosFavortios(): void {
    console.log('los filtrados favs anrtwes', this.filteredCampeonatosFavs);
    this.filteredCampeonatosFavs = this.campeonatos.filter((campeonato) => {
      campeonato.id === 1
    });
    console.log('los filtrados favs desp', this.filteredCampeonatosFavs);
    
  }

  filterEquipos(): void {
    // Imprimir el término de búsqueda
    console.log('Término de búsqueda:', this.equipoSearchTerm);

    // Filtrar los equipos
    this.filteredEquipos = this.equipos.filter((equipo) =>
      equipo.nombre.toLowerCase().includes(this.equipoSearchTerm.toLowerCase())
    );

    // Imprimir los equipos filtrados
    console.log('Equipos filtrados:', this.filteredEquipos);
  }
  onCampeonatoChange(event: any) {
    const tipoCampeonatoSeleccionado = event.target.value;

    let imagenUrl = '';
    if (tipoCampeonatoSeleccionado === '1') {
      imagenUrl = '../../assets/portadaSoccer.jpg';
    } else if (tipoCampeonatoSeleccionado === '2') {
      imagenUrl = '../../assets/portadaVoley.jpg';
    }

    // Asignamos los valores al formulario reactivo
    this.campeonatoForm.patchValue({
      urlImagen: imagenUrl,
    });
  }

  //Dialog para seleccionar equipos
  // Declaración del array para almacenar los equipos seleccionados
  selectedEquipos: any[] = [];

  // Método para añadir equipo a la lista seleccionada
  addToSelected(equipo: any): void {
    // Verifica si ya existe un equipo con el mismo ID en selectedEquipos
    const exists = this.selectedEquipos.some(
      (selected) => selected.id === equipo.id
    );

    if (!exists) {
      this.selectedEquipos.push(equipo);
    } else {
      console.log('El equipo ya está en la lista de seleccionados:', equipo);
    }
  }

  // Método para eliminar equipo de la lista seleccionada
  removeFromSelected(equipo: any): void {
    console.log('Estado actual de selectedEquipos:', this.selectedEquipos);
    console.log(
      'Intentando eliminar el equipo:',
      equipo,
      'del campeonato con id:',
      this.currentCampeonatoId
    );

    const haJugado = this.equiposJugaron.get(equipo.id);
    console.log('Valor de haJugado para el equipo:', haJugado); // Log para ver el valor de haJugado

    if (haJugado === false || haJugado == undefined) {
      console.log(
        'El equipo no ha jugado. Procediendo a buscar el EquipoCampeonato...'
      );

      this.equipoCampeonatoService
        .findByCampeonatoId(this.currentCampeonatoId!)
        .subscribe({
          next: (equiposCampeonato) => {
            const equipoCampeonato = equiposCampeonato.find(
              (ec) =>
                ec.equipoId === equipo.id &&
                ec.campeonatoId === this.currentCampeonatoId
            );
            console.log(
              'Valor de equipoCampeonato para el equipo:',
              equipoCampeonato
            ); // Log para ver el valor de haJugado

            if (equipoCampeonato) {
              console.log(
                'EquipoCampeonato encontrado para eliminar:',
                equipoCampeonato
              );
              this.equipoCampeonatoService.delete(equipoCampeonato).subscribe({
                next: (response) => {
                  console.log(
                    'Equipo eliminado de la base de datos:',
                    response
                  );
                  this.selectedEquipos = this.selectedEquipos.filter(
                    (e) => e.id !== equipo.id
                  );
                  console.log(
                    'Equipos seleccionados después de eliminar:',
                    this.selectedEquipos
                  );
                },
                error: (err) => {
                  console.error('Error al eliminar el equipo:', err);
                },
              });
            } else {
              console.log('El equipo es nuevo, eliminando de selectedEquipos.');
              this.selectedEquipos = this.selectedEquipos.filter(
                (e) => e.id !== equipo.id
              );
              console.log(
                'Equipos seleccionados después de eliminar (equipo nuevo):',
                this.selectedEquipos
              );
            }
          },
          error: (error) => {
            console.error('Error al cargar equipos del campeonato', error);
          },
        });
    } else {
      // Usar una alerta simple para informar que no se puede eliminar
      alert('No se puede quitar el equipo porque ha jugado un partido.');
      console.log('El equipo ha jugado y no se puede eliminar.');
    }
  }
  // Método para abrir el diálogo de selección de equipos
  openSeleccionarEquiposDialog(campeonato: Campeonato): void {
    this.isEditing = true;
    this.currentCampeonatoId = campeonato.id;
    this.campeonatoForm.patchValue(campeonato);
    console.log(
      'El campeonato al que se añadirá tiene el id ' + this.currentCampeonatoId
    );

    /*
    this.loadEquiposPorCampeonato(this.currentCampeonatoId);
    console.log('Equipos seleccionados antes de abrir el diálogo:', this.selectedEquipos);
    */

    combineLatest([
      this.equipoCampeonatoService.findByCampeonatoId(this.currentCampeonatoId),
      this.partidoService.getPartidosByCampeonato(this.currentCampeonatoId),
    ])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([equipoCampeonatoData, partidosData]) => {
        //this.getEquiposJuegan(equipoCampeonatoData, partidosData)
        let equiposEnCampeonato = this.CampeonatoService.getEquiposEnCampeonato(
          this.equipos,
          equipoCampeonatoData
        );
        this.selectedEquipos = equiposEnCampeonato;

        this.equiposJugaron = this.getEquiposJugaron(
          equiposEnCampeonato,
          partidosData
        );
        console.log('equiposJugaron', this.equiposJugaron);
      });

    if (this.currentCampeonatoId !== undefined) {
      this.dialog.open(this.seleccionarEquiposDialog, {
        width: '500px',
      });
    } else {
      console.error(
        'No se ha seleccionado ningún campeonato antes de abrir el diálogo'
      );
    }
  }

  // Método para cerrar el diálogo y reiniciar el formulario
  closeSeleccionarEquiposDialog(): void {
    this.equiposForm.reset();
    this.selectedEquipos = []; // Reiniciar el array de equipos seleccionados
    this.dialog.closeAll();
  }

  // Método para añadir los equipos seleccionados al campeonato
  addEquiposToCampeonato(): void {
    if (
      confirm(
        '¿Estás seguro de que deseas realizar estos cambios en los equipos del campeonato?'
      )
    ) {
      if (this.currentCampeonatoId !== undefined) {
        // Filtrar solo los equipos nuevos (que no están en la base de datos)
        const equiposNuevos = this.selectedEquipos.filter(
          (equipo) => !this.equiposJugaron.has(equipo.id)
        ); // Verifica si el equipo no está en el Map

        if (equiposNuevos.length > 0) {
          const equiposCampeonato: EquipoCampeonato[] = equiposNuevos.map(
            (equipo) => ({
              id: 0,
              equipoId: equipo.id, // Extraer solo el id del objeto equipo
              campeonatoId: this.currentCampeonatoId!,
              grupoId: null 
            })
          );

          console.log(
            'IDs de equipos seleccionados para agregar:',
            this.selectedEquipos
          );
          console.log(
            'Equipos nuevos formateados para enviar:',
            equiposCampeonato
          ); // Log para ver cómo queda el objeto
          console.log(
            'ID del campeonato al que se están agregando los equipos:',
            this.currentCampeonatoId
          );

          // Llamar al servicio para guardar los equipos asociados al campeonato
          this.equipoCampeonatoService.saveAll(equiposCampeonato).subscribe(
            () => {
              console.log('Equipos añadidos al campeonato con éxito');
              this.loadCampeonato();
              this.closeSeleccionarEquiposDialog();
            },
            (error) => {
              console.error('Error al añadir equipos al campeonato', error);
            }
          );
        } else {
          console.log('No hay equipos nuevos que añadir.');
        }
      } else {
        console.error('No se ha seleccionado ningún campeonato');
      }
      this.dialog.closeAll();
    }
  }

  deleteCampeonato(): void {
    if (this.currentCampeonatoId !== undefined) {
      if (confirm('¿Estás seguro de que deseas eliminar este campeonato?')) {
        this.CampeonatoService.deleteCampeonatoById(
          this.currentCampeonatoId
        ).subscribe(
          () => {
            console.log('Campeonato eliminado con éxito');
            this.loadCampeonato();
            this.dialog.closeAll();
          },
          (error) => {
            console.error('Error al eliminar el campeonato:', error);
            console.log(
              'Error completo:',
              error.message,
              error.status,
              error.error
            );
          }
        );
      }
    } else {
      console.error('No se ha seleccionado ningún campeonato para eliminar');
    }
  }

  loadEquiposPorCampeonato(campeonatoId: number): void {
    this.equipoCampeonatoService.findByCampeonatoId(campeonatoId).subscribe({
      next: (equiposCampeonato) => {
        // Imprimir en consola lo que retorna findByCampeonatoId
        console.log(
          'Datos retornados por findByCampeonatoId:',
          equiposCampeonato
        );

        // Llenar selectedEquipos con la marca de equipos existentes
        this.selectedEquipos = equiposCampeonato
          .map((ec) => {
            const equipo = this.equipos.find(
              (equipo) => equipo.id === ec.equipoId
            );
            if (equipo) {
              return { ...equipo, isExisting: true }; // Añadir propiedad isExisting
            }
            return undefined;
          })
          .filter((equipo) => equipo !== undefined);

        // Mostrar el contenido de selectedEquipos con el campo isExisting añadido
        console.log(
          'Contenido de selectedEquipos después de agregar isExisting:',
          this.selectedEquipos
        );
      },
      error: (error) => {
        console.error('Error al cargar los equipos del campeonato', error);
      },
    });
  }

  getEquiposJugaron(
    equipos: Equipo[],
    partidos: PartidoDTO[]
  ): Map<number, boolean> {
    //map => {key: <equipoId>, value: <siJuega>}
    const res = new Map<number, boolean>();

    equipos.forEach((element) => {
      res.set(element.id, false);
    });

    partidos.forEach((element) => {
      if (element.equipo1Id) res.set(element.equipo1Id, true);
      if (element.equipo2Id) res.set(element.equipo2Id, true);
    });

    return res;
  }

  eliminarEquipoCampeonato(equipoCampeonato: EquipoCampeonato): void {
    this.equipoCampeonatoService.delete(equipoCampeonato).subscribe({
      next: (response) => {
        // Aquí puedes manejar la respuesta si es necesario
        console.log('Equipo eliminado:', response);
        // Actualiza tu lista de equipos después de la eliminación
        this.loadEquiposPorCampeonato(equipoCampeonato.campeonatoId);
      },
      error: (err) => {
        console.error('Error al eliminar el equipo:', err);
      },
    });
  }


}
