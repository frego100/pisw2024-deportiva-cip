import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaCampeonatosQueSigoComponent } from './lista-campeonatos-sigo.component';

describe('ListaCampeonatosQueSigoComponent', () => {
  let component: ListaCampeonatosQueSigoComponent;
  let fixture: ComponentFixture<ListaCampeonatosQueSigoComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ListaCampeonatosQueSigoComponent]
    });
    fixture = TestBed.createComponent(ListaCampeonatosQueSigoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
