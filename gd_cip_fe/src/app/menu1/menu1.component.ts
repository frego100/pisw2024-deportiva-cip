import { GlobalService } from '../services/global.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs'; // Importa Subscription de rxjs
import { RolUsuarioService } from '../rol-usuario.service';
import { AuthService } from '@auth0/auth0-angular';
import { Router } from '@angular/router';
@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.css']
})
export class Menu1Component {
  username= this.gs.nombreUsuario;
  nuevoCampeonatoOpen= false;
  isLoggedIn= false;

  esAdministrador= false;

  private subscription!: Subscription;

  menuItems = {
    misCampeonatosMenu: true,
    registroEquiposMenu: false,
    registroJugadoresMenu: false,
    registroUsuariosMenu: false,
    misCampeonatosQueSigoMenu: false,
  };
  constructor(
    private router: Router,
    private rolUsuarioService: RolUsuarioService,
    private gs: GlobalService,
    public auth: AuthService,
  ) {}

  resetMenuItems() {
    Object.keys(this.menuItems).forEach((key: string) => {
      // Afirmación de tipo como índice del objeto menuItems
      (this.menuItems as {[key: string]: boolean})[key] = false;
    });
  }
  ngOnInit(): void {
    this.subscription = this.rolUsuarioService.esAdministrador$.subscribe(esAdministrador => {
      this.esAdministrador = esAdministrador;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  hacerAdministrador(): void {
    this.rolUsuarioService.cambiarEsAdministrador(true);
  }

  LogoutwithAuth0(){
    const returnToUrl = window.location.origin; // Detecta el origen dinámicamente
    this.auth.logout({ logoutParams: { returnTo: returnToUrl } });
  }

  logout(): void {
    this.gs.logout();
    const returnToUrl = window.location.origin; // Detecta el origen dinámicamente
    this.auth.logout({ logoutParams: { returnTo: returnToUrl } });
    this.router.navigate([returnToUrl+'/user-login']);
  }
}
