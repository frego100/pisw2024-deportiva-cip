import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { combineLatest, Subject, take, takeUntil } from 'rxjs';
import { Campeonato } from '../services/campeonato.service';
import { Equipo, EquipoService } from '../services/equipo.service';
import { GlobalService } from '../services/global.service';
import { PartidoDTO, PartidosService } from '../services/partidos.service';
import { Player, PlayerService } from '../services/player.service';
import { RoleService } from '../services/role.service';

//declare var $: any;

@Component({
  selector: "app-editar-resultado",
  templateUrl: "./editar-resultado.component.html",
  styleUrls: ["./editar-resultado.component.css"],
})
export class EditarResultadoComponent implements OnInit, OnDestroy {
  equipo1: any = {
    nombre: "Equipo 1",
    goles: [],
    tarjetas: [],
    faltas: [],
    alineacion: [],
    sustituciones: [],
    golEnContra: [],
    asistencias: [], //Asistencias
  };

  equipo2: any = {
    nombre: "Equipo 2",
    goles: [],
    tarjetas: [],
    faltas: [],
    alineacion: [],
    sustituciones: [],
    golEnContra: [],
    asistencias: [], //Asistencias
  };

  @Input() partido!: PartidoDTO
  @Input() campeonatoId!: number

  @Output() onEliminar = new Subject<void>()
  @Output() onGuardar = new Subject<void>()

  partidoEquipo1DetDel: any;
  partidoEquipo2DetDel: any;
  //mejorJugador: string | null = null;
  mejorJugadorId: number | null = null;
  mejorPorteroId: number | null = null;
  allPlayers: any[] = [];

  selectedStatus = "";
  marcador = {
    equipo1: 0,
    equipo2: 0,
  };
  fase: number = 0;

  //Equipos
  allTeams: { id: number; name: string }[] = [];
  filteredTeams1: { id: number; name: string }[] = [];
  filteredTeams2: { id: number; name: string }[] = [];

  selectedTeam1: number | undefined;
  selectedTeam2: number | undefined;

  idCampeonato: number = -1;

  estado = this.gs.partidoFutbolEstado;
  equipo1Id = -1;
  equipo2Id = -1;

  destroy$ = new Subject<void>();
  equiposEnCampeonato$ = this.gs.equiposEnCampeonato$;

  @Input() campeonato!: Campeonato;
  @Input() isHidden: boolean = false;
  @Input() EquiposEliminatorias: any[] = [];
  @Input() faseInicial: string = "";

  tipoEditableBox = this.gs.tipoEditableBox

  isAdministrador = false

  constructor(
    private partidoService: PartidosService,
    private playerService: PlayerService,
    private equipoService: EquipoService,
    private gs: GlobalService,
    private rs: RoleService,
  ) { }

  async ngOnInit() {
    console.log("editar-resultado.ngOnInit()")
    this.isAdministrador = this.rs.hasRole('Administrador')
    this.fase = this.gs.partidoFutbolDTO.fase;
    //console.log(this.faseInicial)
    if (this.fase !== 1) {
      this.allTeams = this.EquiposEliminatorias.map((equipo) => ({
        id: equipo.id,
        name: equipo.team,
      }));
      await this.filtrarEquiposPorFase();
    }
    else {
      await this.cargarEquiposDelCampeonato();
    }

    this.inicializarEliminarDetalles();
    this.loadPartido(this.gs.partidoId);

    //console.log(this.allTeams);
    if (this.selectedTeam1 !== undefined && this.selectedTeam2 !== undefined) {
      this.filteredTeams1 = this.allTeams.filter(team => team.id! !== this.selectedTeam2);
      this.filteredTeams2 = this.allTeams.filter(team => team.id! !== this.selectedTeam1);
    } else {
      this.filteredTeams1 = [...this.allTeams];
      this.filteredTeams2 = [...this.allTeams];
    }
  }

  private filtrarEquiposPorFase(): Promise<void> {
    return new Promise((resolve) => {
      this.partidoService.getPartidosByCampeonato(this.campeonato.id).subscribe((partidos) => {
        const partidosMismaFase = partidos.filter((partido) => partido.fase === this.fase);
        const equiposEnUso = new Set<number>();
        partidosMismaFase.forEach((partido) => {
          if (partido.id === this.gs.partidoId) return; // Ignorar el partido actual
          if (partido.equipo1Id) equiposEnUso.add(partido.equipo1Id);
          if (partido.equipo2Id) equiposEnUso.add(partido.equipo2Id);
        });

        console.log("Equipos en uso:", equiposEnUso);
        this.allTeams = this.allTeams.filter((equipo) => !equiposEnUso.has(equipo.id));
        console.log(this.allTeams);
        resolve(); // Notificar que la operación ha terminado
      });
    });
  }

  private cargarEquiposDelCampeonato(): Promise<void> {
    return new Promise((resolve) => {
      this.equiposEnCampeonato$
        .pipe(takeUntil(this.destroy$))
        .subscribe((equipos: Equipo[]) => {
          this.allTeams = equipos.map((equipo) => ({
            id: equipo.id,
            name: equipo.nombre,
          }));
          resolve(); // Notificar que la operación ha terminado
        });
    });
  }

  loadPartido(partidoId: number) {
    //console.log("loadPartido")
    this.selectedStatus = this.gs.partidoFutbolDTO.tipoResultado;
    this.equipo1Id = this.gs.partidoFutbolDTO.equipo1Id;
    this.equipo2Id = this.gs.partidoFutbolDTO.equipo2Id;
    console.log("equiposId", this.equipo1Id, this.equipo2Id)

    // Si no hay equipos seleccionados, inicializa todo vacío
    if (
      this.equipo1Id === undefined ||
      this.equipo2Id === undefined ||
      this.equipo1Id === -1 ||
      this.equipo2Id === -1
    ) {
      this.inicializarPartidoVacio();
      return;
    }

    // Continuar con la carga de los equipos si están seleccionados
    this.selectedTeam1 = this.equipo1Id;
    this.selectedTeam2 = this.equipo2Id;

    this.loadDetalle(partidoId)
  }

  loadDetalle(partidoId: number){
    combineLatest([
      this.partidoService.getPartidoEquipo(partidoId, this.equipo1Id),
      this.partidoService.getPartidoEquipo(partidoId, this.equipo2Id),
    ])
    .pipe(take(1))
    .subscribe(([equipo1Detalle, equipo2Detalle]) => {
      this.mapResponseToTeam(equipo1Detalle, this.equipo1, this.equipo1Id)
      this.mapResponseToTeam(equipo2Detalle, this.equipo2, this.equipo2Id)

      this.marcador.equipo1 = this.equipo1.goles.length
      this.marcador.equipo2 = this.equipo2.goles.length

      let jugadoresEnAlineacion1 = this.getJugadoresEnAlineacion(this.equipo1.alineacion, this.equipo1Id)
      let jugadoresEnAlineacion2 = this.getJugadoresEnAlineacion(this.equipo2.alineacion, this.equipo2Id)

      this.updateJugadoresEnAlineacion(jugadoresEnAlineacion1, 1)
      this.updateJugadoresEnAlineacion(jugadoresEnAlineacion2, 2)

      this.allPlayers = [...jugadoresEnAlineacion1, ...jugadoresEnAlineacion2]
    })
  }

  getJugadoresEnAlineacion(alineacion: any[], equipoId: number) {
    //console.log("getJugadoresEnAlineacion alineacion", equipoId, alineacion)
    let jugadoresEnAlineacion: Player[] = []
    let jugadoresEnEquipo = new Map<number, Player>()
    //console.log("jugadoresEnCampeonato", this.gs.jugadoresEnCampeonato)
    this.gs.jugadoresEnCampeonato.get(equipoId)?.forEach(e => {
      if (e.id)
        jugadoresEnEquipo.set(e.id, e)
    })
    alineacion.forEach(e => {
      let player = jugadoresEnEquipo.get(e.jugadorId)
      if (player)
        jugadoresEnAlineacion.push(player)
    })

    return jugadoresEnAlineacion
  }

  updateJugadoresEnAlineacion(jugadoresEnAlineacion: any, numeroEquipo: number) {
    //console.log("jugadoresEnAlineacion", numeroEquipo, jugadoresEnAlineacion)
    if (numeroEquipo == 1)
      this.gs.updateJugadoresEnAlineacion1(jugadoresEnAlineacion)
    else
      this.gs.updateJugadoresEnAlineacion2(jugadoresEnAlineacion)
  }

  inicializarPartidoVacio() {
    this.equipo1 = {
      nombre: "Equipo 1",
      goles: [],
      tarjetas: [],
      faltas: [],
      alineacion: [],
      sustituciones: [],
      golEnContra: [],
      asistencias: [],
    };

    this.equipo2 = {
      nombre: "Equipo 2",
      goles: [],
      tarjetas: [],
      faltas: [],
      alineacion: [],
      sustituciones: [],
      golEnContra: [],
      asistencias: [],
    };

    this.marcador = {
      equipo1: 0,
      equipo2: 0,
    };

    this.selectedTeam1 = undefined;
    this.selectedTeam2 = undefined;

    //console.log("Partido inicializado sin equipos.");
  }

  mapResponseToTeam(response: any, equipo: any, equipoId: number) {
    equipo.goles = response.goles || [];

    equipo.tarjetas = [
      ...response.tarjetasAmarillas,
      ...response.tarjetasRojas,
      ...response.tarjetasAzules,
    ];

    equipo.faltas = response.faltas || [];
    equipo.alineacion = response.alineacion || [];
    equipo.sustituciones = response.sustituciones || [];
    equipo.golEnContra = response.golesContra || [];
    equipo.asistencias = response.jugadasPartido || [];
    equipo.nombre = response.nombre || equipo.nombre;

    //console.log("mapResponseToTeam() jugadoresEnCampeonato", equipoId, this.gs.jugadoresEnCampeonato.get(equipoId))
    equipo.alineacion.forEach((element: any) => {
      const jugador = this.gs.jugadoresEnCampeonato.get(equipoId)?.find(
        (jugador) => jugador.id == element.jugadorId
      );
      //console.log("mapResponseToTeam() jugador", equipoId, jugador)
      if (jugador) {
        element.codCIP = jugador.codCIP;
        element.nombre = jugador.name;
      }
    });

    //console.log("mapResponseToTeam() equipo.alineacion", equipo.alineacion)
  }

  incrementScore(equipo: "equipo1" | "equipo2") {
    this.marcador[equipo]++;
    //this.updateGoles(equipo);
  }

  decrementScore(equipo: "equipo1" | "equipo2") {
    if (this.marcador[equipo] > 0) {
      this.marcador[equipo]--;
      this.updateGoles(equipo);
    }
  }

  updateGoles(equipo: "equipo1" | "equipo2") {
    const golesDiff = this.marcador[equipo] - this[equipo].goles.length;
    if (golesDiff > 0) {
      for (let i = 0; i < golesDiff; i++) {
        this[equipo].goles.push({ jugador: null }); // Añade un gol
      }
    } else {
      this[equipo].goles.splice(this[equipo].goles.length + golesDiff); // Remueve goles
    }
  }

  onStatusChange(newStatus: string) {
    this.selectedStatus = newStatus;
    //console.log("Estado del partido cambiado a:", newStatus);
  }

  onItemSelected(flag: number, equipoId: number) {
    console.log("onItemSelected");
    if (flag == 1) {
      this.selectedTeam1 = equipoId;
      // Actualiza las opciones del segundo select
      this.filteredTeams2 = this.allTeams.filter(team => team.id !== this.selectedTeam1);

      //Limpiar alineacion
      this.equipo1.alineacion = []
    } else {
      this.selectedTeam2 = equipoId;
      // Actualiza las opciones del primer select
      this.filteredTeams1 = this.allTeams.filter(team => team.id !== this.selectedTeam2);

      //Limpiar alineacion
      this.equipo2.alineacion = []
    }

    //Actualizar jugadores en partido
    if (this.selectedTeam1)
      this.allPlayers = this.gs.jugadoresEnCampeonato.get(this.selectedTeam1) || []
  }

  @Output() onGuardarExitoso: EventEmitter<boolean> = new EventEmitter(); // Evento emitir, parte del actualizar y guardar del boton

  guardar() {
    //Por mientras un condicional para evitar equipo repetidos
    if (this.selectedTeam1 === undefined || this.selectedTeam2 === undefined) {
      console.error("Ambos equipos deben ser seleccionados.");
      return;
    }

    if (this.selectedTeam1 === this.selectedTeam2) {
      console.error("No puedes seleccionar el mismo equipo para ambos.");
      return;
    }

    this.gs.partidoFutbolDTO.equipo1Id = this.selectedTeam1;
    this.gs.partidoFutbolDTO.equipo2Id = this.selectedTeam2;

    const partido: PartidoDTO = {
      id: this.gs.partidoId,
      campeonatoId: this.campeonato.id,
      equipo1Id: this.gs.partidoFutbolDTO.equipo1Id,
      equipo2Id: this.gs.partidoFutbolDTO.equipo2Id,
      fecha: this.gs.partidoFutbolDTO.fecha,
      fase: this.gs.partidoFutbolDTO.fase,
      tipoResultado: this.selectedStatus,
      marcadorEquipo1: this.marcador.equipo1,
      marcadorEquipo2: this.marcador.equipo2,
      mejorJugadorId: this.mejorJugadorId,
      mejorPorteroId: this.mejorPorteroId,
      grupo: ''
    };

    //console.log("Guardando partido:", partido);

    this.partidoService.savePartido(1, partido)
      .pipe(take(1))
      .subscribe(data => {
        this.partidoService.getPartidosByCampeonato(this.campeonato.id)
          .pipe(take(1))
          .subscribe(data => {
            this.gs.updatePartidos(data)
          })
      })

    const detallePartido = {
      goles: this.getNuevosGoles(this.equipo1.goles, this.equipo2.goles),
      tarjetasAmarillas: this.getNuevaTarjetaAmarilla(
        this.equipo1.tarjetas,
        this.equipo2.tarjetas
      ),
      tarjetasRojas: this.getNuevaTarjetaRoja(
        this.equipo1.tarjetas,
        this.equipo2.tarjetas
      ),
      tarjetasAzules: this.getNuevaTarjetaAzul(
        this.equipo1.tarjetas,
        this.equipo2.tarjetas
      ),
      faltas: this.getNuevasFaltas(this.equipo1.faltas, this.equipo2.faltas),
      alineacion: this.getNuevaAlineacion(
        this.equipo1.alineacion,
        this.equipo2.alineacion
      ),
      sustituciones: this.getNuevaSustitucion(
        this.equipo1.sustituciones,
        this.equipo2.sustituciones
      ),
      golesContra: this.getNuevoGolEnContra(
        this.equipo1.golEnContra,
        this.equipo2.golEnContra
      ),
      portero: [],
      jugadasPartido: this.getNuevaAsistencia(
        this.equipo1.asistencias,
        this.equipo2.asistencias
      ),
    };

    //console.log("Guardando detalles del partido:", detallePartido);

    const eliminarDetallePartido = {
      goles: this.getNuevosGoles(
        this.partidoEquipo1DetDel.goles,
        this.partidoEquipo2DetDel.goles
      ),
      tarjetasAmarillas: this.getNuevaTarjetaAmarilla(
        this.partidoEquipo1DetDel.tarjetas,
        this.partidoEquipo2DetDel.tarjetas
      ),
      tarjetasRojas: this.getNuevaTarjetaRoja(
        this.partidoEquipo1DetDel.tarjetas,
        this.partidoEquipo2DetDel.tarjetas
      ),
      tarjetasAzules: this.getNuevaTarjetaAzul(
        this.partidoEquipo1DetDel.tarjetas,
        this.partidoEquipo2DetDel.tarjetas
      ),
      faltas: this.getNuevasFaltas(
        this.partidoEquipo1DetDel.faltas,
        this.partidoEquipo2DetDel.faltas
      ),
      alineacion: this.getNuevaAlineacion(
        this.partidoEquipo1DetDel.alineacion,
        this.partidoEquipo2DetDel.alineacion
      ),
      sustituciones: this.getNuevaSustitucion(
        this.partidoEquipo1DetDel.sustituciones,
        this.partidoEquipo2DetDel.sustituciones
      ),
      golesContra: this.getNuevoGolEnContra(
        this.partidoEquipo1DetDel.golEnContra,
        this.partidoEquipo2DetDel.golEnContra
      ),
      jugadasPartido: this.getNuevaAsistencia(
        this.partidoEquipo1DetDel.asistencias,
        this.partidoEquipo2DetDel.asistencias
      ),
    };

    //console.log("eliminarDetPar", eliminarDetallePartido)

    combineLatest([
      this.partidoService.deletePartidoDet(eliminarDetallePartido),
      this.partidoService.savePartidoDet(detallePartido),
      this.partidoService.savePartido(1, partido)
    ])
      .pipe(takeUntil(this.destroy$))
      .subscribe(([deletePartidoData, savePartidoDet, savePartido]) => {
        this.onGuardarExitoso.emit()
        this.inicializarEliminarDetalles()
        //console.log("emitUpdatePartidos()")
        //this.gs.emitUpdatePartidos()
        //this.gs.updatePartidos(savePartido)
      })
  }

  getNuevaAlineacion(alineacion1: any[], alineacion2: any[]) {
    const nuevaAlineacion = [
      ...alineacion1.map((jugador: any) => ({
        ...jugador,
      })),
      ...alineacion2.map((jugador: any) => ({
        ...jugador,
      })),
    ];

    //console.log("Nueva Alineacion", nuevaAlineacion);

    return nuevaAlineacion;
  }

  getNuevosGoles(gol1: any[], gol2: any[]) {
    const nuevosGoles = [
      ...gol1.map((gol: any) => ({
        ...gol,
      })),
      ...gol2.map((gol: any) => ({
        ...gol,
      })),
    ];
    //console.log("Nuevos goles", nuevosGoles);

    return nuevosGoles;
  }

  getNuevasFaltas(faltas1: any[], faltas2: any[]) {
    const nuevasFaltas = [
      ...faltas1.map((falta: any) => ({
        ...falta,
        equipoId: this.equipo1Id,
      })),
      ...faltas2.map((falta: any) => ({
        ...falta,
        equipoId: this.equipo2Id,
      })),
    ];
    //console.log("Nuevas faltas:", nuevasFaltas);

    return nuevasFaltas;
  }

  getNuevoGolEnContra(golEnContra1: any[], golEnContra2: any[]) {
    const nuevosGolesEnContra = [
      ...golEnContra1.map((golesEnContra: any) => ({
        ...golesEnContra,
        equipoId: this.equipo1Id,
      })),
      ...golEnContra2.map((golesEnContra: any) => ({
        ...golesEnContra,
        equipoId: this.equipo2Id,
      })),
    ];
    //console.log("Nuevos goles en contra", nuevosGolesEnContra);

    return nuevosGolesEnContra;
  }

  getNuevaSustitucion(sustituciones1: any[], sustituciones2: any[]) {
    const nuevaSustitucion = [
      ...sustituciones1.map((sustitucion: any) => ({
        ...sustitucion,
        equipoId: this.equipo1Id,
      })),
      ...sustituciones2.map((sustitucion: any) => ({
        ...sustitucion,
        equipoId: this.equipo2Id,
      })),
    ];
    //console.log("Nueva Sustitucion", nuevaSustitucion);

    return nuevaSustitucion;
  }

  getNuevaTarjetaAmarilla(tarjetas1: any[], tarjetas2: any[]) {
    const nuevaTarjetaAmarilla = [
      ...tarjetas1
        .filter((tarjeta: any) => tarjeta.color === 1)
        .map((tarjeta: any) => ({
          ...tarjeta,
          equipoId: this.equipo1Id,
        })),
      ...tarjetas2
        .filter((tarjeta: any) => tarjeta.color === 1)
        .map((tarjeta: any) => ({
          ...tarjeta,
          equipoId: this.equipo2Id,
        })),
    ];

    //console.log("Nueva Tarjeta Amarilla", nuevaTarjetaAmarilla);

    return nuevaTarjetaAmarilla;
  }
  getNuevaTarjetaRoja(tarjetas1: any[], tarjetas2: any[]) {
    const nuevaTarjetaRoja = [
      ...tarjetas1
        .filter((tarjeta: any) => tarjeta.color === 2)
        .map((tarjeta: any) => ({
          ...tarjeta,
          equipoId: this.equipo1Id,
        })),
      ...tarjetas2
        .filter((tarjeta: any) => tarjeta.color === 2)
        .map((tarjeta: any) => ({
          ...tarjeta,
          equipoId: this.equipo2Id,
        })),
    ];
    //console.log("Nueva Tarjeta Roja", nuevaTarjetaRoja);

    return nuevaTarjetaRoja;
  }
  getNuevaTarjetaAzul(tarjetas1: any[], tarjetas2: any[]) {
    const nuevaTarjetaAzul = [
      ...tarjetas1
        .filter((tarjeta: any) => tarjeta.color === 3)
        .map((tarjeta: any) => ({
          ...tarjeta,
          equipoId: this.equipo1Id,
        })),
      ...tarjetas2
        .filter((tarjeta: any) => tarjeta.color === 3)
        .map((tarjeta: any) => ({
          ...tarjeta,
          equipoId: this.equipo2Id,
        })),
    ];
    //console.log("Nueva Tarjeta Azul", nuevaTarjetaAzul);

    return nuevaTarjetaAzul;
  }

  getNuevaAsistencia(asistencia1: any[], asistencia2: any[]) {
    const nuevasAsistencias = [
      ...asistencia1.map((asistencias: any) => ({
        ...asistencias,
        equipoId: this.equipo1Id,
      })),
      ...asistencia2.map((asistencias: any) => ({
        ...asistencias,
        equipoId: this.equipo2Id,
      })),
    ];
    //console.log("Nuevas aistencias", nuevasAsistencias);
    return nuevasAsistencias;
  }

  handleCambiarPuntaje(equipoId: number) {
    //console.log("handleCambiarPuntaje", equipoId)
    if (equipoId == this.equipo1Id){
      //console.log("handleCambiarPuntaje", this.equipo1.alineacion)
      this.marcador.equipo1 = this.equipo1.goles.length
    }
    else{
      //console.log("handleCambiarPuntaje", this.equipo2.alineacion)
      this.marcador.equipo2 = this.equipo2.goles.length
    }
  }

  handleDisminuirPuntaje(equipoId: number) {
    if (equipoId == this.equipo1Id) this.decrementScore("equipo1");
    else this.decrementScore("equipo2");
  }

  //Actualiza el marcador contrario cuando es gol en contra:
  handleCambiarPuntajeEnContra(equipoId: number) {
    if (equipoId == this.equipo2Id) this.incrementScore("equipo1");
    else this.incrementScore("equipo2");
  }

  handleDisminuirPuntajeEnContra(equipoId: number) {
    if (equipoId == this.equipo2Id) this.decrementScore("equipo1");
    else this.decrementScore("equipo2");
  }

  eliminarPartido() {
    //console.log(`Inicio eliminacion de partido ID=${this.gs.partidoId}`);
    /*this.partidoService.deletePartido(this.gs.partidoId).subscribe((data) => {
      this.onGuardarExitoso.emit();
      this.gs.emitUpdatePartidos();
    });*/

    this.partidoService.deletePartido(this.gs.partidoId)
      .pipe(take(1))
      .subscribe(data => {
        this.partidoService.getPartidosByCampeonato(this.campeonato.id)
          .pipe(take(1))
          .subscribe(data => {
            this.gs.updatePartidos(data)
            this.onGuardarExitoso.emit();
          })
        //this.onEliminar.next()
      })
  }

  inicializarEliminarDetalles() {
    this.partidoEquipo1DetDel = {
      goles: [],
      tarjetas: [],
      faltas: [],
      alineacion: [],
      sustituciones: [],
      golEnContra: [],
      asistencias: [], //asistencia
    };

    this.partidoEquipo2DetDel = {
      goles: [],
      tarjetas: [],
      faltas: [],
      alineacion: [],
      sustituciones: [],
      golEnContra: [],
      asistencias: [],
    };
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  getFaseValue(phase: string): number {
    switch (phase) {
      case 'Octavos': return this.gs.Fase.OCTAVOS;
      case 'Cuartos': return this.gs.Fase.CUARTOS;
      case 'Semifinal': return this.gs.Fase.SEMIFINAL;
      case 'Final': return this.gs.Fase.FINAL;
      default: return -1; // O algún valor por defecto
    }
  }
}
