import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-confirmation-popup',
  templateUrl: './confirmation-popup.component.html',
  styleUrls: ['./confirmation-popup.component.css']
})
export class ConfirmationPopupComponent {
  @Input() message: string = 'Are you sure you want to proceed?';
  @Output() confirmAction: EventEmitter<void> = new EventEmitter<void>();
  @Output() cancelAction: EventEmitter<void> = new EventEmitter<void>();
  visible: boolean = false;

  show(): void {
    this.visible = true;
  }

  hide(): void {
    this.visible = false;
  }

  confirm(): void {
    this.confirmAction.emit();
    this.hide();
  }

  cancel(): void {
    this.cancelAction.emit();
    this.hide();
  }
}
