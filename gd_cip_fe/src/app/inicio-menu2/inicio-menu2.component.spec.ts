import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InicioMenu2Component } from './inicio-menu2.component';

describe('InicioMenu2Component', () => {
  let component: InicioMenu2Component;
  let fixture: ComponentFixture<InicioMenu2Component>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [InicioMenu2Component]
    });
    fixture = TestBed.createComponent(InicioMenu2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
