import { Component, Input, OnInit } from '@angular/core';
import { Equipo, EquipoService } from '../services/equipo.service';
import { ListaCampeonatosService } from '../services/lista-campeonatos.service';
import { Campeonato, CampeonatoService } from '../services/campeonato.service';
import { CampeonatosSeguidosService, CampeonatosSeguidos } from '../services/campeonatos-seguidos.service';
import { EquipoCampeonato, EquipoCampeonatoService } from '../services/equipo-campeonato.service';
import { map, Observable, Subject, takeUntil } from 'rxjs';
import { GlobalService } from '../services/global.service';
import { ReturnStatement } from '@angular/compiler';

export class Comentario {
  constructor(
    public nombre: string,
    public textoComentario: string,
    public fecha: string
  ) { }
}

@Component({
  selector: 'app-inicio-menu2',
  templateUrl: './inicio-menu2.component.html',
  styleUrls: ['./inicio-menu2.component.css']
})
export class InicioMenu2Component implements OnInit {

  Grupos = [
    { label: 'Grupo A', value: 1 },
    { label: 'Grupo B', value: 2 }
  ];

  campeonato$!: Observable<Campeonato>;
  idCampeonato = -1;
  @Input() campeonato!: Campeonato;

  equipos: any[] = [] //Array [0]
  equiposEnGrupos: any[] = []
  campeonatosSeguidos: CampeonatosSeguidos[] = [];

  
  estructura2: any[] = []//Array de [1]
  arrayCombinado: any[] = []; //Guarda el de partido {equipos[] , estructura2[]}


  equiposEnCampeonato$ = this.gs.equiposEnCampeonato$

  private destroy$ = new Subject<void>()

  numeroSeguidores = "125";
  privacidadTorneo = "Publico";

  constructor(
    private equipoService: EquipoService,
    private campeonatoService: CampeonatoService,
    private equipoCampeonatoService: EquipoCampeonatoService,
    private CampeonatosSeguidosService: CampeonatosSeguidosService,
    private gs: GlobalService,
  ) {
  }

  ngOnInit(): void {
    this.loadEquiposEnCampeonato();
    this.loadCampeonatosSeguidos();
  }

  loadCampeonatosSeguidos(): void {
    this.CampeonatosSeguidosService.getCampeonatosSeguidos().subscribe(campeonatosSeguidos => {
      console.log(campeonatosSeguidos);
      this.campeonatosSeguidos = campeonatosSeguidos;
    });
  }

  loadEquiposEnCampeonato() {
    this.equiposEnCampeonato$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        this.equipos = data;
        this.equiposEnGrupos = this.getEquiposEnGrupos(5)
        console.log('estructura 1:', this.equipos);
  
        // Cargar datos de estructura2 después de equipos
        this.equipoCampeonatoService.findByCampeonatoId(this.campeonato.id).subscribe(datos => {
          this.estructura2 = datos;
          console.log('estructura 2:', this.estructura2);
  
          // Combinar ambas estructuras
          this.arrayCombinado = this.equipos.map(equipo => {
            const detalleEstructura2 = this.estructura2.find(
              item => item.equipoId === equipo.id
            ) || {}; // Por si no se encuentra un equivalente en estructura2
            
            return [equipo, detalleEstructura2];
          });
  
          console.log('Equipos en Grupos (combinados):', this.arrayCombinado);
        });
      });
  }
  

  // Función para dividir los equipos en grupos
  getEquiposEnGrupos(tamañoGrupo: number) {
    console.log("getEquiposEnGrupos", this.equipos)
    let grupos = [];
    if (!this.equipos)
      return []
    for (let i = 0; i < this.equipos.length; i += tamañoGrupo) {
      grupos.push(this.equipos.slice(i, i + tamañoGrupo));
    }
    return grupos;
  }

  comentarios: Comentario[] = [
    new Comentario('Usuario 1', 'Este es un gran producto.', "17/06/2023"),
    new Comentario('Usuario 2', 'Me encanta su servicio al cliente.', "17/06/2023"),
  ];



  //Añade el nombre del grupo
  getGrupoNombre(grupoId: number | null): string {
    const grupo = this.Grupos.find(g => g.value === grupoId);
    return grupo ? grupo.label : 'Equipo sin grupo';
  }

  // Función para manejar el cambio en ng-select
  onGrupoChange(estructura2: any) {
    this.equipoCampeonatoService.updateEquipoCampeonato(estructura2).subscribe({
      next: (actualizado) => console.log(`Equipo actualizado: ${actualizado.equipoId} con grupoId ${actualizado.grupoId}`),
      error: (error) => console.error('Error al actualizar el equipo', error),
    });
  }
  
  ngOnDestroy(){
    this.destroy$.next()
    this.destroy$.complete()
  }

  isCampeonatoFav(campeonato: Campeonato): boolean {
    const exists = this.campeonatosSeguidos.some(campSeg => campSeg.campeonatoId === campeonato.id);
    return exists
  }

  toggleFavCamp(campeonato: Campeonato) {    //funcion favoritos camp
    
    const newCampeonatoSeguido: CampeonatosSeguidos = { campeonatoId: campeonato.id, usuarioCipId: Number(sessionStorage.getItem("usuarioCipId"))};
    console.log('Creando nuevo campeonato fav con datos:', newCampeonatoSeguido);

    if(this.isCampeonatoFav(campeonato)){
      const currentCampFav = this.campeonatosSeguidos.find(campSeg => campSeg.campeonatoId === campeonato.id)
      
      this.CampeonatosSeguidosService.deleteById(currentCampFav?.id).subscribe(() => {
        console.log('Campeonato eliminado con éxito',currentCampFav);
        this.loadCampeonatosSeguidos();
      })
    } else {
      this.CampeonatosSeguidosService.save(newCampeonatoSeguido).subscribe(() => {
        this.loadCampeonatosSeguidos();
        console.log('Campeonato fav agregado');
    })
    }
  }
}
