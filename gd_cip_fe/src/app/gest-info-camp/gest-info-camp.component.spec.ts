import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestInfoCampComponent } from './gest-info-camp.component';

describe('GestInfoCampComponent', () => {
  let component: GestInfoCampComponent;
  let fixture: ComponentFixture<GestInfoCampComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GestInfoCampComponent]
    });
    fixture = TestBed.createComponent(GestInfoCampComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
