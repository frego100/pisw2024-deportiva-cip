import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-gest-info-camp',
  templateUrl: './gest-info-camp.component.html',
  styleUrls: ['./gest-info-camp.component.css']
})
export class GestInfoCampComponent {
  constructor(
    public dialogRef: MatDialogRef<GestInfoCampComponent>,
    @Inject (MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {
    this.dialogRef.close(this.data.state);
  }
}
