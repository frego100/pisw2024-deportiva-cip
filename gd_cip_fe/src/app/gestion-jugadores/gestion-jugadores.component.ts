import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PlayerService } from '../services/player.service';
import { EquipoService, Equipo } from '../services/equipo.service';
import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { MatPaginator } from '@angular/material/paginator';  // Importar MatPaginator
import { MatTableDataSource } from '@angular/material/table';  // Importar MatTableDataSource
import * as XLSX from 'xlsx';
import { UsuarioService, UsuarioCIP } from '../services/usuario.service';//NUEVO
import { FileUploadServiceService } from '../services/file-upload-service.service';//NUEVO
import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RoleService } from '../services/role.service';

// Validador personalizado para el documento
export function validateDocument(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const value = control.value;
    if (value && (!/^\d{8}$/.test(value))) { // Valida que tenga exactamente 8 dígitos
      return { invalidDocument: true }; // Clave personalizada para el error
    }
    return null; // No hay error
  };
}

interface Player {
  id?: number;
  name: string;
  shortName: string; // BORRAR ATRIBUTO
  codCIP: number;
  position: string;
  number: string; // BORRAR ATRIBUTO
  codFam: string;
  document: string;
  birthDate: Date;
  phone: string;
  email: string;  // Añadir email
  equipoID: number;  // Añadir equipoID
}

@Component({
  selector: 'app-gestion-jugadores',
  templateUrl: './gestion-jugadores.component.html',
  styleUrls: ['./gestion-jugadores.component.css'],
})

export class GestionJugadoresComponent implements OnInit {
  @ViewChild('playerDialog') playerDialog!: TemplateRef<any>;
  //Confirmación de guardado
  @ViewChild('confirmationPopup') confirmationPopup!: ConfirmationPopupComponent;
  @ViewChild('confirmationSuccess') confirmationSuccess!: ConfirmationComponent;

  //Confirmación de eliminación
  @ViewChild('confirmationDelete') confirmationDelete!: ConfirmationPopupComponent;
  @ViewChild('confirmationDeleteSuccess') confirmationDeleteSuccess!: ConfirmationComponent;
  @ViewChild(MatPaginator) paginator!: MatPaginator;  // Referencia al paginador
  @ViewChild('fileUploadDialog') fileUploadDialog!: TemplateRef<any>;
  csvContent: string = '';
  selectedFile: File | null = null;
  isLoading = false; // Indicador de carga
  dataSource = new MatTableDataSource<Player>([]);

  posiciones = [
    { label: 'Delantero', value: 'delantero' },
    { label: 'Centrocampista', value: 'centrocampista' },
    { label: 'Defensa', value: 'defensa' },
    { label: 'Portero', value: 'portero' }
  ];
  // Lista de capítulos
  capitulos = [
    { label: 'AGRONOMICA', value: 'AGRONOMICA' },
    { label: 'ALIMENTARIA', value: 'ALIMENTARIA' },
    { label: 'AMBIENTAL', value: 'AMBIENTAL' },
    { label: 'CIVIL', value: 'CIVIL' },
    { label: 'ELECTRONICA', value: 'ELECTRONICA' },
    { label: 'GEOLOGICA GEOFISICA Y MINAS', value: 'GEOLOGICA GEOFISICA Y MINAS' },
    { label: 'INDUSTRIAL', value: 'INDUSTRIAL' },
    { label: 'MATERIALES', value: 'MATERIALES' },
    { label: 'MECANICA ELECTRICA', value: 'MECANICA ELECTRICA' },
    { label: 'METALURGICA', value: 'METALURGICA' },
    { label: 'MINAS', value: 'MINAS' },
    { label: 'SISTEMAS E INFORMATICA', value: 'SISTEMAS E INFORMATICA' },
    { label: 'S/CAPITULO', value: 'S/CAPITULO' },
    { label: 'PESQUERA', value: 'PESQUERA' },
    { label: 'QUIMICA', value: 'QUIMICA' },
  ];

  selectedPlayer: any;

  esAdministrador: boolean = false;

  players: Player[] = [];
  equipos: Equipo[] = [];

  filteredPlayers: Player[] = [];
  searchTerm: string = '';

  selectedTeam: number | null = null;  // Nuevo: para manejar la selección de equipo

  playerForm: FormGroup;
  isEditing: boolean = false;
  currentPlayerId?: number;

  progress: number = 0;

  constructor(
    private fb: FormBuilder,
    private dialog: MatDialog,
    private playerService: PlayerService,
    private equipoService: EquipoService,
    private usuarioService: UsuarioService,
    private fileUpload: FileUploadServiceService,
    private snackBar: MatSnackBar,
    private roleService: RoleService,
  ) {
    this.playerForm = this.fb.group({
      name: ['', Validators.required],
      shortName: [''], // BORRAR
      codCIP: ['', Validators.required],
      position: [''],
      number: [''], // BORRAR
      codFam: [''],
      document: ['', validateDocument()],
      birthDate: [''],
      phone: [''],
      capitulo: [''],
      email: ['', [Validators.required, Validators.email]],  // Añadir email al formulario
      equipoID: [null, Validators.required]  // Añadir equipoID al formulario

    });

  }

  ngOnInit(): void {
    this.selectedTeam = null;  // Inicializar con 'Todos' seleccionado
    this.loadPlayers();
    this.loadEquipos();
    this.esAdministrador = this.roleService.hasRole('Administrador');
    console.log("ADMIN"+ this.esAdministrador);
    if(!this.esAdministrador){
      this.playerForm.disable();
    }
    
    
  }

  isDocumentInvalid(): boolean {
    const control = this.playerForm.get('document');
    return !!(control?.hasError('invalidDocument') && control.touched);
  }

 // Método para manejar selección de archivo
 onFileSelected(event: any): void {
  const file = event.target.files[0];
  if (file) {
    this.selectedFile = file;
  }
}
// Función que se ejecuta cuando se presiona "Subir archivo"
uploadFile(): void {
  if (this.selectedFile) {
    this.readExcelFile(this.selectedFile);
  }
  if (this.selectedFile) {
    this.fileUpload.uploadFile(this.selectedFile).subscribe({
      next: (response: any) => {  // Acepta cualquier tipo de respuesta
        if (typeof response === 'string') {
          // Si es un string, lo mostramos directamente
          console.log('Archivo subido correctamente:', response);
          alert(response);  // Muestra el mensaje de éxito o error del backend
        } else {
          // Si es otro tipo de respuesta (como un objeto o algún tipo distinto)
          console.log('Respuesta recibida:', response);
          alert('Archivo subido exitosamente.');
        }
      },
      error: (err) => {
        console.error('Error al subir el archivo:', err);
        alert('Error al subir el archivo.');
      },
    });
  } else {
    alert('No se ha seleccionado ningún archivo para subir.');
  }
}
  // Leer el archivo Excel y procesarlo en bloques
readExcelFile(file: File): void {
  const reader = new FileReader();
  reader.onload = (e: ProgressEvent<FileReader>) => {
    const data = new Uint8Array(e.target?.result as ArrayBuffer);
    const workbook = XLSX.read(data, { type: 'array' });
    const sheetName = workbook.SheetNames[0];
    const worksheet = workbook.Sheets[sheetName];
    const rows = XLSX.utils.sheet_to_json<any[]>(worksheet, { header: 1 }) as any[][];

    console.log('Contenido Excel procesado:', rows);
    this.processExcelDataInChunks(rows); // Procesar en bloques
  };
  reader.readAsArrayBuffer(file);
}
// Procesar los datos del Excel en bloques para evitar sobrecargar la memoria
processExcelDataInChunks(rows: any[][]): void {
  const chunkSize = 1000; // Procesar en bloques de 1000 filas
  let startIndex = 1; // Comenzamos en la fila 1 (ya que la fila 0 es para los encabezados)
  const usuarios: UsuarioCIP[] = [];
  // Mostrar el spinner y comenzar el progreso
  this.isLoading = true;
  this.progress = 0;

  const processChunk = () => {
    const chunk = rows.slice(startIndex, startIndex + chunkSize);
    chunk.forEach((row) => {
      const cip = row[0]?.toString().trim();
      const colegiado = row[1]?.toString().trim() || '';
      const dni = row[3]?.toString().trim()|| '';
      const capitulo = row[4]?.toString().trim()|| '';

      if (cip) {
        usuarios.push({
          id: 0,
          cip,
          nombre: colegiado,
          dni,
          capitulo,
        });
      }
    });

    startIndex += chunkSize;
    this.progress = Math.round((startIndex / rows.length) * 100);

    // Si hay más filas para procesar, continuar con el siguiente bloque
    if (startIndex < rows.length) {
      setTimeout(processChunk, 0); // Para evitar bloquear el hilo principal
    } else {
      this.createUsuarios(usuarios); // Crear usuarios cuando termine de procesar todos los bloques
      
    }
  };

  // Iniciar el procesamiento de bloques
  processChunk();
}


  // Este método se ejecuta cuando el codCIP cambia
  onCodCIPChange() {
    const codCIP = this.playerForm.get('codCIP')?.value;

    if (codCIP) {
      // Llamamos al servicio para obtener los datos del usuario según el CIP
      this.usuarioService.findByCip(codCIP).subscribe(
        (usuario: UsuarioCIP) => {
          if (usuario) {
            // Si encontramos un usuario con ese CIP, autocompletamos el nombre
            this.playerForm.get('name')?.setValue(usuario.nombre);
            this.playerForm.get('document')?.setValue(usuario.dni); // Autocompleta el DNI
            this.playerForm.get('capitulo')?.setValue(usuario.capitulo); // Autocompleta el capítulo
          }
        },
        (error) => {
          console.error('Error al obtener el usuario CIP:', error);
        }
      );
    }
  }

  // Llamamos a onCodCIPChange cuando el codCIP cambia
  onCodCIPInput() {
    this.onCodCIPChange();
  }


  // Crear usuarios CIP

    async createUsuarios(usuarios: UsuarioCIP[]): Promise<void> {
      const chunkSize = 100; // Tamaño del bloque de usuarios a procesar
      for (let i = 0; i < usuarios.length; i += chunkSize) {
        const chunk = usuarios.slice(i, i + chunkSize);
    
        try {
          // Procesar cada bloque de usuarios
          await Promise.all(
            chunk.map((usuario) =>
              this.usuarioService.save(usuario).toPromise()
            )
          );
          console.log(`Bloque ${i / chunkSize + 1} procesado correctamente.`);
        } catch (error) {
          console.error('Error al procesar un bloque de usuarios:', error);
        }
    
        // Actualizar progreso
        this.progress = Math.round(((i + chunkSize) / usuarios.length) * 100);
        console.log(`Progreso: ${this.progress}%`);
    
        // Retrasar entre bloques para no saturar el backend
        await new Promise((resolve) => setTimeout(resolve, 500));
      }
      this.dialog.closeAll();
      // Mostrar mensaje al finalizar
      this.snackBar.open('Todos los usuarios han sido creados.', '', {
        duration: 3000,
        horizontalPosition: 'center',
        verticalPosition: 'top',
      });
    
      // Detener el spinner
      this.isLoading = false;
    }
    

  createPlayers(players: Player[]): void {
    console.log('Jugadores listos para enviar:', players);

    players.forEach(player => {
      console.log('Enviando jugador al servicio:', player);
      this.playerService.addPlayer(player).subscribe({
        next: (response) => console.log('Jugador creado:', response),
        error: (error) => console.error('Error al crear jugador:', error),
      });
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;  // Asigna el paginador después de que la vista ha sido inicializada
  }


  loadPlayers(): void {
    this.playerService.getPlayers().subscribe(players => {
      console.log(players);
      this.players = players;
      //this.players = players;
      this.dataSource.data = players;
      this.filterPlayers();
    });
  }

  openFileUploadDialog(): void {
    this.dialog.open(this.fileUploadDialog, {
      width: '500px', // Opcional: puedes ajustar el tamaño del diálogo
    });
  }
  loadEquipos(): void {
    this.equipoService.getEquipos().subscribe(equipos => {
      console.log(equipos);
      this.equipos = equipos;
      // Ordenar alfabéticamente por el nombre del equipo
      this.equipos.sort((a, b) => a.nombre.localeCompare(b.nombre));

    });
  }

  openAddPlayerDialog(): void {
    this.isEditing = false;
    this.playerForm.reset();
    this.dialog.open(this.playerDialog);
  }

  openEditPlayerDialog(player: Player): void {
    this.isEditing = true;
    this.currentPlayerId = player.id;
    this.playerForm.patchValue(player);
    this.dialog.open(this.playerDialog);
  }

  showDuplicatePlayerError(existingPlayer: Player): void {
    alert(`El jugador con el código CIP ${existingPlayer.codCIP} y nombre ${existingPlayer.name} ya está registrado.`);
  }


  savePlayer(): void {
    const newPlayer = this.playerForm.value;

    // Verifica si el codCIP ya existe en otro jugador (excepto el que se está editando)
  const existingPlayer = this.players.find(player =>
    player.codCIP === newPlayer.codCIP && player.id !== this.currentPlayerId
  );
    if (existingPlayer) {
      // Muestra un mensaje de error
      this.showDuplicatePlayerError(existingPlayer);
      return;
    }


    if (this.playerForm.invalid) {
      this.playerForm.markAllAsTouched();
      return;
    }
    this.confirmationPopup.show();
  }


  onConfirmSave(): void {
    if (this.isEditing && this.currentPlayerId !== undefined) {
      const updatedPlayer: Player = { id: this.currentPlayerId, ...this.playerForm.value };
      this.playerService.updatePlayer(updatedPlayer).subscribe(() => {
        this.loadPlayers();
        this.dialog.closeAll();
        this.confirmationSuccess.show();
      });
    } else {
      const newPlayer: Player = { ...this.playerForm.value };
      this.playerService.addPlayer(newPlayer).subscribe(() => {
        this.loadPlayers();
        this.dialog.closeAll();
        this.confirmationSuccess.show();
      });
    }
  }

  onCancelSave(): void {
    console.log('Cancelado');
    this.dialog.closeAll(); // Cierra todos los diálogos abiertos
    this.playerForm.reset(); // Restablece el formulario
    this.isEditing = false; // Resetea el estado de edición

  }

  deletePlayer(): void {
    if (this.isEditing && this.currentPlayerId !== undefined) {
      this.confirmationDelete.show();  // Muestra el popup de confirmación para eliminar
    }
  }

  onConfirmDelete(): void {
    if (this.isEditing && this.currentPlayerId !== undefined) {
      this.playerService.deletePlayer(this.currentPlayerId).subscribe(() => {
        this.loadPlayers();
        this.dialog.closeAll();
        this.confirmationDeleteSuccess.show();  // Muestra popup de éxito después de eliminar
      });
    }
  }

  onCancelDelete(): void {
    console.log('cancelado');
  }

  closeDialog(): void {
    this.selectedFile = null;
    this.dialog.closeAll();
    this.isLoading = false;
  }

  filterPlayers(): void {
    const filteredPlayers = this.players.filter(player => {
      return (
        player.name.toLowerCase().includes(this.searchTerm.toLowerCase()) &&
        (this.selectedTeam ? player.equipoID === this.selectedTeam : true)
      );
    });
    this.dataSource.data = filteredPlayers;
    this.paginator.pageIndex = 0;  // Reinicia el índice de la página
  }

  filterByTeam(): void {
    if (this.selectedTeam === null) {
      this.dataSource.data = this.players;  // Mostrar todos los jugadores si no se filtra por equipo
    } else {

      this.dataSource.data = this.players.filter(player => player.equipoID === this.selectedTeam);
    }
  }
  

}
