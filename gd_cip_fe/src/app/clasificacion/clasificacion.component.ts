import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MenuCampeonatoComponent } from '../menu-campeonato/menu-campeonato.component';
import { ClasificacionService, ClasificacionData } from '../services/clasificacion.service';
import { Router } from '@angular/router';
import { GlobalService } from '../services/global.service';
import { Campeonato } from '../services/campeonato.service';
import { Equipo } from '../services/equipo.service';
import { combineLatest, filter, map, Observable, of, Subject, switchMap, take, takeUntil } from 'rxjs';
import { PartidoDTO } from '../services/partidos.service';
import { EquipoCampeonatoService } from '../services/equipo-campeonato.service';

@Component({
  selector: 'app-clasificacion',
  templateUrl: './clasificacion.component.html',
  styleUrls: ['./clasificacion.component.css']
})
export class ClasificacionComponent implements OnInit, OnDestroy {

  //Llama al service del campeonato
  @Input() campeonato!: Campeonato;
  @Input() partidos!: PartidoDTO;
  
  //Array de los datos de la tabla
  displayedColumns: string[] = ['position', 'team', 'pts', 'j', 'g', 'e', 'p', 'gf', 'gc', 'dif'];

  selectedPhase: string = '1';
  selectedCampeonatoId: number = -1;//para el filtrado por campeonato

  //Variables para clasificacion
  groups: { name: string, teams: any[] }[] = [];//por grupos
  allTeamsDataSource: any[] = [];//array de todos los equipos
  allTeamsDataSourceGroups: any[] = []; // para clasificación por grupos

   //Extraer los equipos que pasaron 
  topTeamsPerGroup: { groupName: string; teams: any[] }[] = [];
  equiposClasificados: any[] = []; // Array para almacenar los equipos clasificados
  equiposEnCampeonato: Equipo[] = [];
  estructura2: any [] = []; //Array del service EquipoCampeonato

  //Bandera para activar y desactivar el calculo de clasificacion, y la fase
  isClasificacionActiva: boolean = true;
  faseFinalizada: boolean = false;

  equipos!: Map<number, string> 
  destroy$ = new Subject<void>()

  constructor(
    public dialog: MatDialog, 
    private clasificacionService: ClasificacionService, 
    private router: Router,
    private equipoCampeonatoService: EquipoCampeonatoService,
    private gs: GlobalService) 
  {}

  ngOnInit(): void {
    console.log("clasificacionComponent.ngOnInit()")
    this.loadClasificacionDataNuevo()
  }

  ngOnDestroy(): void {
    console.log("ClasificacionComponent.onDetroy()")
    this.destroy$.next()
    this.destroy$.complete()
  }
  
 
  // Método para detener el cálculo de la tabla
  detenerCalculoClasificacion(): void {
    this.isClasificacionActiva = false; // Desactiva el cálculo
    console.log('Cálculo de clasificación detenido');
  }

   //Método para reactivar el cálculo 
   reactivarCalculoClasificacion(): void {
    this.isClasificacionActiva = true;
    console.log('Cálculo de clasificación activado');
    this.loadClasificacionDataNuevo(); // Recarga los datos al activar
  }


 // Carga inicial dividida
 loadClasificacionDataNuevo(): void {
  if (!this.isClasificacionActiva && this.faseFinalizada) {
    console.log('Cálculo de clasificación está desactivado.');
    return;
  }

  this.selectedCampeonatoId = this.campeonato.id;

  this.equipoCampeonatoService.findByCampeonatoId(this.campeonato.id).subscribe(datos => {
    this.estructura2 = datos;
    console.log('estructura 2:', this.estructura2);

    combineLatest([
      this.gs.equiposEnCampeonato$, 
      this.gs.partidosSubject$
    ])
      .pipe(take(1))
      .subscribe(([equiposEnCampeonatoData, partidos]) => {
        this.equiposEnCampeonato = equiposEnCampeonatoData;
        this.calculateClasificacion(equiposEnCampeonatoData, partidos);
      });

    // Escuchar actualizaciones del componente editar-partido al guardarlo.
    this.gs.partidosSubject$
      .pipe(takeUntil(this.destroy$))
      .subscribe(data => {
        console.log('Evento de actualización recibido.');

        if (this.faseFinalizada) {
          return;
        }

        combineLatest([
          this.gs.equiposEnCampeonato$, 
          this.gs.partidosSubject$
        ])
          .pipe(take(1))
          .subscribe(([equiposEnCampeonatoData, partidos]) => {
            this.equiposEnCampeonato = equiposEnCampeonatoData;
            this.calculateClasificacion(equiposEnCampeonatoData, partidos);
          });
      });
    });
}

calculateClasificacion(equiposEnCampeonato: Equipo[], partidos: PartidoDTO[]): void {

  if (!this.isClasificacionActiva && this.faseFinalizada) {
    console.log('Cálculo de clasificación desactivado y fase finalizada.');
    return;
  }


  // Crear un mapa para obtener el grupoId basado en equipoId desde estructura2
  const equipoGrupoMap = new Map<number, number>();
  this.estructura2.forEach(item => {
    equipoGrupoMap.set(item.equipoId, item.grupoId);
  });

  const equipoStats = new Map<number, any>();

  // Inicializa estadísticas
  equiposEnCampeonato.forEach(equipo => {
    equipoStats.set(equipo.id, {
      id: equipo.id,
      team: equipo.nombre,
      grupoId: equipoGrupoMap.get(equipo.id) || null,
      pts: 0,
      j: 0,
      g: 0,
      e: 0,
      p: 0,
      gf: 0,
      gc: 0,
      dif: 0,
    });
  });

  // Procesa partidos para calcular estadísticas
  partidos.filter(partido => partido.tipoResultado === 'REALIZADO' && (partido.fase === 1 || partido.fase === null) ).forEach(partido => {
    const equipo1 = equipoStats.get(partido.equipo1Id);
    const equipo2 = equipoStats.get(partido.equipo2Id);

    if (!equipo1 || !equipo2) return;

    equipo1.j++;
    equipo2.j++;

    equipo1.gf += partido.marcadorEquipo1;
    equipo1.gc += partido.marcadorEquipo2;
    equipo2.gf += partido.marcadorEquipo2;
    equipo2.gc += partido.marcadorEquipo1;

    equipo1.dif = equipo1.gf - equipo1.gc;
    equipo2.dif = equipo2.gf - equipo2.gc;

    if (partido.marcadorEquipo1 > partido.marcadorEquipo2) {
      equipo1.g++;
      equipo2.p++;
      equipo1.pts += 3;
    } else if (partido.marcadorEquipo1 < partido.marcadorEquipo2) {
      equipo2.g++;
      equipo1.p++;
      equipo2.pts += 3;
    } else {
      equipo1.e++;
      equipo2.e++;
      equipo1.pts++;
      equipo2.pts++;
    }
  });

  const teamData = Array.from(equipoStats.values());

  if(equiposEnCampeonato.length > 5){
    
    const groupA = teamData.filter(team => team.grupoId === 1);
    const groupB = teamData.filter(team => team.grupoId === 2);

    this.groups = [
      {
          name: 'Grupo A',
          teams: groupA.sort((a, b) => b.pts - a.pts || b.ds - a.ds || b.dp - a.dp)
      },
      {
          name: 'Grupo B',
          teams: groupB.sort((a, b) => b.pts - a.pts || b.ds - a.ds || b.dp - a.dp)
      }
    ];

    this.groups.forEach(group => {
        group.teams.forEach((team, index) => {
            team.position = index + 1;
        });
    });

     //Extrae el top de equipos segun la cantidad requerida
     combineLatest([
      this.gs.faseFinalizada$,
      of(this.groups)
    ])
    .pipe(
      takeUntil(this.destroy$)
    )
    .subscribe(([faseFinalizadaMap, gruposProcesados]) => {

      this.faseFinalizada = faseFinalizadaMap.get(this.selectedCampeonatoId) ?? false;
      //this.equiposClasificados = equiposClasificados;
      if (this.faseFinalizada) {
        console.log('Fase finalizada para campeonato:', this.selectedCampeonatoId);
        this.detenerCalculoClasificacion();
        
        // Procesar clasificación según los equipos clasificados
        if (gruposProcesados.length > 0) {
          if (equiposEnCampeonato.length > 5 && equiposEnCampeonato.length <= 9) {
            this.extraerEquiposSemifinales();
          } else if (equiposEnCampeonato.length > 9 && equiposEnCampeonato.length <= 20) {
            this.extraerEquiposCuartos();
          } else if (equiposEnCampeonato.length > 20) {
            this.extraerEquiposOctavos();
          }
        }else {
          console.warn('No se encontraron grupos procesados.');
        }
      }
    });

  } else{
      this.allTeamsDataSource = teamData.sort((a,b) => b.pts - a.pts || b.dif - a.dif || b.gf - a.gf);
      this.allTeamsDataSource.forEach((team,index)=>{
        team.position = index+1;
      });
  }

}

  
  extraerEquiposSemifinales(): void {
    this.topTeamsPerGroup = this.groups.map(group => ({
      groupName: group.name,
      teams: group.teams.slice(0, 2), // Extrae los 2 primeros equipos de cada grupo
    }));

    this.equiposClasificados = this.topTeamsPerGroup.flatMap(group => group.teams);
        if (this.equiposClasificados.length === 0) {
          console.warn('No se encontraron equipos clasificados para semifinales.');
      } else {
          console.log('Equipos clasificados semifinales', this.equiposClasificados);
      }

      this.gs.updateEquiposClasificados(this.selectedCampeonatoId, this.equiposClasificados);
    
  }
  
  extraerEquiposCuartos(): void {
    this.topTeamsPerGroup = this.groups.map(group => ({
      groupName: group.name,
      teams: group.teams.slice(0, 4), // Extrae los 4 primeros equipos de cada grupo
    }));
    this.equiposClasificados = this.topTeamsPerGroup.flatMap(group => group.teams);
    console.log('Equipos clasificados cuartos', this.equiposClasificados);

    //Actualiza el listado de equipos clasificados
    this.gs.updateEquiposClasificados(this.selectedCampeonatoId, this.equiposClasificados);
  }

  extraerEquiposOctavos(): void {
    this.topTeamsPerGroup = this.groups.map(group => ({
      groupName: group.name,
      teams: group.teams.slice(0, 8), // Extrae los 8 primeros equipos de cada grupo
    }));
    this.equiposClasificados = this.topTeamsPerGroup.flatMap(group => group.teams);
    console.log('Equipos clasificados octavos', this.equiposClasificados);

    //Actualiza el listado de equipos clasificados
    this.gs.updateEquiposClasificados(this.selectedCampeonatoId, this.equiposClasificados);
  }



  onPhaseChange(event: any): void {
    this.selectedPhase = event.target.value;

  }


  openMenuCampeonato(): void {
    this.dialog.open(MenuCampeonatoComponent);
  }

  //Esto todavia no es funcional, es de fase 2
  updatePhaseData(): void {
    if (this.selectedPhase === '2') {
       console.log('fase 2')
    }
  }
//Obtiene los datos para mostrar en Equipo-vista
  setTeamData(row: any): void {
    this.clasificacionService.setSelectedTeam({
      name: row.team,
      wins: row.g,
      draws: row.e,
      loses: row.p,
      id: row.id
    });
    this.router.navigate(['/equipo-vista']);//esto navegabilidad, debe arreglarse en front
  }

  //envia los datos al momento de seleccionar el equipo en la tabla clasificacion
  onRowClick(row: any): void {
    this.setTeamData(row);
  }

  isTeamClasificado(team: any): boolean {
    if (this.equiposEnCampeonato.length > 5 && this.equiposEnCampeonato.length <= 9) {
      const topTeams = this.groups.flatMap(group => group.teams.slice(0, 2)); 
      return topTeams.some(eq => eq.id === team.id);
    }else if(this.equiposEnCampeonato.length > 9 && this.equiposEnCampeonato.length <= 20){
      const topTeams = this.groups.flatMap(group => group.teams.slice(0, 4)); 
      return topTeams.some(eq => eq.id === team.id);
    }else if(this.equiposEnCampeonato.length > 20){
      const topTeams = this.groups.flatMap(group => group.teams.slice(0, 8)); 
      return topTeams.some(eq => eq.id === team.id);
    }
    return false;
  }
  
  
}

