import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-finales',
  templateUrl: './finales.component.html',
  styleUrls: ['./finales.component.css']
})
export class FinalesComponent {
  @Input() equipos: any[] = [];

  ngOnInit() {
    console.log('Equipos en finales:', this.equipos);
  }
}
