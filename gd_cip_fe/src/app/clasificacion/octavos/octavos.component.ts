import { Component, Input } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-octavos',
  templateUrl: './octavos.component.html',
  styleUrls: ['./octavos.component.css']
})
export class OctavosComponent {
  @Input() equipos: any[] = []; //Recibo informacion de los equipos que pasaron

  constructor(private gs: GlobalService) {}

  eliminatoriaDates: any[] = [];

  private destroy$ = new Subject<void>();

   ngOnInit() {
    

    this.gs.eliminatoriaDates$
      .pipe(takeUntil(this.destroy$))
      .subscribe((dates) => {
        this.eliminatoriaDates = dates;
        //console.log('Recibido en OtroComponente:', this.eliminatoriaDates);
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }


  // Obtener el nombre del equipo por ID
  getEquipoNombre(id: number): string {
    const equipo = this.equipos.find(e => e.id === id);
    return equipo ? equipo.team : 'Sin Asignar';
  }

  // Obtener partidos por fase (3: Semifinales)
  getPartidos(fase: number): any[] {
    const faseData = this.eliminatoriaDates.find(f => f.fase === fase);
    return faseData ? faseData.partidos : [];
  }

  // Obtener partidos de la final
  getFinal(): any[] {
    const faseData = this.eliminatoriaDates.find(f => f.fase === 2);
    return faseData ? [faseData.partidos[0]] : [];
  }

  // Obtener partidos de 3er/4to puesto
  getTercerCuarto(): any[] {
    const faseData = this.eliminatoriaDates.find(f => f.fase === 2);
    return faseData ? [faseData.partidos[1]] : [];
  }

  // Determinar si un equipo ganó
  isWinner(partido: any, equipo: number): boolean {
    if (equipo === 1) {
      return partido.marcadorEquipo1 > partido.marcadorEquipo2;
    } else if (equipo === 2) {
      return partido.marcadorEquipo2 > partido.marcadorEquipo1;
    }
    return false;
  }
}
