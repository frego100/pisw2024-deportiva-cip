import { Injectable } from '@angular/core';
import { AuthConfig, OAuthService } from 'angular-oauth2-oidc';

@Injectable({
  providedIn: 'root'
})
export class AuthGoogleService {

  constructor(private oauthService: OAuthService) {
    this.initLogin();
   }

  initLogin(){
    const config: AuthConfig={
      issuer: 'https://accounts.google.com',
      strictDiscoveryDocumentValidation: false,
      clientId:'658732871244-2fvjo5m6fe4tclvnjdugc1j6a19rurju.apps.googleusercontent.com',
      redirectUri: window.location.origin + '/home', // redireccionamiento luego de autenticacion
      scope: 'openid profile email' // traemos informacion de la cuenta de google
    }
    this.oauthService.configure(config);
    this.oauthService.setupAutomaticSilentRefresh();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();

  }
  loginAdmin(){
    this.oauthService.initLoginFlow()
  }

  logoutAdmin(){
    this.oauthService.logOut();
  }
  getProfile(){
    return this.oauthService.getIdentityClaims();
  }
}
