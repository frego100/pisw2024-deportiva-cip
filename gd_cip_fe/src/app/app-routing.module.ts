import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginCipComponent } from './login-cip/login-cip.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { CrearCampeonatoComponent } from './crear-campeonato/crear-campeonato.component';
import { UserLoginComponent } from './user-login/user-login.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { MisCampeonatosComponent } from './mis-campeonatos/mis-campeonatos.component';
import { Menu1Component } from './menu1/menu1.component';
import { CrCampCatComponent} from './cr-camp-cat/cr-camp-cat.component'
import { RegisterUserComponent } from './register-user/register-user.component';
import { InicioMenu2Component } from './inicio-menu2/inicio-menu2.component';
import { TablaPartidosComponent } from './tabla-partidos/tabla-partidos.component';
import { TablaPartidosVoleyComponent } from './tabla-partidos-voley/tabla-partidos-voley.component';
import { EditarResultadoComponent } from './editar-resultado/editar-resultado.component';
import { ClasificacionComponent } from './clasificacion/clasificacion.component';
import { EquipoVistaComponent } from './equipo-vista/equipo-vista.component';
import { Menu2Component } from './menu2/menu2.component';
import { VistaPartidoComponent } from "./vista-partido/vista-partido.component";
import { OpcionesInfoCampeonatoComponent } from './opciones-info-campeonato/opciones-info-campeonato.component';
import { MenuCampeonatoComponent } from './menu-campeonato/menu-campeonato.component';
import { GestionJugadoresComponent } from './gestion-jugadores/gestion-jugadores.component';
import { ListaEquiposComponent } from './lista-equipos/lista-equipos.component';
import { ListaDeportesComponent } from './lista-deportes/lista-deportes.component';
import { ClasificacionUsuarioComponent } from './clasificacion-usuario/clasificacion-usuario.component';
import { MatchSummaryComponent } from './equipo-vista/match-summary/match-summary.component';
import { GestionUsuariosComponent } from './gestion-usuarios/gestion-usuarios.component';
import { BotonNuevoCampeonatoComponent } from './boton-nuevo-campeonato/boton-nuevo-campeonato.component';
import { EditarResultadoVoleyComponent } from './editar-resultado-voley/editar-resultado-voley.component';
import { ClasificacionVoleyComponent } from './clasificacion-voley/clasificacion-voley.component';
import { EqvoleyVistaComponent } from './eqvoley-vista/eqvoley-vista.component';
import { EditableBoxVoleyComponent } from './editable-box-voley/editable-box-voley.component';
import { AuthGuard } from './auth.guard';
import { CallbackComponent } from './callback/callback.component';
import { MenuCampeonatoCategoriasComponent } from './menu-campeonato-categorias/menu-campeonato-categorias.component';
const routes: Routes = [
  { path: 'categoria', component: CategoriaComponent, canActivate: [AuthGuard]},
  { path: 'crear-campeonato', component: CrearCampeonatoComponent, canActivate: [AuthGuard]},
  { path: 'login_cip', component: LoginCipComponent},
  { path: 'register-user', component: RegisterUserComponent},
  { path: 'user-login', component: UserLoginComponent },
  { path: 'admin-login', component: AdminLoginComponent },
  { path: 'home', component: MisCampeonatosComponent, canActivate: [AuthGuard] },// mis-campeonatos = home
  { path: 'menu1', component: Menu1Component,},
  { path: 'cr-camp-cat', component: CrCampCatComponent, canActivate: [AuthGuard]},
  { path: 'mis-campeonatos', component: MisCampeonatosComponent, canActivate: [AuthGuard] },
  { path: 'inicio-menu2', component: InicioMenu2Component, canActivate: [AuthGuard]},
  { path: 'partidos', component: TablaPartidosComponent, canActivate: [AuthGuard]},
  { path: 'editar-resultado', component: EditarResultadoComponent, canActivate: [AuthGuard]},
  { path: 'clasificacion', component: ClasificacionComponent, canActivate: [AuthGuard]},
  { path: 'clasificacion-user', component: ClasificacionUsuarioComponent, canActivate: [AuthGuard]},
  { path: 'equipo-vista', component: EquipoVistaComponent, canActivate: [AuthGuard]},
  { path: 'menu2', component: Menu2Component, canActivate: [AuthGuard]},
  { path: 'vista-partido', component: VistaPartidoComponent, canActivate: [AuthGuard]},
  { path: 'opciones-info-campeonato', component: OpcionesInfoCampeonatoComponent, canActivate: [AuthGuard] },
  { path: 'menu-campeonato', component: MenuCampeonatoComponent, canActivate: [AuthGuard] },
  { path: 'gestion-jugadores', component: GestionJugadoresComponent, canActivate: [AuthGuard] },
  { path: 'lista-equipos', component: ListaEquiposComponent, canActivate: [AuthGuard]},
  { path: 'lista-deportes', component: ListaDeportesComponent, canActivate: [AuthGuard]},
  { path: 'partidosvistaequipos', component: MatchSummaryComponent, canActivate: [AuthGuard]},
  { path: 'gestion-usuarios', component: GestionUsuariosComponent, canActivate: [AuthGuard] },
  { path: 'boton-nuevo-campeonato', component: BotonNuevoCampeonatoComponent, canActivate: [AuthGuard]},
  { path: 'partidos-voley', component: TablaPartidosVoleyComponent, canActivate: [AuthGuard]},
  { path: 'clasificacion-voley', component: ClasificacionVoleyComponent, canActivate: [AuthGuard]},
  { path: 'Team-Voley', component: EqvoleyVistaComponent, canActivate: [AuthGuard]},
  { path: 'menu-campeonato-categorias', component: MenuCampeonatoCategoriasComponent, canActivate: [AuthGuard]},
  { path: '', redirectTo: '/user-login', pathMatch: 'full' }, // Ruta predeterminada
  { path: 'editar-resultado-voley', component: EditarResultadoVoleyComponent },
  { path: 'callback', component: CallbackComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
