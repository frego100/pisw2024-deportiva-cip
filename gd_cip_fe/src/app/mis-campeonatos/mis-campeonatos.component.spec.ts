import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MisCampeonatosComponent } from './mis-campeonatos.component';

describe('MisCampeonatosComponent', () => {
  let component: MisCampeonatosComponent;
  let fixture: ComponentFixture<MisCampeonatosComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MisCampeonatosComponent]
    });
    fixture = TestBed.createComponent(MisCampeonatosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
