import { Component, OnInit, Input } from '@angular/core';
import { AuthGoogleService } from '../auth-google.service'; //servicio de autenticación
import { Router } from '@angular/router';
import { UsuarioAdminService } from '../services/usuario-admin.service';
import { MatDialog } from '@angular/material/dialog';

import { CategoriaComponent } from '../categoria/categoria.component';
import { CrCampCatComponent } from '../cr-camp-cat/cr-camp-cat.component';
import { CrearCampeonatoComponent } from '../crear-campeonato/crear-campeonato.component';

import { TipoCampService } from '../services/SCrearCamp/tipo-camp.service';
import { CampeonatoService } from '../services/SCrearCamp/campeonato.service';

@Component({
  selector: 'app-mis-campeonatos',
  templateUrl: './mis-campeonatos.component.html',
  styleUrls: ['./mis-campeonatos.component.css']
})
export class MisCampeonatosComponent implements OnInit {
  username= 'Juan';
  nuevoCampeonatoOpen= false;
  isLoggedIn= false;
  tiposCampeonato: any[] = [];
  @Input() campeonatos: any[] = [];
  listCampeonatos= false;
 

  campeonatosCreados = [
    { id: 1, nombre: 'Campeonato 1' },
    { id: 2, nombre: 'Campeonato 2' },
    // Agrega más objetos según sea necesario
    
  ];

  constructor(
    private authGoogleService: AuthGoogleService,
    private router: Router,
    private service: UsuarioAdminService,
    public dialog: MatDialog, 
    private tipoCampeonatoService: TipoCampService, 
    private campeonatoService: CampeonatoService,
  ){}

  // Cerrando sesion de google  
  logOut() {
    this.authGoogleService.logoutAdmin()
    this.router.navigate(['user-login'])
  }

  ngOnInit(){
    this.campeonatos = this.campeonatos || []; // Asegúrate de tener un valor predeterminado

    this.tipoCampeonatoService.getTiposCampeonato().subscribe(
      data => {
        this.tiposCampeonato = data;
      },
      error => {
        console.error('Error al obtener los tipos de campeonatos:', error);
      }
    );
 
    // Obteniendo datos de Google para el admin
    const data = JSON.stringify(this.authGoogleService.getProfile())
    console.log(data)

    // googleProfile
    let gp = this.authGoogleService.getProfile();
    if(gp == null)
      return
    
    let request = this.service.getByEmail({
      "usuEmail": gp['email'],
    });
    request.subscribe((v) => {
      console.log(v)
      if(v == null){
        let aux = {
          "usuEmail": gp['email'],
          "usuNombre": gp['name']
        }
        this.service.insertAdmin(aux).subscribe();
      }
    })
  }

  //Ventana modal categoria
  openCategoriaModal(tipo: any): void {

    const campeonato = {
      nombreCamp: "", // Campo vacío para el nombre
      tipoCampeonato: tipo // Asignar el tipo de campeonato ala tabla
    };

    const dialogRef = this.dialog.open(CategoriaComponent, {
      data: { tipoCampeonato: tipo}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Datos Recibidos:', result);
        this.createCampeonato(result.tipoCampeonato, result.deporte);
      }
    });

  }

  createCampeonato(tipoCampeonato: any, deporte: any): void {
    const campeonato = {
      nombreCamp: "", // Campo vacío para el nombre
      tipoCampeonato: tipoCampeonato // Asignar el tipo de campeonato
    };

    this.campeonatoService.createCampeonato(campeonato).subscribe(
      (newCampeonato: any) => {
        console.log('Created campeonato:', newCampeonato);

        // Abrir el modal después de crear el campeonato
        const dialogRef = this.dialog.open(CrearCampeonatoComponent, {
          data: { 
            tipoCampeonato: tipoCampeonato,
            deporte: deporte,
            campeonatoId: newCampeonato.idCamp
          }
        });

        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            console.log('El diálogo de CrearCampeonatoComponent se cerró con resultado:', result);
          }
        });
      },
      error => {
        console.error('Error al crear el campeonato:', error);
      }
    );
  }



    // Crear el campeonato antes de abrir el modal
    /*this.campeonatoService.createCampeonato(campeonato).subscribe(
      (newCampeonato: any) => {
        console.log('Created campeonato:', newCampeonato);

        // Abrir el modal después de crear el campeonato
        const dialogRef = this.dialog.open(CategoriaComponent, {
          data: { tipoCampeonato: tipo, campeonatoId: newCampeonato.idCamp }
        });

      //No hacer nada en particular cuando el modal se cierra,
      //solo asegurar que la operación no se repita.
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            console.log('El diálogo se cerró con resultado:', result);
          }
        });
      },
      error => {
        console.error('Error al crear el campeonato:', error);
      }
    );
  }*/


  //Ventana modal cr-camp-cat(la que sale al momento de selccionar cameponato con categorias)
  openCrCampCatModal(tipo: any): void{
    const dialogRef = this.dialog.open(CrCampCatComponent, {
      data: { tipoCampeonato: tipo }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('El diálogo se cerró con resultado:', result);
    });
  }
}
