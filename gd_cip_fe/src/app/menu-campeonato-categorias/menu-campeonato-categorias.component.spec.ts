import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCampeonatoCategoriasComponent } from './menu-campeonato-categorias.component';

describe('MenuCampeonatoCategoriasComponent', () => {
  let component: MenuCampeonatoCategoriasComponent;
  let fixture: ComponentFixture<MenuCampeonatoCategoriasComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MenuCampeonatoCategoriasComponent]
    });
    fixture = TestBed.createComponent(MenuCampeonatoCategoriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
