package com.example.gd_cip_be.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gd_cip_be.entity.AlineacionVoley;
import com.example.gd_cip_be.entity.repository.AlineacionVoleyRepository;

@Service
public class AlineacionVoleyService {
    @Autowired
    AlineacionVoleyRepository avr;

    public List<AlineacionVoley> findByPartidoIdAndEquipoId(Long partidoId, Long equipoId){
        return avr.findByPartidoIdAndEquipoId(partidoId, equipoId);
    }

    public void deleteAll(List<AlineacionVoley> o_){
        avr.deleteAll(o_);
    }

    public List<AlineacionVoley> saveAll(List<AlineacionVoley> o_){
        return avr.saveAll(o_);
    }
}
