package com.example.gd_cip_be.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gd_cip_be.entity.PuntoVoley;
import com.example.gd_cip_be.entity.repository.PuntoVoleyRepository;

@Service
public class PuntoVoleyService {
    @Autowired
    PuntoVoleyRepository pvr;

    public List<PuntoVoley> saveAll(List<PuntoVoley> o_){
        return pvr.saveAll(o_);
    }

    public PuntoVoley save(PuntoVoley o){
        return pvr.save(o);
    }

    public void delete(PuntoVoley o){
        pvr.delete(o);
    }

    public void deleteById(Long id){
        pvr.deleteById(id);
    }

    public void deleteAll(List<PuntoVoley> o_){
        pvr.deleteAll(o_);
    }

    public Optional<PuntoVoley> findById(Long id){
        return pvr.findById(id);
    }

    public List<PuntoVoley> findByPartidoIdEquipoId(Long partidoId, Long equipoId){
        return pvr.findByPartidoIdAndEquipoId(partidoId, equipoId);
    }
}
