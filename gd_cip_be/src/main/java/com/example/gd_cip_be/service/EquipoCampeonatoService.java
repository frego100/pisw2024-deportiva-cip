package com.example.gd_cip_be.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gd_cip_be.entity.EquipoCampeonato;
import com.example.gd_cip_be.entity.repository.EquipoCampeonatoRepository;

@Service
public class EquipoCampeonatoService {

    @Autowired
    EquipoCampeonatoRepository ecr;

    public List<EquipoCampeonato> saveAll(List<EquipoCampeonato> o_){
        return ecr.saveAll(o_);
    }

    public EquipoCampeonato save(EquipoCampeonato o){
        return ecr.save(o);
    }

    public void delete(EquipoCampeonato o){
        ecr.delete(o);
    }

    public void deleteAll(List<EquipoCampeonato> o_){
        ecr.deleteAll(o_);
    }

    public List<EquipoCampeonato> findByCampeonatoId(Long id){
        return ecr.findByCampeonatoId(id);
    }
}
