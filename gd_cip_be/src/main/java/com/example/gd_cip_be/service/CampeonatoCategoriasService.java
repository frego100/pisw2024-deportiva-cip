package com.example.gd_cip_be.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gd_cip_be.entity.CampeonatoCategorias;
import com.example.gd_cip_be.entity.repository.CampeonatoCategoriasRepository;

@Service
public class CampeonatoCategoriasService {
    @Autowired
    CampeonatoCategoriasRepository ccr;

    public CampeonatoCategorias save(CampeonatoCategorias o){
        return ccr.save(o);
    }

    public void deleteById(Long id){
        ccr.deleteById(id);
    }

    public List<CampeonatoCategorias>  findAll(){
        return ccr.findAll();
    }

    public CampeonatoCategorias findById(Long id){
        return ccr.findById(id).orElseThrow();
    }
}
