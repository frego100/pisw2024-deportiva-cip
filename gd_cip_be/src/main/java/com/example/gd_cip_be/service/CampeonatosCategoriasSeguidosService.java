package com.example.gd_cip_be.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gd_cip_be.entity.CampeonatosCategoriasSeguidos;
import com.example.gd_cip_be.entity.repository.CampeonatosCategoriasSeguidosRepository;

@Service
public class CampeonatosCategoriasSeguidosService {
    @Autowired
    CampeonatosCategoriasSeguidosRepository r;

    public List<CampeonatosCategoriasSeguidos> findByUsuarioCipId(Long id){
        return r.findByUsuarioCipId(id);
    }

    public CampeonatosCategoriasSeguidos save(CampeonatosCategoriasSeguidos o){
        return r.save(o);
    }

    public void deleteById(Long id){
        r.deleteById(id);
    }

    public void deleteAllById(List<Long> id_){
        r.deleteAllById(id_);
    }
}
