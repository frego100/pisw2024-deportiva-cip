package com.example.gd_cip_be.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gd_cip_be.entity.CampeonatosSeguidos;
import com.example.gd_cip_be.entity.repository.CampeonatosSeguidosRepository;

@Service
public class CampeonatosSeguidosService {
    @Autowired
    private CampeonatosSeguidosRepository r;

    public List<CampeonatosSeguidos> findByUsuarioCipId(Long id){
        return r.findByUsuarioCipId(id);
    }

    public CampeonatosSeguidos save(CampeonatosSeguidos o){
        return r.save(o);
    }

    public void deleteById(Long id){
        r.deleteById(id);
    }

    public void deleteAllById(List<Long> id_){
        r.deleteAllById(id_);
    }
}
