package com.example.gd_cip_be.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gd_cip_be.dto.PartidoVoleyDTO;
import com.example.gd_cip_be.dto.PartidoVoleyDetDTO;
import com.example.gd_cip_be.entity.AlineacionVoley;
import com.example.gd_cip_be.entity.Equipo;
import com.example.gd_cip_be.entity.PartidoVoley;
import com.example.gd_cip_be.entity.PuntoVoley;
import com.example.gd_cip_be.entity.Sustitucion;
import com.example.gd_cip_be.entity.repository.EquipoRepository;
import com.example.gd_cip_be.entity.repository.PartidoVoleyRepository;
import com.example.gd_cip_be.entity.repository.PuntoVoleyRepository;

@Service
public class PartidoVoleyService {
	
	@Autowired
	PartidoVoleyRepository r;

	@Autowired
	EquipoRepository er;

	@Autowired
	AlineacionVoleyService avs;
	
	@Autowired
	PuntoVoleyRepository pvr;

	@Autowired
	SustitucionService ss;
	
	public void deleteById(Long id) {
		r.deleteById(id);
	}
	
	public PartidoVoley save(PartidoVoley o) {
		return r.save(o);
	}
	
	public List<PartidoVoleyDTO> findByCampeonatoId(Long id){
		List<PartidoVoleyDTO> res = new ArrayList<PartidoVoleyDTO>();
		List<PartidoVoley> partidos = r.findByCampeonatoId(id);
		Map<Long, Equipo> equipos = er.findAll()
			.stream()
			.collect(Collectors.toMap(Equipo::getId, (equipo) -> equipo));

		partidos.forEach((t) -> {
			//String eq1 = equipos.get(t.getEquipo1Id()).getNombre();
			String eq1 = Optional.ofNullable(t.getEquipo1Id())
				.map(equipos::get)
				.map(Equipo::getNombre)
				.orElse("Equipo 1");
			//String eq2 = equipos.get(t.getEquipo2Id()).getNombre();
			String eq2 = Optional.ofNullable(t.getEquipo2Id())
				.map(equipos::get)
				.map(Equipo::getNombre)
				.orElse("Equipo 2");
			res.add(new PartidoVoleyDTO(t, eq1, eq2));
		});

		return res;
	}

	public PartidoVoley findById(Long id){
		return r.findById(id).orElseThrow();
	}

	public PartidoVoleyDetDTO savePartidoVoleyDet(PartidoVoleyDetDTO o){
		avs.saveAll(o.getAlineacion());
		pvr.saveAll(o.getPuntos());
		ss.saveAll(o.getSustituciones());

		return o;
	}

	public void deletePartidoVoleyDet(PartidoVoleyDetDTO o){
		avs.deleteAll(o.getAlineacion());
		pvr.deleteAll(o.getPuntos());
		ss.deleteAll(o.getSustituciones());
	}

	public PartidoVoleyDetDTO getPartidoVoleyDet(Long partidoId, Long equipoId){
		PartidoVoleyDetDTO res = new PartidoVoleyDetDTO();

		List<AlineacionVoley> alineacion = avs.findByPartidoIdAndEquipoId(partidoId, equipoId);
		res.setAlineacion(alineacion);

		List<PuntoVoley> puntos = pvr.findByPartidoIdAndEquipoId(partidoId, equipoId);
		res.setPuntos(puntos);

		List<Sustitucion> sustituciones = ss.getByPartidoIdEquipoId(partidoId, equipoId);
		res.setSustituciones(sustituciones);

		return res;
	}
}
