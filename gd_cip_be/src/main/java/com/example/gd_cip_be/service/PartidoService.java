package com.example.gd_cip_be.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gd_cip_be.dto.PartidoDetDTO;
import com.example.gd_cip_be.entity.Equipo;
import com.example.gd_cip_be.entity.Partido;
import com.example.gd_cip_be.entity.repository.PartidoRepository;

@Service
public class PartidoService {
    
    @Autowired
    PartidoRepository pr;
    
    @Autowired
    GolService gs;
    
    @Autowired
    TarjetaService ts;
    
    @Autowired
    FaltaService fs;
    
    @Autowired
    AlineacionService as;
    
    @Autowired
    SustitucionService ss;
    
    @Autowired
    PorteroService ps;
    
    @Autowired
    JugadaService js;
    
    @Autowired
    EquipoService es;
    
    public Partido getById(Long id) {
        return pr.findById(id).orElseThrow(() -> new RuntimeException("Partido con id "+id+" no encontrado."));
    }
    
    public Partido save(Partido o) {
        return pr.save(o);
    }
    
    public List<Partido> getByCampeonatoId(Long id) {
        return pr.findByCampeonatoId(id);
    }
    
    public PartidoDetDTO getByPartidoEquipoId(Long partidoId , Long equipoId){
        PartidoDetDTO res = null;
        
        res = new PartidoDetDTO(
                gs.getByPartidoIdEquipoId(partidoId, equipoId, 0),
                ts.getByPartidoIdEquipoId(partidoId, equipoId, 1),
                ts.getByPartidoIdEquipoId(partidoId, equipoId, 2),
                ts.getByPartidoIdEquipoId(partidoId, equipoId, 3),
                fs.getByPartidoIdEquipoId(partidoId, equipoId),
                as.getByPartidoIdEquipoId(partidoId, equipoId),
                ss.getByPartidoIdEquipoId(partidoId, equipoId),
                gs.getByPartidoIdEquipoId(partidoId, equipoId, 1),
                ps.getByPartidoIdEquipoId(partidoId, equipoId),
                js.getByPartidoIdEquipoId(partidoId, equipoId)
        );
        
        return res;
    }
    
    public PartidoDetDTO savePartidoEquipoId(PartidoDetDTO o) {
        PartidoDetDTO res = new PartidoDetDTO(
                gs.saveAll(o.getGoles()),
                ts.saveAll(o.getTarjetasAmarillas()),
                ts.saveAll(o.getTarjetasRojas()),
                ts.saveAll(o.getTarjetasAzules()),
                fs.saveAll(o.getFaltas()),
                as.saveAll(o.getAlineacion()),
                ss.saveAll(o.getSustituciones()),
                gs.saveAll(o.getGolesContra()),
                ps.saveAll(o.getPortero()),
                js.saveAll(o.getJugadasPartido())
        );
        
        return res;
    }
    
    public void deleteById(Long id) {
        pr.deleteById(id);
    }
    
    public void deletePartidoDet(PartidoDetDTO o) {
        gs.deleteAll(o.getGoles());
        ts.deleteAll(o.getTarjetasAmarillas());
        ts.deleteAll(o.getTarjetasRojas());
        ts.deleteAll(o.getTarjetasAzules());
        fs.deleteAll(o.getFaltas());
    	as.deleteAll(o.getAlineacion());
    	ss.deleteAll(o.getSustituciones());
    	gs.deleteAll(o.getGolesContra());
    }
}
