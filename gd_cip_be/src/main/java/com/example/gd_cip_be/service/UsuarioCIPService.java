package com.example.gd_cip_be.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.gd_cip_be.entity.UsuarioCIP;
import com.example.gd_cip_be.entity.repository.UsuarioCIPRepository;

@Service
public class UsuarioCIPService {
    @Autowired
    UsuarioCIPRepository r;
    
    public UsuarioCIP findById(Long id) {
        return r.findById(id)
            .orElseThrow(() -> new RuntimeException("Usuario con ID "+id+" no encontrado"));
    }

    public UsuarioCIP findByCip(String cip){
        return r.findByCip(cip).orElseThrow();
    }

    public UsuarioCIP save(UsuarioCIP o){
        return r.save(o);
    }

    public void deleteById(Long id){
        r.deleteById(id);
    }

    public List<UsuarioCIP> findAll(){
        return r.findAll();
    }
}
