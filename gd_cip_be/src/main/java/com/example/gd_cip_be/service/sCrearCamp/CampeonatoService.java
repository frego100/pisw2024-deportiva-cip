package com.example.gd_cip_be.service.sCrearCamp;

import com.example.gd_cip_be.entity.ClasificacionCab;
import com.example.gd_cip_be.entity.eCrearCamp.Campeonato;
import com.example.gd_cip_be.entity.repository.ClasificacionCabRepository;
import com.example.gd_cip_be.entity.repository.rCrearCamp.CampeonatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CampeonatoService {

    @Autowired
    private CampeonatoRepository campeonatoRepository;

    @Autowired
    private ClasificacionCabRepository ccr;

    public List<Campeonato> getAllCampeonatos() {
        return campeonatoRepository.findAll();
    }

    public Campeonato getCampeonatoById(Long id) {
        return campeonatoRepository.findById(id).orElse(null);
    }

    public List<Campeonato> getByCampeonatoCategoriasId(Long id){
        return campeonatoRepository.findByCampeonatoCategoriasId(id);
    }

    public Campeonato crearCampeonato(Campeonato campeonato) {
        ccr.save(new ClasificacionCab(campeonato.getId()));
        return campeonatoRepository.save(campeonato);
    }

    public void deleteCampeonato(Campeonato campeonato){
        ccr.delete(new ClasificacionCab(campeonato.getId()));
        campeonatoRepository.delete(campeonato);
    }

    public void deleteCampeonatoById(Long id){
        ccr.deleteById(id);
        campeonatoRepository.deleteById(id);
    }
}