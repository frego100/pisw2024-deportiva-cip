package com.example.gd_cip_be.dto;

import org.springframework.beans.BeanUtils;

import com.example.gd_cip_be.entity.Equipo;
import com.example.gd_cip_be.entity.Partido;

import lombok.Data;

@Data
public class PartidoCabDTO extends Partido {

    Equipo equipo1;
    Equipo equipo2;

    public PartidoCabDTO(
            Partido p,
            Equipo equipo1,
            Equipo equipo2
    ) {
        super();
        BeanUtils.copyProperties(p, this);
        this.equipo1 = equipo1;
        this.equipo2 = equipo2;
    }
}
