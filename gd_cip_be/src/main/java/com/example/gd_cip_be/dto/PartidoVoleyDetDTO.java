package com.example.gd_cip_be.dto;

import java.util.List;

import com.example.gd_cip_be.entity.Alineacion;
import com.example.gd_cip_be.entity.AlineacionVoley;
import com.example.gd_cip_be.entity.PuntoVoley;
import com.example.gd_cip_be.entity.Sustitucion;

import lombok.Data;

@Data
public class PartidoVoleyDetDTO {
    private List<AlineacionVoley> alineacion;
    private List<PuntoVoley> puntos;
    private List<Sustitucion> sustituciones;
}
