package com.example.gd_cip_be.dto;

import org.springframework.beans.BeanUtils;

import com.example.gd_cip_be.entity.PartidoVoley;

import lombok.Data;

@Data
public class PartidoVoleyDTO extends PartidoVoley {
    private String equipo1Nombre;
    private String equipo2Nombre;

    public PartidoVoleyDTO(PartidoVoley p, String eq1, String eq2){
        BeanUtils.copyProperties(p, this);
        this.equipo1Nombre = eq1;
        this.equipo2Nombre = eq2;
    }
}
