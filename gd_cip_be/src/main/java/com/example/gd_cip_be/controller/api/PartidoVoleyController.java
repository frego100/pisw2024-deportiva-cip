package com.example.gd_cip_be.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.gd_cip_be.dto.PartidoVoleyDTO;
import com.example.gd_cip_be.dto.PartidoVoleyDetDTO;
import com.example.gd_cip_be.entity.PartidoVoley;
import com.example.gd_cip_be.service.PartidoVoleyService;

@RestController
@RequestMapping(path = "partidoVoley")
public class PartidoVoleyController {
	
	@Autowired
	PartidoVoleyService ps;
	
	@PostMapping
	public PartidoVoley save(@RequestBody PartidoVoley o) {
		return ps.save(o);
	}
	
	@DeleteMapping(path = "/{id}")
	public void deleteById(@PathVariable Long id) {
		ps.deleteById(id);
	}
	
	@GetMapping(path = "/byCampeonato/{id}")
	public List<PartidoVoleyDTO> findByCameponatoId(@PathVariable Long id){
		return ps.findByCampeonatoId(id);
	}

	@GetMapping(path = "/{id}")
	public PartidoVoley findById(@PathVariable Long id){
		return ps.findById(id);
	}

	@PostMapping(path = "/detalle")
	public PartidoVoleyDetDTO savePartidoDetalle(@RequestBody PartidoVoleyDetDTO o){
		return ps.savePartidoVoleyDet(o);
	}

	@DeleteMapping(path = "/detalle")
	public void deletePartidoDetalle(@RequestBody PartidoVoleyDetDTO o){
		ps.deletePartidoVoleyDet(o);
	}

	@GetMapping(path = "/detalle")
	public PartidoVoleyDetDTO findPartidoDetalle(@RequestParam Long partidoId, @RequestParam Long equipoId){
		return ps.getPartidoVoleyDet(partidoId, equipoId);
	}
}
