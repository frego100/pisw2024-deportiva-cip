package com.example.gd_cip_be.controller.api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(path = "/upload")
public class FileUploadController {
    private static final String UPLOADS_DIR = "uploads/";

    @PostMapping
    public Boolean uploadFile(@RequestParam("file") MultipartFile file){
        try {
            Path uploadPath = Paths.get(UPLOADS_DIR);
            if (!Files.exists(uploadPath)){
                Files.createDirectories(uploadPath);
            }

            Path filePath = uploadPath.resolve(file.getOriginalFilename());
            Files.write(filePath, file.getBytes());

            return true;
        }
        catch(IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
