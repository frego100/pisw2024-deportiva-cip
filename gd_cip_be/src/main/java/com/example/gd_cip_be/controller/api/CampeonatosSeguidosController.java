package com.example.gd_cip_be.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.gd_cip_be.entity.CampeonatosSeguidos;
import com.example.gd_cip_be.service.CampeonatosSeguidosService;

@RestController
@RequestMapping(path = "/campeonatosSeguidos")
public class CampeonatosSeguidosController {
    @Autowired
    private CampeonatosSeguidosService css;

    @GetMapping(path = "/{id}")
    public List<CampeonatosSeguidos> findByUduarioCipId(@PathVariable("id") Long id){
        return css.findByUsuarioCipId(id);
    }

    @PostMapping
    public CampeonatosSeguidos save(@RequestBody CampeonatosSeguidos o){
        return css.save(o);
    }

    @DeleteMapping("/{id}")
    public void deletebyId(@PathVariable("id") Long id){
        css.deleteById(id);
    }

    @DeleteMapping
    public void deleteAllById(@RequestBody List<Long> id_){
        css.deleteAllById(id_);
    }
}
