package com.example.gd_cip_be.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.gd_cip_be.entity.UsuarioCIP;
import com.example.gd_cip_be.service.UsuarioCIPService;

@RestController
@RequestMapping(path = "usuarioCIP")
public class UsuarioCIPController {
    @Autowired
    UsuarioCIPService s;
    
    @GetMapping(path = "/cip/{cip}")
    public UsuarioCIP findByCIP(@PathVariable("cip") String cip) {
        return s.findByCip(cip);
    }

    @GetMapping(path = "/{id}")
    public UsuarioCIP findById(@PathVariable("id") Long id){
        return s.findById(id);
    }

    @GetMapping
    public List<UsuarioCIP> findAll(){
        return s.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable("id") Long id){
        s.deleteById(id);
    }

    @PostMapping
    public UsuarioCIP save(@RequestBody UsuarioCIP o){
        return s.save(o);
    }
}
