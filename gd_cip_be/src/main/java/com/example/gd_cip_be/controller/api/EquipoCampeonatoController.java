package com.example.gd_cip_be.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.gd_cip_be.entity.EquipoCampeonato;
import com.example.gd_cip_be.service.EquipoCampeonatoService;

@RestController
@RequestMapping(path = "equipoCampeonato")
public class EquipoCampeonatoController {

    @Autowired
    EquipoCampeonatoService ecs;

    @PostMapping
    public EquipoCampeonato save(@RequestBody EquipoCampeonato o){
        return ecs.save(o);
    }

    @PostMapping(path = "/all")
    public List<EquipoCampeonato> saveAll(@RequestBody List<EquipoCampeonato> o_){
        return ecs.saveAll(o_);
    }

    @GetMapping(path = "/{id}")
    public List<EquipoCampeonato> findByCampeonatoId(@PathVariable Long id){
        return ecs.findByCampeonatoId(id);
    }

    @DeleteMapping()
    public void delete(@RequestBody EquipoCampeonato o){
        ecs.delete(o);
    }

    @DeleteMapping(path = "/all")
    public void deleteAll(@RequestBody List<EquipoCampeonato> o_){
        ecs.deleteAll(o_);
    }
}
