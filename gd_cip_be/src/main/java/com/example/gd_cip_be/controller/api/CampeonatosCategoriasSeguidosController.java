package com.example.gd_cip_be.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.gd_cip_be.entity.CampeonatosCategoriasSeguidos;
import com.example.gd_cip_be.service.CampeonatosCategoriasSeguidosService;

@RestController
@RequestMapping(path = "/campeonatosCategoriasSeguidos")
public class CampeonatosCategoriasSeguidosController {
    @Autowired
    private CampeonatosCategoriasSeguidosService s;

    @GetMapping(path = "/{id}")
    public List<CampeonatosCategoriasSeguidos> findByUduarioCipId(@PathVariable("id") Long id){
        return s.findByUsuarioCipId(id);
    }

    @PostMapping
    public CampeonatosCategoriasSeguidos save(@RequestBody CampeonatosCategoriasSeguidos o){
        return s.save(o);
    }

    @DeleteMapping("/{id}")
    public void deletebyId(@PathVariable("id") Long id){
        s.deleteById(id);
    }

    @DeleteMapping
    public void deleteAllById(@RequestBody List<Long> id_){
        s.deleteAllById(id_);
    }
}
