package com.example.gd_cip_be.controller.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.gd_cip_be.entity.CampeonatoCategorias;
import com.example.gd_cip_be.service.CampeonatoCategoriasService;

@RestController
@RequestMapping(path = "/campeonatoCategorias")
public class CampeonatoCategoriasController {
    @Autowired
    CampeonatoCategoriasService ccs;

    @GetMapping(path = "/{id}")
    public CampeonatoCategorias findById(@PathVariable("id") Long id){
        return ccs.findById(id);
    }

    @GetMapping
    public List<CampeonatoCategorias> findAll(){
        return ccs.findAll();
    }

    @DeleteMapping(path = "/{id}")
    public void deleteById(@PathVariable("id") Long id){
        ccs.deleteById(id);
    }

    @PostMapping
    public CampeonatoCategorias save(@RequestBody CampeonatoCategorias o){
        return ccs.save(o);
    }
}
