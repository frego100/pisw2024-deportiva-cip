package com.example.gd_cip_be.entity.repository.rCrearCamp;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.eCrearCamp.Deporte;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "deporte", path = "deporte")
public interface DeporteRepository extends JpaRepository<Deporte, Long> {

}