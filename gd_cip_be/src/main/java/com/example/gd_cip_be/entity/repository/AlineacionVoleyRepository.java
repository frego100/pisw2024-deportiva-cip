package com.example.gd_cip_be.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.AlineacionVoley;

public interface AlineacionVoleyRepository extends JpaRepository<AlineacionVoley, Long> {
    List<AlineacionVoley> findByPartidoIdAndEquipoId(Long partidoId, Long equipoId);
}
