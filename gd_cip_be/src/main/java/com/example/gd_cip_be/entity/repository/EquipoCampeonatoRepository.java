package com.example.gd_cip_be.entity.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.EquipoCampeonato;

public interface EquipoCampeonatoRepository extends JpaRepository<EquipoCampeonato, Long> {
    public List<EquipoCampeonato> findByCampeonatoId(Long campeonatoId);
}
