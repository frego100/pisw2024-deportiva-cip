package com.example.gd_cip_be.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.Jugador;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "jugador", path = "jugador")
public interface JugadorRepository extends JpaRepository<Jugador, Long> {

}
