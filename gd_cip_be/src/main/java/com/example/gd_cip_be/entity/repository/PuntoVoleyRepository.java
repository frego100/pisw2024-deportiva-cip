package com.example.gd_cip_be.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.PuntoVoley;

public interface PuntoVoleyRepository extends JpaRepository<PuntoVoley, Long> {
    List<PuntoVoley> findByPartidoIdAndEquipoId(Long partidoId, Long equipoId);
}
