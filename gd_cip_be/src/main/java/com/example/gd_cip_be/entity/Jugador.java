package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "jugador")
public class Jugador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "jugId")
    private Long id;
        
    @Column(name = "jugEmail")
    private String email;
    
    @Column(name = "jugNombre")
    private String name;
    
    @Column(name = "jugPosicion")
    private String position;
    
    @Column(name = "jugDNI")
    private String document;
    
    @Column(name = "jugTelefono")
    private String phone;
    
    @Column(name = "jugNacimiento")
    private String birthDate;
    
    @Column(name = "jugEquipoId")
    private Long equipoID;
    
    @Column(name = "jugCIP")
    private Long codCIP;
    
    @Column(name = "jugFamiliarCIP")
    private String codFam;

    @Column(name = "jugCapitulo")
    private String capitulo;
}
