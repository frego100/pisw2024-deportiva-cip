package com.example.gd_cip_be.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.Partido;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "partido", path = "partido")
public interface PartidoRepository extends JpaRepository<Partido, Long> {
    //List<Partido> findByFaseId(Long id);
    List<Partido> findByCampeonatoId(Long id);
}
