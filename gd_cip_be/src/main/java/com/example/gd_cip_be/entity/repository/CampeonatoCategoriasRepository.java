package com.example.gd_cip_be.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.CampeonatoCategorias;

public interface CampeonatoCategoriasRepository extends JpaRepository<CampeonatoCategorias, Long> {

}
