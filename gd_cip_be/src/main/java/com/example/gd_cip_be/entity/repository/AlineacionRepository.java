package com.example.gd_cip_be.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.gd_cip_be.entity.Alineacion;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "alineacion", path = "alineacion")
public interface AlineacionRepository extends JpaRepository<Alineacion, Long>, JpaSpecificationExecutor<Object> {
    List<Alineacion> findByPartidoIdAndEquipoId(Long partidoId, Long equipoId);
}
