package com.example.gd_cip_be.entity.repository.rCrearCamp;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.eCrearCamp.Campeonato;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "campeonato", path = "campeonato")
public interface CampeonatoRepository extends JpaRepository<Campeonato, Long> {
    List<Campeonato> findByCampeonatoCategoriasId(Long campeonatoCategoriasId);
}