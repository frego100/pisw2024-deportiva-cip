package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "punto_voley")
public class PuntoVoley {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "punVolId")
    private Long id;

    @Column(name = "punVolJugadorId")
    private Long jugadorId;

    @Column(name = "punVolPartidoId")
    private Long partidoId;

    @Column(name = "punVolEquipoId")
    private Long equipoId;

    @Column(name = "punVolSet")
    private Integer set;
    
    @Column(name = "punVolPuntos")
    private Integer puntos;
}
