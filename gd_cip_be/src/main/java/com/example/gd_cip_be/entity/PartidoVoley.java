package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "partido_voley")
public class PartidoVoley {
	@Id
	@Column(name = "parId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	 
	@Column(name = "parFecha")
	private String fecha;
	 
	@Column(name = "parPuntaje11")
	private Integer puntaje11;
	 
	@Column(name = "parPuntaje12")
	private Integer puntaje12;
	 
	@Column(name = "parPuntaje21")
	private Integer puntaje21;
	 
	@Column(name = "parPuntaje22")
	private Integer puntaje22;
	 
	@Column(name = "parPuntaje31")
	private Integer puntaje31;
	 
	@Column(name = "parPuntaje32")
	private Integer puntaje32;
	 
	@Column(name = "parPuntaje41")
	private Integer puntaje41;
	 
	@Column(name = "parPuntaje42")
	private Integer puntaje42;
	 
	@Column(name = "parPuntaje51")
	private Integer puntaje51;
	 
	@Column(name = "parPuntaje52")
	private Integer puntaje52;
	 
	@Column(name = "parCampeonatoId")
	private Long campeonatoId;

	@Column(name = "parEquipo1Id")
	private Long equipo1Id;

	@Column(name = "parEquipo2Id")
	private Long equipo2Id;

	@Column(name = "parEstado")
	private String estado;

	@Column(name = "parNumeroSets")
	private Integer numeroSets;

	@Column(name = "parFase")
	private Long fase;

	@Column(name = "parGrupo")
	private String grupo;
}
