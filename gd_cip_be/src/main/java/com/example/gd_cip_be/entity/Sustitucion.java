package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "sustitucion")
@Data
public class Sustitucion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "susId")
    private Long Id;
    
    @Column(name = "susJugadorEntraId")
    private Long jugadorEntraId;
    
    @Column(name = "susJugadorSaleId")
    private Long jugadorSaleId;
    
    @Column(name = "susDescripcion")
    private String descripcion;
    
    @Column(name = "susTiempo")
    private Integer tiempo;
    
    @Column(name = "susMinuto")
    private Integer minuto;
    
    @Column(name = "susSegundo")
    private Integer segundo;

    @Column(name = "susNumeroCamiseta")
    private Integer numeroCamiseta;
    
    @Column(name = "susPartidoId")
    private Long partidoId;
    
    @Column(name = "susEquipoId")
    private Long equipoId;
}
