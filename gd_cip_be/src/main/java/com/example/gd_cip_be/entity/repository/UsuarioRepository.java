package com.example.gd_cip_be.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.Usuario;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "usuario", path = "usuario")
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
    Usuario getByEmail(String email);
}
