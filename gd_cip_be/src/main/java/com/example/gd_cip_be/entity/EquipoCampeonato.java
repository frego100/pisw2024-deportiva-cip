package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "equipo_campeonato_unico")
public class EquipoCampeonato {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "equCamId")
    private Long id;

    @Column(name = "equCamEquId")
    private Long equipoId;

    @Column(name = "equCamCamUniId")
    private Long campeonatoId;

    @Column(name = "equCamGruId")
    private Integer grupoId;
}
