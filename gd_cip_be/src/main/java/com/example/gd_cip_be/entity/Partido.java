package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Table;
import lombok.Data;
import jakarta.persistence.Id;

@Data
@Entity
@Table(name = "partido")
public class Partido {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "parId", nullable = false)
    private Long id;
    
    @Column(name = "parCampeonatoId")
    private Long campeonatoId;
      
    @Column(name = "parEquipoId1")
    private Long equipo1Id;
    
    @Column(name = "parEquipoId2")
    private Long equipo2Id;
    
    @Column(name = "parFecha")
    private String fecha;
    
    @Column(name = "parTipoResultado")
    private String tipoResultado;
    
    @Column(name = "parMarcador1")
    private Integer marcadorEquipo1;

    @Column(name = "parMarcador2")
    private Integer marcadorEquipo2;
    
    @Column(name = "parMejorJugadorId")
    private Long mejorJugadorId;
    
    @Column(name = "parMejorPorteroId")
    private Long mejorPorteroId;

    @Column(name = "parFase")
    private Long fase;

    @Column(name = "parGrupo")
    private String grupo;
}
