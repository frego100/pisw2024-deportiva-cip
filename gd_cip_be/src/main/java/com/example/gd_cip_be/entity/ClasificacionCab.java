package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "clasificacion_cab")
public class ClasificacionCab {
    
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long claCabId;
    
    @Column
    private Long claCabGrupoId;

    public ClasificacionCab(){}

    public ClasificacionCab(Long campeonatoId){
        this.claCabId = campeonatoId;
        this.claCabGrupoId = (long) 1;
    }
}
