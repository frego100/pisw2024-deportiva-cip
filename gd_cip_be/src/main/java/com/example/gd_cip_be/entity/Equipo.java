package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "equipo")
public class Equipo {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "equId")
    private Long id;
    
    @Column(name = "equEmail")
    private String email;
    
    @Column(name = "equNombre")
    private String nombre;
    
    @Column(name = "equEntrenador")
    private String entrenador;
    
    @Column(name = "equGruId")
    private Long grupoId;
    
    @Column(name = "equCampeonatoId")
    private Long campeonatoId;
    
    @Column(name = "equDelegadoNombre1")
    private String delegadoNombre1;

    @Column(name = "equDelegadoTelefono1")
    private String delegadoTelefono1;

    @Column(name = "equDelegadoNombre2")
    private String delegadoNombre2;

    @Column(name = "equDelegadoTelefono2")
    private String delegadoTelefono2;
    
    @Column(name = "equDelegadoNombre3")
    private String delegadoNombre3;

    @Column(name = "equDelegadoTelefono3")
    private String delegadoTelefono3;

    @Column(name = "equDelegadoNombre4")
    private String delegadoNombre4;

    @Column(name = "equDelegadoTelefono4")
    private String delegadoTelefono4;

    @Column(name = "equDelegadoNombre5")
    private String delegadoNombre5;

    @Column(name = "equDelegadoTelefono5")
    private String delegadoTelefono5;
}
