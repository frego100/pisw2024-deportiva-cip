package com.example.gd_cip_be.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.gd_cip_be.entity.Gol;

@RepositoryRestResource(collectionResourceRel = "gol", path = "gol")
public interface GolRepository extends JpaRepository<Gol, Long>, JpaSpecificationExecutor<Object> {

}
