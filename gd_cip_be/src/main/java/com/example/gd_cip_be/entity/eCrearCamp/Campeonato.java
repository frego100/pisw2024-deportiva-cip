package com.example.gd_cip_be.entity.eCrearCamp;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "campeonato_unico")
public class Campeonato {
    @Id
    @Column(name = "camUniId", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "camUniTitulo")
    private String tituloCampeonato;
    
    @Column(name = "camUniOrganizadorNombre")
    private String organizadorNombre;
    
    @Column(name = "camUniOrganizadorEmail")
    private String organizadorEmail;
    
    @Column(name = "camUniOrganizadorNumero")
    private String organizadorNumero;

    @Column(name = "camUniFechaInicio")
    private String fechaInicio;
    
    @Column(name = "camUniFechaFin")
    private String fechaFin;
    
    @Column(name = "camUniTituloHonorifico")
    private String tituloHonorifico;

    @Column(name = "camUniUrlImagen")
    private String urlImagen;

    @Column(name = "camUniTipoCampeonato")
    private Integer tipoCampeonato;

    @Column(name = "camUniCamId")
    private Long campeonatoCategoriasId;
}