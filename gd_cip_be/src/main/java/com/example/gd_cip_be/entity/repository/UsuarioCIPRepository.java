package com.example.gd_cip_be.entity.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.UsuarioCIP;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "usuarioCIP", path = "usuarioCIP")
public interface UsuarioCIPRepository extends JpaRepository<UsuarioCIP, Long> {
    Optional<UsuarioCIP> findByCip(String cip);
}
