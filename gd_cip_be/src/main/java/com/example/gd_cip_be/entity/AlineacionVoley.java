package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "alineacion_voley")
public class AlineacionVoley {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "aliVolId")
    private Long id;
    
    @Column(name = "aliVolJugadorId")
    private Long jugadorId;
    
    @Column(name = "aliVolPartidoId")
    private Long partidoId;
    
    @Column(name = "aliVolEquipoId")
    private Long equipoId;
    
    @Column(name = "aliVolNumeroCamiseta")
    private Integer numeroCamiseta;
}
