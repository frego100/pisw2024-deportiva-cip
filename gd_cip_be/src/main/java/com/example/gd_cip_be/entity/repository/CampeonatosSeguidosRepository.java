package com.example.gd_cip_be.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.CampeonatosSeguidos;

public interface CampeonatosSeguidosRepository extends JpaRepository<CampeonatosSeguidos, Long> {
    List<CampeonatosSeguidos> findByUsuarioCipId(Long usuarioCipId);
}
