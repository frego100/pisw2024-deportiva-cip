package com.example.gd_cip_be.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.CampeonatosCategoriasSeguidos;

public interface CampeonatosCategoriasSeguidosRepository extends JpaRepository<CampeonatosCategoriasSeguidos, Long> {
    List<CampeonatosCategoriasSeguidos> findByUsuarioCipId(Long usuarioCipId);
}
