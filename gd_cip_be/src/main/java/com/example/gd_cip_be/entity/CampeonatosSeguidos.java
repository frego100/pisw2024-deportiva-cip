package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "campeonatos_seguidos")
public class CampeonatosSeguidos {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "camId")
    private Long id;

    @Column(name = "camCampeonatoId")
    private Long campeonatoId;

    @Column(name = "camUsuarioCipId")
    private Long usuarioCipId;
}
