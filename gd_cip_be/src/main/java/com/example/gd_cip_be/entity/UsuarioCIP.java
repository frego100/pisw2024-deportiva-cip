package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "usuario_cip")
public class UsuarioCIP {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usuId")
    private Long id;

    @Column(name = "usuCip")
    private String cip;
    
    @Column(name = "usuNombre")
    private String nombre;

    @Column(name = "usuCapitulo")
    private String capitulo;

    @Column(name = "usuDni")
    private String dni;
}
