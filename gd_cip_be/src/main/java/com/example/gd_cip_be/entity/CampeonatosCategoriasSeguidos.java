package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "campeonatos_categorias_seguidos")
public class CampeonatosCategoriasSeguidos {
    @Id
    @Column(name = "camId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "camCampeonatoId")
    private Long campeonatoCategoriasId;

    @Column(name = "camUsuarioCipId")
    private Long usuarioCipId;
}
