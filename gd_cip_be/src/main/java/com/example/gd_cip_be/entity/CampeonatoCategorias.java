package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "campeonato_categorias")
public class CampeonatoCategorias {
    @Id
    @Column(name = "camId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "camTitulo")
    private String tituloCampeonato;
    
    @Column(name = "camOrganizadorNombre")
    private String organizadorNombre;
    
    @Column(name = "camOrganizadorEmail")
    private String organizadorEmail;
    
    @Column(name = "camOrganizadorNumero")
    private String organizadorNumero;

    @Column(name = "camFechaInicio")
    private String fechaInicio;
    
    @Column(name = "camFechaFin")
    private String fechaFin;
    
    @Column(name = "camTituloHonorifico")
    private String tituloHonorifico;

    @Column(name = "camUrlImagen")
    private String urlImagen;
}
