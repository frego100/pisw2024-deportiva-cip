package com.example.gd_cip_be.entity.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.PartidoVoley;

public interface PartidoVoleyRepository extends JpaRepository<PartidoVoley, Long> {
	List<PartidoVoley> findByCampeonatoId(Long campeonatoId);
}
