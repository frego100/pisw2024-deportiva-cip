package com.example.gd_cip_be.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "usuario")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usuId", nullable = false)
    private Long id;
    
    @Column(name = "usuEmail", length = 254, nullable = false)
    private String email;
    
    @Column(name = "usuNombre", length = 150)
    private String nombre;
    
    @Column(name = "usuApellidos")
    private String apellidos;
    
    @Column(name = "usuFechaNacimiento")
    private String fechaNacimiento;
    
    @Column(name = "usuContraseña", length = 45)
    private String contraseña;
    
    @Column(name = "usuDNI")
    private String dni;
    
    @Column(name = "usuRol")
    private String rol;
}
