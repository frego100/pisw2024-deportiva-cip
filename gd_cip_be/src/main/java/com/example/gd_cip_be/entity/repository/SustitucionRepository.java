package com.example.gd_cip_be.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.example.gd_cip_be.entity.Sustitucion;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "sustitucion", path = "sustitucion")
public interface SustitucionRepository extends JpaRepository<Sustitucion, Long>, JpaSpecificationExecutor<Object> {

}
