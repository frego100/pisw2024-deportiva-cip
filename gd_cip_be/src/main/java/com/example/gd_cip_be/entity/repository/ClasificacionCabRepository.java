package com.example.gd_cip_be.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.gd_cip_be.entity.ClasificacionCab;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "clasificacionCab", path = "clasificacionCab")
public interface ClasificacionCabRepository extends JpaRepository<ClasificacionCab, Long> {

}
