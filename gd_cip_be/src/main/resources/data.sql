INSERT INTO tipo_campeonato (idTC, tipo) VALUES (1, 'Campeonato único');
INSERT INTO tipo_campeonato (idTC, tipo) VALUES (2, 'Campeonato con Categorías');

INSERT INTO deporte (depId, depNombre) VALUES (1, 'Fútbol');
INSERT INTO deporte (depId, depNombre) VALUES (2, 'Voley');
INSERT INTO deporte (depId, depNombre) VALUES (3, 'Baloncesto');

INSERT INTO fases (idfases, nombre) VALUES (1, 'Todos contra todos');
INSERT INTO fases (idfases, nombre) VALUES (2, 'Todos contra todos + Eliminatorias');
INSERT INTO fases (idfases, nombre) VALUES (3, 'Eliminatorias');
