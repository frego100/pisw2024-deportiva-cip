-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: localhost    Database: gd-cip
-- ------------------------------------------------------
-- Server version	8.0.39

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `alineacion`
--

LOCK TABLES `alineacion` WRITE;
/*!40000 ALTER TABLE `alineacion` DISABLE KEYS */;
INSERT INTO `alineacion` VALUES (1,1,'Portero',1,1,11),(2,2,'Defensa',1,1,22),(3,1,'Delantero',1,2,33),(4,1,NULL,4,2,1),(5,2,NULL,4,2,2),(6,2,NULL,5,2,2),(7,1,NULL,5,1,1);
/*!40000 ALTER TABLE `alineacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `alineacion_voley`
--

LOCK TABLES `alineacion_voley` WRITE;
/*!40000 ALTER TABLE `alineacion_voley` DISABLE KEYS */;
/*!40000 ALTER TABLE `alineacion_voley` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `arbitro`
--

LOCK TABLES `arbitro` WRITE;
/*!40000 ALTER TABLE `arbitro` DISABLE KEYS */;
/*!40000 ALTER TABLE `arbitro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `campeonato_categorias`
--

LOCK TABLES `campeonato_categorias` WRITE;
/*!40000 ALTER TABLE `campeonato_categorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `campeonato_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `campeonato_unico`
--

LOCK TABLES `campeonato_unico` WRITE;
/*!40000 ALTER TABLE `campeonato_unico` DISABLE KEYS */;
INSERT INTO `campeonato_unico` VALUES (4,NULL,'Futbol 1',NULL,'2024-12-06','2024-12-13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Franco Delgado','fdelgadov@unsa.edu.pe','988988988','Honor a....','../../assets/portadaSoccer.jpg',1);
/*!40000 ALTER TABLE `campeonato_unico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `campeonatos_seguidos`
--

LOCK TABLES `campeonatos_seguidos` WRITE;
/*!40000 ALTER TABLE `campeonatos_seguidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `campeonatos_seguidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `clasificacion_cab`
--

LOCK TABLES `clasificacion_cab` WRITE;
/*!40000 ALTER TABLE `clasificacion_cab` DISABLE KEYS */;
INSERT INTO `clasificacion_cab` VALUES (4,1);
/*!40000 ALTER TABLE `clasificacion_cab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `clasificacion_det`
--

LOCK TABLES `clasificacion_det` WRITE;
/*!40000 ALTER TABLE `clasificacion_det` DISABLE KEYS */;
INSERT INTO `clasificacion_det` VALUES (1,1,1,1,1,1,1,1,1,1,1,1,NULL,NULL),(2,2,2,3,2,2,2,2,5,2,2,2,NULL,NULL),(3,1,3,2,3,3,3,3,2,3,3,3,NULL,NULL),(4,2,4,4,5,4,4,4,6,4,4,1,NULL,NULL),(5,1,4,1,7,3,4,4,3,5,5,2,NULL,NULL),(6,2,6,3,4,2,3,2,7,6,6,3,NULL,NULL),(7,1,2,2,3,1,1,3,4,7,7,1,NULL,NULL),(8,2,5,4,1,2,2,4,8,8,8,2,NULL,NULL);
/*!40000 ALTER TABLE `clasificacion_det` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `color`
--

LOCK TABLES `color` WRITE;
/*!40000 ALTER TABLE `color` DISABLE KEYS */;
INSERT INTO `color` VALUES (1,'Amarilla'),(2,'Roja'),(3,'Azul');
/*!40000 ALTER TABLE `color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `deporte`
--

LOCK TABLES `deporte` WRITE;
/*!40000 ALTER TABLE `deporte` DISABLE KEYS */;
INSERT INTO `deporte` VALUES (1,'Fútbol'),(2,'Voley'),(3,'Baloncesto');
/*!40000 ALTER TABLE `deporte` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `equipo`
--

LOCK TABLES `equipo` WRITE;
/*!40000 ALTER TABLE `equipo` DISABLE KEYS */;
INSERT INTO `equipo` VALUES (1,'fdelgadov@unsa.edu.pe','Tigres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'fdelgadov@unsa.edu.pe','Leones',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'fdelgadov@unsa.edu.pe','Caballos',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `equipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `equipo_campeonato_unico`
--

LOCK TABLES `equipo_campeonato_unico` WRITE;
/*!40000 ALTER TABLE `equipo_campeonato_unico` DISABLE KEYS */;
INSERT INTO `equipo_campeonato_unico` VALUES (9,1,4,NULL),(10,2,4,NULL),(11,3,4,NULL);
/*!40000 ALTER TABLE `equipo_campeonato_unico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `equipo_jugador`
--

LOCK TABLES `equipo_jugador` WRITE;
/*!40000 ALTER TABLE `equipo_jugador` DISABLE KEYS */;
INSERT INTO `equipo_jugador` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `equipo_jugador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `falta`
--

LOCK TABLES `falta` WRITE;
/*!40000 ALTER TABLE `falta` DISABLE KEYS */;
INSERT INTO `falta` VALUES (1,1,NULL,NULL,NULL,NULL,1),(2,2,NULL,NULL,NULL,NULL,1),(3,1,NULL,NULL,NULL,NULL,2),(4,2,NULL,NULL,NULL,5,2),(5,1,NULL,NULL,NULL,5,1);
/*!40000 ALTER TABLE `falta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `fases`
--

LOCK TABLES `fases` WRITE;
/*!40000 ALTER TABLE `fases` DISABLE KEYS */;
INSERT INTO `fases` VALUES (1,'GRUPOS'),(2,'FINAL'),(3,'SEMIFINAL'),(4,'CUARTOS'),(5,'OCTAVOS');
/*!40000 ALTER TABLE `fases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `gol`
--

LOCK TABLES `gol` WRITE;
/*!40000 ALTER TABLE `gol` DISABLE KEYS */;
INSERT INTO `gol` VALUES (1,1,2,NULL,NULL,NULL,NULL,0,NULL,1),(2,2,1,NULL,NULL,NULL,NULL,1,NULL,1),(3,1,1,NULL,NULL,NULL,NULL,0,NULL,1),(4,1,1,NULL,NULL,NULL,NULL,0,NULL,1),(5,2,2,NULL,NULL,NULL,NULL,0,NULL,2),(6,2,2,NULL,NULL,NULL,NULL,0,NULL,2),(7,2,2,NULL,NULL,NULL,NULL,1,NULL,1),(8,2,2,NULL,NULL,NULL,NULL,1,NULL,1),(9,1,1,NULL,NULL,NULL,NULL,1,NULL,2),(10,1,1,NULL,NULL,NULL,NULL,1,NULL,2),(11,2,NULL,NULL,NULL,NULL,NULL,0,5,2),(12,1,NULL,NULL,NULL,NULL,NULL,0,5,1);
/*!40000 ALTER TABLE `gol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jugada`
--

LOCK TABLES `jugada` WRITE;
/*!40000 ALTER TABLE `jugada` DISABLE KEYS */;
INSERT INTO `jugada` VALUES (1,1,'111',NULL,NULL,NULL,NULL,1),(2,2,'222',NULL,NULL,NULL,NULL,1),(3,1,'333',NULL,NULL,NULL,NULL,2),(4,2,NULL,NULL,NULL,NULL,5,2),(5,1,NULL,NULL,NULL,NULL,5,1);
/*!40000 ALTER TABLE `jugada` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `jugador`
--

LOCK TABLES `jugador` WRITE;
/*!40000 ALTER TABLE `jugador` DISABLE KEYS */;
INSERT INTO `jugador` VALUES (1,'fdelgadov@unsa.edu.pe','A','Defensa','11111111','333333333','12/14/11',NULL,1,1,NULL,'SISTEMAS E INFORMATICA'),(2,'fdelgadov@unsa.edu.pe','B','Defensa','22222222','123123123','25/11/13',NULL,2,2,NULL,'SISTEMAS E INFORMATICA');
/*!40000 ALTER TABLE `jugador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `partido`
--

LOCK TABLES `partido` WRITE;
/*!40000 ALTER TABLE `partido` DISABLE KEYS */;
INSERT INTO `partido` VALUES (5,2,1,'2024-12-06','REALIZADO',1,1,NULL,NULL,4,1,'');
/*!40000 ALTER TABLE `partido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `partido_voley`
--

LOCK TABLES `partido_voley` WRITE;
/*!40000 ALTER TABLE `partido_voley` DISABLE KEYS */;
/*!40000 ALTER TABLE `partido_voley` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `portero`
--

LOCK TABLES `portero` WRITE;
/*!40000 ALTER TABLE `portero` DISABLE KEYS */;
INSERT INTO `portero` VALUES (1,1,1,NULL,1),(2,2,2,NULL,1),(3,1,3,NULL,2);
/*!40000 ALTER TABLE `portero` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `punto_voley`
--

LOCK TABLES `punto_voley` WRITE;
/*!40000 ALTER TABLE `punto_voley` DISABLE KEYS */;
/*!40000 ALTER TABLE `punto_voley` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `sustitucion`
--

LOCK TABLES `sustitucion` WRITE;
/*!40000 ALTER TABLE `sustitucion` DISABLE KEYS */;
INSERT INTO `sustitucion` VALUES (1,1,2,NULL,NULL,NULL,NULL,NULL,1,1),(2,2,1,NULL,NULL,NULL,NULL,NULL,1,2),(3,2,1,NULL,NULL,NULL,NULL,NULL,5,2),(4,1,2,NULL,NULL,NULL,NULL,NULL,5,1);
/*!40000 ALTER TABLE `sustitucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tarjeta`
--

LOCK TABLES `tarjeta` WRITE;
/*!40000 ALTER TABLE `tarjeta` DISABLE KEYS */;
INSERT INTO `tarjeta` VALUES (1,1,NULL,NULL,NULL,NULL,1,NULL,1),(2,2,NULL,NULL,NULL,NULL,2,NULL,1),(3,1,NULL,NULL,NULL,NULL,3,NULL,1),(4,2,NULL,NULL,NULL,NULL,1,5,2),(5,1,NULL,NULL,NULL,NULL,2,5,1),(6,2,NULL,NULL,NULL,NULL,3,5,2);
/*!40000 ALTER TABLE `tarjeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `tipo_campeonato`
--

LOCK TABLES `tipo_campeonato` WRITE;
/*!40000 ALTER TABLE `tipo_campeonato` DISABLE KEYS */;
INSERT INTO `tipo_campeonato` VALUES (1,'Campeonato único'),(2,'Campeonato con Categorías');
/*!40000 ALTER TABLE `tipo_campeonato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'fdelgadov','fdelgadov@unsa.edu.pe','Franco','Delgado Valencia','12/09','75275880','Administrador');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `usuario_cip`
--

LOCK TABLES `usuario_cip` WRITE;
/*!40000 ALTER TABLE `usuario_cip` DISABLE KEYS */;
INSERT INTO `usuario_cip` VALUES (1,'75275880','Franco Delgado Valencia','SISTEMAS E INFORMATICA',NULL);
/*!40000 ALTER TABLE `usuario_cip` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-12-06 16:34:46
