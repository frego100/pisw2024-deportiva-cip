INSERT INTO `color` VALUES (1,'Amarilla'),(2,'Roja'),(3,'Azul');

INSERT INTO `deporte` VALUES (1,'Fútbol'),(2,'Voley'),(3,'Baloncesto');

INSERT INTO `fases` VALUES (1,'GRUPOS'),(2,'FINAL'),(3,'SEMIFINAL'),(4,'CUARTOS'),(5,'OCTAVOS');

INSERT INTO `tipo_campeonato` VALUES (1,'Campeonato único'),(2,'Campeonato con Categorías');

INSERT INTO `usuario_cip` VALUES (1,'75275880','Franco Delgado Valencia','SISTEMAS E INFORMATICA',NULL)